package zunayedhassan.ImageEditorFX;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.layout.HBox;

/**
 *
 * @author Zunayed Hassan
 */
public class AdjustElement extends HBox {
    public static final int DEFAULT_LABEL_WIDTH = 80;
    
    public String Title = "";
    public double MiniumValue = 0;
    public double MaxiumValue = 0;
    public double InitialValue = 0;
    
    private Label _titleLabel = new Label();
    private Slider _slider = null;
    private Spinner<Double> _spinner = null;
    
    public AdjustElement(String title, double min, double max, double value) {
        this._initializeProperties(title, min, max, value);
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeProperties(String title, double min, double max, double value) {
        this.Title = title;
        this.MiniumValue = min;
        this.MaxiumValue = max;
        this.InitialValue = value;
    }
    
    private void _initializeLayout() {
        this.setAlignment(Pos.CENTER_LEFT);
        
        this._titleLabel.setText(this.Title);
        this._titleLabel.prefWidth(DEFAULT_LABEL_WIDTH);
        this._titleLabel.setMinWidth(DEFAULT_LABEL_WIDTH);
        
        this._slider = new Slider(this.MiniumValue, this.MaxiumValue, this.InitialValue);
        
        this._spinner = new Spinner(this.MiniumValue, this.MaxiumValue, this.InitialValue);
        this._spinner.setMaxWidth(70);
        this._spinner.setEditable(true);
        
        this.getChildren().addAll(
                this._titleLabel,
                this._slider,
                this._spinner
        );
    }
    
    private void _initializeEvents() {
        
    }
    
    public Slider GetSlider() {
        return this._slider;
    }
    
    public Spinner GetSpinner() {
        return this._spinner;
    }
    
    public void Reset() {
        this._slider.setValue(this.InitialValue);
        this._spinner.getValueFactory().valueProperty().set(this.InitialValue);
    }
}
