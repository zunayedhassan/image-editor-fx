package zunayedhassan.ImageEditorFX;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;
import javafx.util.StringConverter;
import zunayedhassan.CommonUI.IconButton;
import zunayedhassan.CommonUI.IconToggleButton;
import zunayedhassan.ImageProcessor.SimpleImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class RotatePane extends MainToolBarPane {
    public static final Color GRID_COLOR = Color.CORNFLOWERBLUE;
    public static final double GRID_WIDTH = 2.0;
    
    private IconButton _rotateCounterClockwiseButton = new IconButton("icons/object-rotate-left.png", "Rotate Couterclockwise");
    private IconButton _rotateClockwiseButton = new IconButton("icons/object-rotate-right.png", "Rotate clockwise");
    private IconButton _flipVerticallyButton = new IconButton("icons/object-flip-vertical.png", "Rotate vertically");
    private IconButton _flipHorizontallyButton = new IconButton("icons/object-flip-horizontal.png", "Rotate horizontally");
    private Slider _straightenSlider = new Slider(-45, 45, 0);
    private Spinner<Integer> _straightenSpinner = new Spinner<>(-45, 45, 0);
    private IconToggleButton _levelToolButton = new IconToggleButton("icons/applications-engineering.png", "Draw a guide to straighten out the image");
    private HBox _rotateHBox = null;
    private Label _angleLabel = new Label("Angle:");
    private ImageModel _backupImage = null;
    private SimpleDoubleProperty _straightenAngleProperty = new SimpleDoubleProperty(0);
    private Line _levelToolLine = this._getLevelToolLine();
    private Line[] _gridLines = { new Line(), new Line(), new Line(), new Line() };
    private Pane _grid = this._getGrid();
    
    public RotatePane(MainView parent) {
        super("Rotate and Flip", parent);
        
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeLayout() {
        GridPane buttonsPane = new GridPane();
        buttonsPane.setAlignment(Pos.CENTER);
        super.Add(buttonsPane);
        
        buttonsPane.setVgap(10);
        
        buttonsPane.add(this._rotateCounterClockwiseButton, 0, 0);
        buttonsPane.add(this._rotateClockwiseButton, 1, 0);
        buttonsPane.add(this._flipVerticallyButton, 0, 1);
        buttonsPane.add(this._flipHorizontallyButton, 1, 1);
        
        Label straightenLabel = new Label("Straighten");
        straightenLabel.getStyleClass().add("main-toolbar-pane-title");
        
        super.Add(super.GetSeperator());
        super.Add(straightenLabel);
        super.Add(super.GetSeperator());
        super.Add(this._angleLabel);
        
        this._angleLabel.setMinWidth(215);
        this._angleLabel.setAlignment(Pos.CENTER_LEFT);
        
        this._rotateHBox = new HBox(5);
        this._rotateHBox.setAlignment(Pos.CENTER);
        this._rotateHBox.getChildren().addAll(
                this._straightenSlider,
                this._straightenSpinner
        );
        
        this._straightenSpinner.setEditable(true);
        this._straightenSpinner.setMaxWidth(70);
        this._levelToolButton.setText("Level Tool");
        
        super.Add(this._rotateHBox);
        super.Add(super.GetSeperator());
        super.Add(this._levelToolButton);
        super.Add(super.GetSeperator());
        super.Add(new Label(
                "Manually adjust the angle or use\nthe Lavel Tool to draw the horizontal\nor vertical guide."
        ));
        
        super.GetMainPane().setAlignment(Pos.TOP_CENTER);
    }
    
    private void _initializeEvents() {
        this._rotateHBox.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _angleLabel.setMinWidth(newValue.doubleValue());
                _levelToolButton.setMinWidth(newValue.doubleValue());
            }
        });
        
        // Rotate Counterclockwise
        this._rotateCounterClockwiseButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyRotateCounterclockwise();
            }
        });
        
        // Rotate clockwise
        this._rotateClockwiseButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyRotateClockwise();
            }
        });
        
        // Straighten
        this._straightenAngleProperty.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _applyStraighten(newValue.intValue());
                _straightenSpinner.getValueFactory().setValue(newValue.intValue());
            }
        });
        
        this._straightenSlider.valueProperty().bindBidirectional(this._straightenAngleProperty);
        
        this._straightenSpinner.getValueFactory().setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer object) {
                return Integer.toString(object);
            }

            @Override
            public Integer fromString(String string) {
                return Integer.parseInt(string);
            }
        });
        
        this._straightenSpinner.getValueFactory().valueProperty().addListener(new ChangeListener<Integer>() {
            @Override
            public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
                _straightenAngleProperty.set(newValue.doubleValue());
            }
        });
        
        // Level tool
        this._levelToolButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean isSelected) {
                if (isSelected) {
                    _addLevelToolLine();
                }
                else {
                    _removeLevelToolLine();
                }
            }
        });
        
        this._grid.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyLevelToolOnMousePressed(event);
            }
        });

        this._grid.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyLevelToolOnMouseDragged(event);
            }
        });
        
        this._grid.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyLevelToolOnMouseReleased(event);
            }
        });
        
        // Vertical Flip
        this._flipVerticallyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyFlipVertical();
            }
        });
        
        // Horizontal Flip
        this._flipHorizontallyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyFlipHorizontally();
            }
        });
    }
    
    public void Initialize() {
        this.StartFeatures();
        this._addGrid();
    }
    
    private void _applyRotateCounterclockwise() {
        if (super.parent.IsFileOpened()) {
            ImageModel result = SimpleImageProcessor.GET_ROTATED_IMAGE_COUNTERCLOCKWISE(super.parent.GetCurrentImage());
            super.parent.OpenImageFile(result);
            this.UpdateGrid();
        }
    }
    
    private void _applyRotateClockwise() {
        if (super.parent.IsFileOpened()) {
            ImageModel result = SimpleImageProcessor.GET_ROTATED_IMAGE_CLOCKWISE(super.parent.GetCurrentImage());
            super.parent.OpenImageFile(result);
            this.UpdateGrid();
        }
    }
    
    private void _applyStraighten(double angle) {
        if ((angle != 0) && super.parent.IsFileOpened()) {
            ImageModel originalImage = new ImageModel(this._backupImage, super.parent.GetCurrentImage().GetFileLocation());
            ImageModel rotatedImage = SimpleImageProcessor.GET_ROTATED_IMAGE(originalImage, -angle);
            ImageModel result = SimpleImageProcessor.GET_STRAIGHTEN_IMAGE(rotatedImage);
            
            ImageModel previousImage = super.parent.GetCurrentImage();
            previousImage = null;
            
            super.parent.OpenImageFile(result);
        }
        
        this.UpdateGrid();
    }
    
    public void TerminateFeatures() {
        this._removeGrid();
        this._removeLevelToolLine();
    }
    
    public void StartFeatures() {
        this._updateBackupImage();
        this.UpdateGrid();
    }
    
    public void UpdateGrid() {
        double width = super.parent.GetCurrentImage().getWidth();
        double height = super.parent.GetCurrentImage().getHeight();
        
        this._grid.setPrefSize(width, height);
        this._grid.setMaxSize(width, height);

        Line line1 = this._gridLines[0];
        Line line2 = this._gridLines[1];
        Line line3 = this._gridLines[2];
        Line line4 = this._gridLines[3];
        
        line1.setStartX(0);
        line1.setStartY(super.parent.GetCurrentImage().getHeight() * 0.33);
        line1.setEndX(super.parent.GetCurrentImage().getWidth());
        line1.setEndY(super.parent.GetCurrentImage().getHeight() * 0.33);
        
        line2.setStartX(0);
        line2.setStartY(super.parent.GetCurrentImage().getHeight() * 0.66);
        line2.setEndX(super.parent.GetCurrentImage().getWidth());
        line2.setEndY(super.parent.GetCurrentImage().getHeight() * 0.66);
        
        line3.setStartX(super.parent.GetCurrentImage().getWidth() * 0.33);
        line3.setStartY(0);
        line3.setEndX(super.parent.GetCurrentImage().getWidth() * 0.33);
        line3.setEndY(super.parent.GetCurrentImage().getHeight());
        
        line4.setStartX(super.parent.GetCurrentImage().getWidth() * 0.66);
        line4.setStartY(0);
        line4.setEndX(super.parent.GetCurrentImage().getWidth() * 0.66);
        line4.setEndY(super.parent.GetCurrentImage().getHeight());
    }
    
    private Pane _getGrid() {
        Pane grid = new Pane();
        grid.setBorder(new Border(new BorderStroke(GRID_COLOR, BorderStrokeStyle.DASHED, CornerRadii.EMPTY, new BorderWidths(GRID_WIDTH))));
        
        for (int i = 0; i < this._gridLines.length; i++) {
            Line line = this._gridLines[i];
            line.setStroke(GRID_COLOR);
            line.setStrokeWidth(GRID_WIDTH);
            line.getStrokeDashArray().add(3d);

            grid.getChildren().add(line);
        }
        
        return grid;
    }
    
    private void _addGrid() {
        if (!super.parent.ImageViewStackPane.getChildren().contains(this._grid)) {
            super.parent.ImageViewStackPane.getChildren().add(this._grid);
        }
    }
    
    private void _removeGrid() {
        if (super.parent.ImageViewStackPane.getChildren().contains(this._grid)) {
            super.parent.ImageViewStackPane.getChildren().remove(this._grid);
        }
    }
    
    private void _addLevelToolLine() {
        if (!this._grid.getChildren().contains(this._levelToolLine)) {
            this._grid.getChildren().add(this._levelToolLine);
            
            this._levelToolLine.setStartX(0);
            this._levelToolLine.setStartY(0);
            this._levelToolLine.setEndX(0);
            this._levelToolLine.setEndY(0);
        }
    }
    
    private void _removeLevelToolLine() {
        if (this._grid.getChildren().contains(this._levelToolLine)) {
            this._grid.getChildren().remove(this._levelToolLine);
        }
    }
    
    public boolean IsLevelToolSelected() {
        return this._levelToolButton.isSelected();
    }
    
    private void _applyLevelToolOnMousePressed(MouseEvent event) {
        if (this.IsLevelToolSelected() && super.parent.IsFileOpened()) {
            this._addLevelToolLine();
            
            this._levelToolLine.setStartX(event.getX());
            this._levelToolLine.setStartY(event.getY());
            
            this._levelToolLine.setEndX(event.getX());
            this._levelToolLine.setEndY(event.getY());
        }
    }
    
    private void _applyLevelToolOnMouseDragged(MouseEvent event) {
        if (this.IsLevelToolSelected() && super.parent.IsFileOpened()) {
            this._levelToolLine.setEndX(event.getX());
            this._levelToolLine.setEndY(event.getY());
        }
    }
    
    private void _applyLevelToolOnMouseReleased(MouseEvent event) {
        if (this.IsLevelToolSelected() && super.parent.IsFileOpened()) {
            this._levelToolLine.setEndX(event.getX());
            this._levelToolLine.setEndY(event.getY());
            
            this._removeLevelToolLine();
            this._calculateAngleFromLevelTool();
        }
    }
    
    private Line _getLevelToolLine() {
        Line line = new Line();
        
        line.setStroke(Color.ORANGE);
        line.setStrokeWidth(2);
        
        return line;
    }
    
    private void _calculateAngleFromLevelTool() {
        int x1 = (int) this._levelToolLine.getStartX();
        int y1 = (int) this._levelToolLine.getStartY();
        
        int x2 = (int) this._levelToolLine.getEndX();
        int y2 = (int) this._levelToolLine.getEndY();
        
        double hypotenuse = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        double adjacent = x2 - x1;
        double angle = Math.toDegrees(Math.cosh(adjacent / hypotenuse)) *  ((x2 > x1) ? 1 : -1);
        
        this._applyStraighten(angle);
    }
    
    private void _updateBackupImage() {
        this._backupImage = SimpleImageProcessor.GET_CLONED_IMAGE(super.parent.GetCurrentImage());
    }
    
    private void _applyFlipVertical() {
        ImageModel result = SimpleImageProcessor.GET_VERTICALLY_FLIPPED_IMAGE(super.parent.GetCurrentImage());
        super.parent.OpenImageFile(result);
        this._updateBackupImage();
    }
    
    private void _applyFlipHorizontally() {
        ImageModel result = SimpleImageProcessor.GET_HORIZONTALLY_FLIPPED_IMAGE(super.parent.GetCurrentImage());
        super.parent.OpenImageFile(result);
        this._updateBackupImage();
    }
}
