package zunayedhassan.ImageEditorFX;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;

/**
 *
 * @author Zunayed Hassan
 */
public class EffectPaneItem extends TitledPane {
    private final GridPane   _gridPane   = new GridPane();
    private final ScrollPane _scrollPane = new ScrollPane(this._gridPane);
    
    private String           _title      = null;
    private int              _row        = 0;
    private int              _column     = 0;
    
    public EffectPaneItem(String title) {
        this._initializeVariables(title);
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeVariables(String title) {
        this._title = title;
    }
    
    private void _initializeLayout() {
        this.setText(this._title);
        this.setContent(this._scrollPane);
        this._gridPane.setPadding(new Insets(7));
        this._gridPane.setVgap(5);
        this._gridPane.setHgap(5);
    }
    
    private void _initializeEvents() {
        
    }
    
    public void Add(Node node) {
        this._gridPane.add(node, _column, _row);
        
        this._column++;
        
        if (this._column == 2) {
            this._column = 0;
            this._row++;
        }
    }
}
