package zunayedhassan.ImageEditorFX;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 *
 * @author Zunayed Hassan
 */
public class ScrapImage extends StackPane {
    public  static final Color      STROKE_COLOR   = Color.CORNFLOWERBLUE;
    public  static final double     STROKE_WIDTH   = 2;
    
    private              ImageModel _image         = null;
    private              double     _orgSceneX     = 0;
    private              double     _orgSceneY     = 0;
    private              double     _orgTranslateX = 0;
    private              double     _orgTranslateY = 0;
    private              boolean    _isSelected    = false;
    
    public ScrapImage(ImageModel image) {
        this._initializeData(image);
        this._initializeLayout();
        this._initializeEvents();
        this._initializeLater();
    }
    
    private void _initializeData(ImageModel image) {
        this._image = image;
    }
    
    private void _initializeLayout() {
        this.getChildren().add(new ImageView(this._image));
        this.setBorder(new Border(new BorderStroke(STROKE_COLOR, BorderStrokeStyle.DASHED, CornerRadii.EMPTY, new BorderWidths(STROKE_WIDTH))));
        this.setCursor(Cursor.MOVE);
    }
    
    private void _initializeEvents() {
        // Width
        this._image.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number width) {
                _setWidth(width.doubleValue());
            }
        });
        
        // Height
        this._image.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number height) {
                _setHeight(height.doubleValue());
            }
        });
        
        // Mouse Events
        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnMousePressed(event);
            }
        });
        
        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnMouseDragged(event);
            }
        });
        
        this.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnMouseReleased();
            }
        });
    }
    
    private void _initializeLater() {
        this._setWidth(this._image.getWidth());
        this._setHeight(this._image.getHeight());
    }
    
    private void _setWidth(double width) {
        this.setWidth(width);
        this.setMaxWidth(width);
        this.setPrefWidth(width);
    }
    
    private void _setHeight(double height) {
        this.setHeight(height);
        this.setMaxHeight(height);
        this.setPrefHeight(height);
    }
    
    private void _applyOnMousePressed(MouseEvent event) {
        this._isSelected = true;
        
        this._orgSceneX = event.getSceneX();
        this._orgSceneY = event.getSceneY();

        this._orgTranslateX = getTranslateX();
        this._orgTranslateY = getTranslateY();
    }
    
    private void _applyOnMouseDragged(MouseEvent event) {
        double offsetX = event.getSceneX() - this._orgSceneX;
        double offsetY = event.getSceneY() - this._orgSceneY;
        double newTranslateX = this._orgTranslateX + offsetX;
        double newTranslateY = this._orgTranslateY + offsetY;
        
        this._setPosition(newTranslateX, newTranslateY);
    }
    
    private void _applyOnMouseReleased() {
        this._isSelected = false;
    }
    
    private void _setPosition(double x, double y) {
        this.setTranslateX(x);
        this.setTranslateY(y);
    }
    
    public ImageModel GetImage() {
        return this._image;
    }
    
    public boolean IsSelected() {
        return this._isSelected;
    }
}
