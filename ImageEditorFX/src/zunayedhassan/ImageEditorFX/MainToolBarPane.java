package zunayedhassan.ImageEditorFX;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Zunayed Hassan
 */
public class MainToolBarPane extends VBox {
    private Label _titleLabel = null;
    private VBox _mainPane = new VBox(5);
    
    protected MainView parent = null;
    
    public MainToolBarPane(String title, MainView parent) {
        super(5);
        this.parent = parent;
        this._initializeLayout(title);
    }
    
    private void _initializeLayout(String title) {
        super.getStyleClass().addAll(
                "common-ui-toolbar-gradient",
                "image-editor-main-pane"
        );
        
        this._titleLabel = new Label(title);
        this._titleLabel.getStyleClass().add("main-toolbar-pane-title");
        
        this.setAlignment(Pos.TOP_CENTER); 
        
        HBox titleHBox = new HBox(5);
        titleHBox.getChildren().add(this._titleLabel);
        titleHBox.setAlignment(Pos.CENTER);
        
        this.getChildren().addAll(
                titleHBox,
                this.GetSeperator(),
                this._mainPane
        );
        
        this._mainPane.setAlignment(Pos.CENTER_LEFT);
    }
    
    public void Add(Node node) {
        this._mainPane.getChildren().add(node);
    }
    
    public VBox GetMainPane() {
        return this._mainPane;
    }
    
    public Pane GetSeperator() {
        Pane separator = new Pane();
        separator.setMinHeight(7);
        separator.setPrefHeight(7);
        
        return separator;
    }
}
