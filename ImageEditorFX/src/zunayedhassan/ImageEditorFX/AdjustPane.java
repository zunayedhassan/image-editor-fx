package zunayedhassan.ImageEditorFX;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import zunayedhassan.ImageProcessor.SimpleImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class AdjustPane extends MainToolBarPane {
    public static enum ADJUSTMENT_OPERATION {
        BRIGHTNESS,
        CONTRAST,
        SATURATION,
        HUE,
        TEMPERATURE,
        TINT,
        GAMMA,
        BLUR,
        SHARPNESS
    }
    
    private final AdjustElement         _brightnessElement    = new AdjustElement("Brightness",  -100, 100, 0);
    private final AdjustElement         _contrastElement      = new AdjustElement("Contrast",    -100, 100, 0);
    private final AdjustElement         _saturationElement    = new AdjustElement("Saturation",  -100, 100, 0);
    private final AdjustElement         _hueElement           = new AdjustElement("Hue",         -180, 180, 0);
    private final AdjustElement         _temparatureElement   = new AdjustElement("Temperature", -100, 100, 0);
    private final AdjustElement         _tintElement          = new AdjustElement("Tint",        -100, 100, 0);
    private final AdjustElement         _gammaElement         = new AdjustElement("Gamma",          1, 100, 1);
    private final Button                _blurButton           = new Button("Blur");
    private final Button                _sharpenButton        = new Button("Sharpen");
    private final Button                _resetButton          = new Button("Reset");
    
    private       ImageModel            _backupImage          = null;
    private       ImageModel            _resetImage           = null;
    private       ADJUSTMENT_OPERATION  _currentOperation     = null;
    
    public AdjustPane(MainView parent) {
        super("Adjust", parent);
        this._initializeLayout();
        this._initializeEvents();
    }
    
    // Layout
    private void _initializeLayout() {
        super.Add(this._brightnessElement);
        super.Add(this._contrastElement);
        super.Add(this._saturationElement);
        super.Add(super.GetSeperator());
        super.Add(this._hueElement);
        super.Add(this._temparatureElement);
        super.Add(this._tintElement);
        super.Add(super.GetSeperator());
        super.Add(this._gammaElement);
        super.Add(super.GetSeperator());
        super.Add(this._blurButton);
        super.Add(this._sharpenButton);
        super.Add(super.GetSeperator());
        super.Add(this._resetButton);
        
        this._blurButton.setPrefWidth(294);
        this._sharpenButton.setPrefWidth(294);
        this._resetButton.setPrefWidth(294);
    }
    
    // Events
    private void _initializeEvents() {
        // Brightness
        this._brightnessElement.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number brightness) {
                _applyOnBrightness(brightness.doubleValue());
                _brightnessElement.GetSpinner().getValueFactory().valueProperty().set(brightness.doubleValue());
            }
        });
        
        this._brightnessElement.GetSpinner().getValueFactory().valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object brightness) {
                _applyOnBrightness((Double) brightness);
                _brightnessElement.GetSlider().setValue((Double) brightness);
            }
        });
        
        // Contrast
        this._contrastElement.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number contrast) {
                _applyOnContrast(contrast.doubleValue());
                _contrastElement.GetSpinner().getValueFactory().valueProperty().set(contrast.doubleValue());
            }
        });
        
        this._contrastElement.GetSpinner().getValueFactory().valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object contrast) {
                _applyOnContrast((Double) contrast);
                _contrastElement.GetSlider().setValue((Double) contrast);
            }
        });
        
        // Saturation
        this._saturationElement.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number saturation) {
                _applyOnSaturation(saturation.doubleValue());
                _saturationElement.GetSpinner().getValueFactory().valueProperty().set(saturation.doubleValue());
            }
        });
        
        this._saturationElement.GetSpinner().getValueFactory().valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object saturation) {
                _applyOnSaturation((Double) saturation);
                _saturationElement.GetSlider().setValue((Double) saturation);
            }
        });
        
        // Hue
        this._hueElement.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number hue) {
                _applyOnHue(hue.doubleValue());
                _hueElement.GetSpinner().getValueFactory().valueProperty().set(hue.doubleValue());
            }
        });
        
        this._hueElement.GetSpinner().getValueFactory().valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object hue) {
                _applyOnHue((Double) hue);
                _hueElement.GetSlider().setValue((Double) hue);
            }
        });
        
        // Temperature
        this._temparatureElement.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number temperature) {
                _applyOnTemperature(temperature.doubleValue());
                _temparatureElement.GetSpinner().getValueFactory().valueProperty().set(temperature.doubleValue());
            }
        });
        
        this._temparatureElement.GetSpinner().getValueFactory().valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object temperature) {
                _applyOnTemperature((Double) temperature);
                _temparatureElement.GetSlider().setValue((Double) temperature);
            }
        });
        
        // Tint
        this._tintElement.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number tint) {
                _applyOnTint(tint.doubleValue());
                _tintElement.GetSpinner().getValueFactory().valueProperty().set(tint.doubleValue());
            }
        });
        
        this._tintElement.GetSpinner().getValueFactory().valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object tint) {
                _applyOnTint((Double) tint);
                _tintElement.GetSlider().setValue((Double) tint);
            }
        });
        
        // Gamma
        this._gammaElement.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number gamma) {
                _applyOnGamma(gamma.doubleValue());
                _gammaElement.GetSpinner().getValueFactory().valueProperty().set(gamma.doubleValue());
            }
        });
        
        this._gammaElement.GetSpinner().getValueFactory().valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object gamma) {
                _applyOnGamma((Double) gamma);
                _gammaElement.GetSlider().setValue((Double) gamma);
            }
        });
        
        // Blur
        this._blurButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyOnBlur();
            }
        });
        
        // Sharpness
        this._sharpenButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applySharpness();
            }
        });
        
        // Reset All
        this._resetButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyResetAll();
            }
        });
    }

    public void Initialize() {
        if (super.parent.IsFileOpened()) {
            this._resetImage = null;
        }
        
        this._updateBackupImage();
    }
    
    public void ClearSettings() {
        this._backupImage = null;
        this._currentOperation = null;
    }
    
    private void _updateBackupImage() {
        if (super.parent.IsFileOpened()) {
            this._backupImage = SimpleImageProcessor.GET_CLONED_IMAGE(super.parent.GetCurrentImage());
        }
    }
    
    private void _updateResetImage() {
        if (this._resetImage == null) {
            this._resetImage  = SimpleImageProcessor.GET_CLONED_IMAGE(super.parent.GetCurrentImage());
        }
    }
    
    public void ClearResetImage() {
        this._resetImage = null;
    }
    
    public void ClearAdjustOptions() {
        this._brightnessElement.Reset();
        this._contrastElement.Reset();
        this._saturationElement.Reset();
        this._hueElement.Reset();
        this._temparatureElement.Reset();
        this._tintElement.Reset();
        this._gammaElement.Reset();
    }
    
    private void _prepareForImageProcessing(ADJUSTMENT_OPERATION currentOperation) {
        if (super.parent.IsFileOpened()) {
            this._updateResetImage();
                
            if (this._currentOperation != currentOperation) {
                this._currentOperation = currentOperation;
                this._updateBackupImage();
            }
        }
    }
    
    private void _applyOnBrightness(double brightness) {
        this._prepareForImageProcessing(ADJUSTMENT_OPERATION.BRIGHTNESS);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_ADJUSTED_IMAGE_AFTER_BRIGHTNESS_CHANGED(originalImage, (int) brightness);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyOnContrast(double contrast) {
        this._prepareForImageProcessing(ADJUSTMENT_OPERATION.CONTRAST);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_ADJUSTED_IMAGE_AFTER_CONTRAST_CHANGED(originalImage, (int) contrast);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyOnSaturation(double saturation) {
        this._prepareForImageProcessing(ADJUSTMENT_OPERATION.SATURATION);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_ADJUSTED_IMAGE_AFTER_SATURATION_CHANGED(originalImage, (int) saturation);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyOnHue(double hue) {
        this._prepareForImageProcessing(ADJUSTMENT_OPERATION.HUE);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_ADJUSTED_IMAGE_AFTER_HUE_CHANGED(originalImage, (int) hue);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyOnTemperature(double temperature) {
        this._prepareForImageProcessing(ADJUSTMENT_OPERATION.TEMPERATURE);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_ADJUSTED_IMAGE_AFTER_TEMERATURE_CHANGED(originalImage, (int) temperature);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyOnTint(double tint) {
        this._prepareForImageProcessing(ADJUSTMENT_OPERATION.TINT);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_ADJUSTED_IMAGE_AFTER_TINT_CHANGED(originalImage, (int) tint);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyOnGamma(double gamma) {
        this._prepareForImageProcessing(ADJUSTMENT_OPERATION.GAMMA);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_ADJUSTED_IMAGE_AFTER_GAMMA_CHANGED(originalImage, (int) gamma);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyOnBlur() {
        if (super.parent.IsFileOpened()) {
            this._updateResetImage();
            
            ImageModel originalImage = super.parent.GetCurrentImage();
            ImageModel result = SimpleImageProcessor.GET_GAUSSIAN_BLURED_IMAGE(originalImage);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applySharpness() {
        if (super.parent.IsFileOpened()) {
            this._updateResetImage();
            
            ImageModel originalImage = super.parent.GetCurrentImage();
            ImageModel result = SimpleImageProcessor.GET_GAUSSIAN_SHARPENED_IMAGE(originalImage);
            super.parent.OpenImageFile(result);
        }
    }

    private void _applyResetAll() {
        if (super.parent.IsFileOpened()) {
            this._updateResetImage();
            this.ClearAdjustOptions();
            super.parent.OpenImageFile(this._resetImage);
        }
    }
}
