package zunayedhassan.ImageEditorFX;

import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 *
 * @author Zunayed Hassan
 */
public class OutlineComboBoxElement extends StackPane {
    private double _width = -1;
    
    public OutlineComboBoxElement() {
        this.Set(this._getText());
    }
    
    public OutlineComboBoxElement(double width) {
        this._width = width;
        this.Set(width);
    }
    
    public void Set(double width) {
        this.Set(this._getLine(width));
    }
    
    public void Set(Node node) {
        this.getChildren().clear();
        this.getChildren().add(node);
    }
    
    private Text _getText() {
        Text text = new Text("X");
        text.setFont(Font.font(13));
        
        return text;
    }
    
    private Line _getLine(double width) {
        Line line = new Line(0, 0, 10, 0);
        line.setStrokeWidth(width);
        
        return line;
    }
    
    public Node Get() {
        if (this._width == -1) {
            return this._getText();
        }
        else {
            return this._getLine(this._width);
        }
    }
    
    public double GetStrokeWidth() {
        double outlineWidth = 0;
                    
        if (this.Get() instanceof Text) {
            outlineWidth = 0.0;
        }
        else if (this.Get() instanceof Line) {
            Line line = (Line) this.Get();
            outlineWidth = line.getStrokeWidth();
        }
        
        return outlineWidth;
    }
}
