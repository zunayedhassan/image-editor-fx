package zunayedhassan.ImageEditorFX;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import zunayedhassan.CommonUI.RoundedIconButton;
import zunayedhassan.CommonUI.RoundedIconToggleButton;
import zunayedhassan.ImageProcessor.SimpleImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class SelectPane extends MainToolBarPane {
    private final HBox                      _selectionToolsTitle       = this._getTitlePane("Selection Tools");
    private final ToggleGroup               _selectionToggleGroup      = new ToggleGroup();
    private final RoundedIconToggleButton   _rectangleSelectButton     = new RoundedIconToggleButton("icons/Select-Rectangular_32.png", "Retangle Select");
    private final RoundedIconToggleButton   _ellipseSelectButton       = new RoundedIconToggleButton("icons/Select-Ellipse_32.png",     "Ellipse Select" );
    private final RoundedIconToggleButton   _lassoSelectButton         = new RoundedIconToggleButton("icons/Select-Lasso_32.png",       "Lasso Select"   );
    private final HBox                      _editToolsTitle            = this._getTitlePane("Edit Tools");
    private final RoundedIconButton         _copyButton                = new RoundedIconButton("icons/edit-copy_32.png",          "Copy"           );
    private final RoundedIconButton         _cutButton                 = new RoundedIconButton("icons/edit-cut_32.png",           "Cut"            );
    private final RoundedIconButton         _pasteButton               = new RoundedIconButton("icons/edit-paste_32.png",         "Paste"          );
    private final RoundedIconButton         _removeButton              = new RoundedIconButton("icons/edit-delete_32.png",        "Remove"         );   
    private final Button                    _applyButton               = new Button("Apply");
    
    private       ImageModel                _clipboardImage            = null;
    
    public SelectPane(MainView parent) {
        super("Select", parent);
        
        this._initializeLayout();
        this._initializeEvents();
    }

    // Layout
    private void _initializeLayout() {
        HBox selectionPane1 = this._getCenterAlignedHBox();
        
        selectionPane1.getChildren().addAll(
                this._rectangleSelectButton
//                this._rectangleSelectButton,
//                this._ellipseSelectButton,
//                this._lassoSelectButton
        );
        
        // XXX: I Will Add more tools later
        this._rectangleSelectButton.setSelected(true);
        
        for (Node node : selectionPane1.getChildren()) {
            this._selectionToggleGroup.getToggles().add((RoundedIconToggleButton) node);
        }
        
        HBox editPane1 = this._getCenterAlignedHBox();
        HBox editPane2 = this._getCenterAlignedHBox();

        editPane1.getChildren().addAll(
                this._copyButton,
                this._pasteButton
        );
        
        editPane2.getChildren().addAll(
                this._cutButton,
                this._removeButton
        );
        
        HBox applyPane = this._getCenterAlignedHBox();
        applyPane.getChildren().add(this._applyButton);
        
        super.Add(this._selectionToolsTitle);
        super.Add(selectionPane1);
        super.Add(super.GetSeperator());
        super.Add(this._editToolsTitle);
        super.Add(editPane1);
        super.Add(editPane2);
        super.Add(super.GetSeperator());
        super.Add(applyPane);
    }
    
    // Events
    private void _initializeEvents() {
        // Rectangle Seletion Button
        this._rectangleSelectButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean isSelected) {
                _applyOnRectangleSelectionButtonSelected(isSelected);
            }
        });
        
        // Copy
        this._copyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyCopy();
            }
        });
        
        // Paste
        this._pasteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyPaste();
            }
        });
        
        // Cut
        this._cutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyCut();
            }
        });
        
        // Delete
        this._removeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyRemove();
            }
        });
        
        // Apply
        this._applyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _apply();
            }
        });
    }
    
    private HBox _getCenterAlignedHBox() {
        HBox hbox = new HBox(5);
        hbox.setAlignment(Pos.CENTER);
        
        return hbox;
    }
    
    private HBox _getTitlePane(String title) {
        HBox titlePane = this._getCenterAlignedHBox();
        titlePane.getChildren().add(new Label(title));
        
        return titlePane;
    }
    
    public boolean IsRectangleSelectionToolSelected() {
        return this._rectangleSelectButton.isSelected();
    }
    
    public boolean IsEllipseSelectionToolSelected() {
        return this._ellipseSelectButton.isSelected();
    }
    
    public boolean IsLassoSelectionToolSelected() {
        return this._lassoSelectButton.isSelected();
    }
    
    private void _applyOnRectangleSelectionButtonSelected(boolean isSelected) {
        if (!isSelected) {
            super.parent.RemovePreviewSelectionRectangle();
        }
    }

    private double[] _applyCopy() {
        if (super.parent.IsRectangleSelectionModeEnabled() && super.parent.IsFileOpened() && super.parent.IsRectangleSelectionPreviewExists()) {
            double[] rectangleSelectionProperties = super.parent.GetPreviewRectangleProperties();
            
            if (!super.parent.IsBlankBounds(rectangleSelectionProperties)) {
                this._clipboardImage = SimpleImageProcessor.GET_IMAGE_PORTION_FROM_RECTANGLE_SELECTION(super.parent.GetCurrentImage(), rectangleSelectionProperties);
                
                return rectangleSelectionProperties;
            }
        }
        
        return null;
    }
    
    private void _applyPaste() {
        if (super.parent.IsFileOpened() && (this._clipboardImage != null)) {
            // XXX: I Will check later
            super.parent.ApplyActualSize();
            
            ImageModel clonedScrapedImageData = SimpleImageProcessor.GET_CLONED_IMAGE(this._clipboardImage);
            super.parent.Add(new ScrapImage(clonedScrapedImageData));
        }
    }
    
    private void _applyCut() {
        double[] bounds = this._applyCopy();
        
        if (bounds != null) {
            ImageModel result = SimpleImageProcessor.GET_IMAGE_AFTER_REMOVE_PORTION(super.parent.GetCurrentImage(), bounds);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyRemove() {
        if (super.parent.IsRectangleSelectionModeEnabled()) {
            double[] rectangleSelectionProperties = super.parent.GetPreviewRectangleProperties();
            
            if (!super.parent.IsBlankBounds(rectangleSelectionProperties)) {
               ImageModel result = SimpleImageProcessor.GET_IMAGE_AFTER_REMOVE_PORTION(super.parent.GetCurrentImage(), rectangleSelectionProperties);
               super.parent.OpenImageFile(result);
            }
        }
    }
    
    private void _apply() {
        super.parent.AttachScrapImageToSourceImage();
    }
}
