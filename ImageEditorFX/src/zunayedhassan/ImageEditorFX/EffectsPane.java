package zunayedhassan.ImageEditorFX;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import zunayedhassan.CommonUI.IconToggleButton;
import zunayedhassan.ImageProcessor.SimpleImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class EffectsPane extends MainToolBarPane {
    public static enum EFFECT_OPERATION {
        GRAYSCALE,
        BLACK_AND_WHITE,
        SEPIA,
        OIL_PAINTING,
        PIXELATE,
        PEN_OUTLINE,
        EMBOSS,
        GLASS,
        COLOR_FILTER_BLACK,
        COLOR_FILTER_WHITE,
        COLOR_FILTER_RED,
        COLOR_FILTER_LIME,
        COLOR_FILTER_BLUE,
        COLOR_FILTER_YELLOW,
        COLOR_FILTER_CYAN,
        COLOR_FILTER_MAGENTA,
        COLOR_FILTER_TEAL
    }
    
    private final Accordion        _effectsAccordion        = new Accordion();
    private final EffectPaneItem   _classicEffectsPane      = new EffectPaneItem("Classic");
    private final EffectPaneItem   _artisticEffectsPane     = new EffectPaneItem("Artistic");
    private final EffectPaneItem   _colorFiltersEffectsPane = new EffectPaneItem("Color Filters");
    private final IconToggleButton _grayscaleEffectButton   = new IconToggleButton("Grayscale",     "icons/effect_grayscale.png",    ContentDisplay.TOP);
    private final IconToggleButton _bAndwEffectButton       = new IconToggleButton("Black & White", "icons/effect_b&w.png",          ContentDisplay.TOP);
    private final IconToggleButton _sepiaEffectButton       = new IconToggleButton("Sepia",         "icons/effect_sepia.png",        ContentDisplay.TOP);
    private final IconToggleButton _oilPaintingButton       = new IconToggleButton("Oil Painting",  "icons/effect_oil_painting.png", ContentDisplay.TOP);
    private final IconToggleButton _pixelateButton          = new IconToggleButton("Pixelate",      "icons/effect_pixelate.png",     ContentDisplay.TOP);
    private final IconToggleButton _penOutlineEffectButton  = new IconToggleButton("Pen Outline",   "icons/effect_pen_outline.png",  ContentDisplay.TOP);
    private final IconToggleButton _embossEffectButton      = new IconToggleButton("Emboss",        "icons/effect_emboss.png",       ContentDisplay.TOP);
    private final IconToggleButton _glassEffectButton       = new IconToggleButton("Glass",         "icons/effect_glass.png",        ContentDisplay.TOP);
    private final ColorFilterItem  _blackColorFilterItem    = new ColorFilterItem("Black",          "icons/effect_black.png",        0, 100, 0         );
    private final ColorFilterItem  _whiteColorFilterItem    = new ColorFilterItem("White",          "icons/effect_white.png",        0, 150, 0         );
    private final ColorFilterItem  _redColorFilterItem      = new ColorFilterItem("Red",            "icons/effect_red.png",          0, 150, 0         );
    private final ColorFilterItem  _limeColorFilterItem     = new ColorFilterItem("Lime",           "icons/effect_lime.png",         0, 150, 0         );
    private final ColorFilterItem  _blueColorFilterItem     = new ColorFilterItem("Blue",           "icons/effect_blue.png",         0, 150, 0         );
    private final ColorFilterItem  _yellowColorFilterItem   = new ColorFilterItem("Yellow",         "icons/effect_yellow.png",       0,  70, 0         );
    private final ColorFilterItem  _cyanColorFilterItem     = new ColorFilterItem("Cyan",           "icons/effect_cyan.png",         0,  70, 0         );
    private final ColorFilterItem  _magentaColorFilterItem  = new ColorFilterItem("Magenta",        "icons/effect_magenta.png",      0,  70, 0         );
    private final ColorFilterItem  _tealColorFilterItem     = new ColorFilterItem("Teal",           "icons/effect_teal.png",         0, 150, 0         );

    private       ImageModel       _backupImage             = null;
    private       EFFECT_OPERATION _currentOperation        = null;
    
    public EffectsPane(MainView parent) {
        super("Effects", parent);
        
        this._initializeLayout();
        this._initializeEvents();
    }
    
    // Layout
    private void _initializeLayout() {
        super.Add(this._effectsAccordion);
        
        this._effectsAccordion.getPanes().addAll(
                this._classicEffectsPane,
                this._artisticEffectsPane,
                this._colorFiltersEffectsPane
        );
        
        // Classic
        this._classicEffectsPane.Add(this._grayscaleEffectButton);
        this._classicEffectsPane.Add(this._bAndwEffectButton);
        this._classicEffectsPane.Add(this._sepiaEffectButton);
        
        // Artistic
        this._artisticEffectsPane.Add(this._oilPaintingButton);
        this._artisticEffectsPane.Add(this._pixelateButton);
        this._artisticEffectsPane.Add(this._penOutlineEffectButton);
        this._artisticEffectsPane.Add(this._embossEffectButton);
        this._artisticEffectsPane.Add(this._glassEffectButton);
        
        // Color Filter
        this._colorFiltersEffectsPane.Add(this._blackColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._whiteColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._redColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._limeColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._blueColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._yellowColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._cyanColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._magentaColorFilterItem);
        this._colorFiltersEffectsPane.Add(this._tealColorFilterItem);
        
        if (this._effectsAccordion.getPanes().size() > 0) {
            this._effectsAccordion.setExpandedPane(this._effectsAccordion.getPanes().get(0));
        }
    }
    
    // Events
    private void _initializeEvents() {
        // Grayscale
        this._grayscaleEffectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyGrayscale();
            }
        });
        
        // Black & White
        this._bAndwEffectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyBlackAndWhite();
            }
        });
        
        // Sepia
        this._sepiaEffectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applySepia();
            }
        });
        
        // Oil Painting
        this._oilPaintingButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyOilPainting();
            }
        });
        
        // Pixelate
        this._pixelateButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyPixelate();
            }
        });
        
        // Pen Outline
        this._penOutlineEffectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyPenOutline();
            }
        });
        
        // Emboss
        this._embossEffectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyEmboss();
            }
        });
        
        // Glass
        this._glassEffectButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyGlass();
            }
        });
        
        // Black Color Filter
        this._blackColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyBlackColorFilter(value.intValue());
            }
        });
        
        // White Color Filter
        this._whiteColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyWhiteColorFilter(value.intValue());
            }
        });
        
        // Red Color Filter
        this._redColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyRedColorFilter(value.intValue());
            }
        });
        
        // Lime Color Filter
        this._limeColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyLimeColorFilter(value.intValue());
            }
        });
        
        // Blue Color Filter
        this._blueColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyBlueColorFilter(value.intValue());
            }
        });
        
        // Yellow Color Filter
        this._yellowColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyYellowColorFilter(value.intValue());
            }
        });
        
        // Cyan Color Filter
        this._cyanColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyCyanColorFilter(value.intValue());
            }
        });
        
        // Magenta Color Filter
        this._magentaColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyMagentaColorFilter(value.intValue());
            }
        });
        
        // Teal Color Filter
        this._tealColorFilterItem.GetSlider().valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number value) {
                _applyTealColorFilter(value.intValue());
            }
        });
    }
    
    public void Initialize() {
        this._updateBackupImage();
    }
    
    public void ClearSettings() {
        this._backupImage = null;
        this._currentOperation = null;
    }
    
    private void _updateBackupImage() {
        if (super.parent.IsFileOpened()) {
            this._backupImage = SimpleImageProcessor.GET_CLONED_IMAGE(super.parent.GetCurrentImage());
        }
    }
    
    public void ClearEffectOptions() {
        for (TitledPane titledPane : this._effectsAccordion.getPanes()) {
            EffectPaneItem effectPaneItem = (EffectPaneItem) titledPane;
            
            for (Node item : ((GridPane) ((ScrollPane) effectPaneItem.getContent()).getContent()).getChildren()) {
                if (item instanceof IconToggleButton) {
                    IconToggleButton effectButton = (IconToggleButton) item;
                    effectButton.Reset();
                }
                else if (item instanceof ColorFilterItem) {
                    ColorFilterItem colorFilterItem = (ColorFilterItem) item;
                    colorFilterItem.Reset();
                }
            }
        }
    }
    
    private void _prepareForImageProcessing(EFFECT_OPERATION currentOperation) {
        if (super.parent.IsFileOpened() && (this._currentOperation != currentOperation)) { 
            this._currentOperation = currentOperation;
            this._updateBackupImage();
        }
    }
    
    private void _applyGrayscale() {
        this._prepareForImageProcessing(EFFECT_OPERATION.GRAYSCALE);
        
        if (super.parent.IsFileOpened()) {
            if (this._grayscaleEffectButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_GRAYSCALLED_IMAGE(originalImage);
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applyBlackAndWhite() {
        this._prepareForImageProcessing(EFFECT_OPERATION.BLACK_AND_WHITE);
        
        if (super.parent.IsFileOpened()) {
            if (this._bAndwEffectButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_BLACK_AND_WHITE_IMAGE(originalImage);
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applySepia() {
        this._prepareForImageProcessing(EFFECT_OPERATION.SEPIA);
        
        if (super.parent.IsFileOpened()) {
            if (this._sepiaEffectButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_SEPIA_IMAGE(originalImage);
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applyOilPainting() {
        this._prepareForImageProcessing(EFFECT_OPERATION.OIL_PAINTING);
        
        if (super.parent.IsFileOpened()) {
            if (this._oilPaintingButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_OIL_PAINTING_IMAGE(originalImage);
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applyPixelate() {
        this._prepareForImageProcessing(EFFECT_OPERATION.PIXELATE);
        
        if (super.parent.IsFileOpened()) {
            if (this._pixelateButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_PIXELATE_IMAGE(originalImage);
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applyPenOutline() {
        this._prepareForImageProcessing(EFFECT_OPERATION.PEN_OUTLINE);
        
        if (super.parent.IsFileOpened()) {
            if (this._penOutlineEffectButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_PEN_OUTLINE_IMAGE(originalImage);
                
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applyEmboss() {
        this._prepareForImageProcessing(EFFECT_OPERATION.EMBOSS);
        
        if (super.parent.IsFileOpened()) {
            if (this._embossEffectButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_EMBOSS_IMAGE(originalImage);
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applyGlass() {
        this._prepareForImageProcessing(EFFECT_OPERATION.GLASS);
        
        if (super.parent.IsFileOpened()) {
            if (this._glassEffectButton.isSelected()) {
                ImageModel originalImage = this._backupImage;
                ImageModel result = SimpleImageProcessor.GET_GLASS_IMAGE(originalImage);
                super.parent.OpenImageFile(result);
            }
            else {
                super.parent.OpenImageFile(this._backupImage);
            }
        }
    }
    
    private void _applyBlackColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_BLACK);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_BLACK(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyWhiteColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_WHITE);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_WHITE(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyRedColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_RED);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_RED(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyLimeColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_LIME);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_LIME(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyBlueColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_BLUE);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_BLUE(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyYellowColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_YELLOW);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_YELLOW(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyCyanColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_CYAN);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_CYAN(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyMagentaColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_MAGENTA);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_MAGENTA(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
    
    private void _applyTealColorFilter(int value) {
        this._prepareForImageProcessing(EFFECT_OPERATION.COLOR_FILTER_TEAL);
        
        if (super.parent.IsFileOpened()) {
            ImageModel originalImage = this._backupImage;
            ImageModel result = SimpleImageProcessor.GET_COLOR_FILTERED_IMAGE_TEAL(originalImage, value);
            super.parent.OpenImageFile(result);
        }
    }
}
