package zunayedhassan.ImageEditorFX;

import java.io.File;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.image.Image;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;

/**
 *
 * @author Zunayed Hassan
 */
public class FileInformationDialog extends Dialog {
    private ImageModel _image = null;
    private GridPane _pane = new GridPane();
    
    public FileInformationDialog(ImageModel image) {
        this._image = image;
        this._initializeLayout();
    }
    
    private void _initializeLayout() {
        String filePath = this._image.GetFileLocation().substring("file".length() + 1, this._image.GetFileLocation().length());
        String fileName = (new File(filePath).getName());
        
        super.setTitle(fileName + " File Info");
        super.setHeaderText("File Information");
        super.setGraphic(new ImageView(this.getClass().getResource("icons/dialog-information.png").toString()));
        super.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        
        this._pane.setVgap(5);
        this._pane.setHgap(15);
        
        String[] fileInfo = this._getFileInformation(filePath);
        
        String[] fileInfoTitle =  new String[] {
            "File Path",
            "File Size",
            "Dimensions",
            "Format",
            "Date Created",
            "Date Modified",
            "X Resolution",
            "Y Resolution",
            "Unit"
        };
        
        
        for (int row = 0, i = 0; row < fileInfo.length + 2; row++, i++) {
            this._addToFileInfoPane(fileInfoTitle[i], fileInfo[i], row);
            
            if ((i == 3) || (i == 5)) {
                this._pane.add(this._getSeparator(), 0, ++row);
            }
        }
        
        super.getDialogPane().setContent(this._pane);
    }
    
    private void _addToFileInfoPane(String title, String value, int row) {
        Label  titleLabel = new Label(title + ":");
        titleLabel.setStyle("-fx-font-weight: bold;");
        
        this._pane.add(titleLabel, 0, row);
        this._pane.add(new Label(value), 1, row);
    }
    
    private String[] _getFileInformation(String fileName) {
        File file = new File(fileName);
        
        if (file.exists()) {
            String[] fileInfo = new String[9];
            
            String filePath = file.getPath();
            String fileSize = (file.length() / 1000.00) + " KB";

            Image image = new Image("file:" + filePath);
            String imageDimensions = image.getWidth() + " X " + image.getHeight() + " pixels";

            String format = "";

            if (filePath.trim().toLowerCase().endsWith(".jpg")) {
                format = "(JPG) Joint Photographic Experts Group";
            }
            else if (filePath.trim().toLowerCase().endsWith(".png")) {
                format = "(PNG) Portable Network Graphics";
            }
            else if (filePath.trim().toLowerCase().endsWith(".bmp")) {
                format = "(BMP) Bitmap Image File";
            }
            else if (filePath.trim().toLowerCase().endsWith(".gif")) {
                format = "(GIF) Graphics Interchange Format";
            }

            String dateCreated = "";
            String dateModified = "";

            try {
                BasicFileAttributes fileAttribute = Files.readAttributes(file.toPath(), BasicFileAttributes.class);

                FileTime creationTime = fileAttribute.creationTime();
                dateCreated = creationTime.toString();

                FileTime modifiedTime = fileAttribute.lastModifiedTime();
                dateModified = modifiedTime.toString();

            }
            catch (IOException exception) {
                exception.printStackTrace();
            }

            String xResolution = Double.toString(Screen.getPrimary().getDpi());
            String yResolution = Double.toString(Screen.getPrimary().getDpi());
            String unit = "Inch";
            
            fileInfo[0] = filePath;
            fileInfo[1] = fileSize;
            fileInfo[2] = imageDimensions;
            fileInfo[3] = format;
            fileInfo[4] = dateCreated;
            fileInfo[5] = dateModified;
            fileInfo[6] = xResolution;
            fileInfo[7] = yResolution;
            fileInfo[8] = unit;
            
            return fileInfo;
        }
        
        return null;
    }
    
    private Pane _getSeparator() {
        Pane separator = new Pane();
        separator.setPrefHeight(7);
        separator.setMinHeight(7);
        
        return separator;
    }
}
