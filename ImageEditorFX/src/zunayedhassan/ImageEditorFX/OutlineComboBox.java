package zunayedhassan.ImageEditorFX;

import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.text.Text;
import javafx.util.Callback;

/**
 *
 * @author Zunayed Hassan
 */
public class OutlineComboBox extends ComboBox<OutlineComboBoxElement> {
    public OutlineComboBox() {
        this.getItems().addAll(
                new OutlineComboBoxElement(),
                new OutlineComboBoxElement(1),
                new OutlineComboBoxElement(2),
                new OutlineComboBoxElement(3),
                new OutlineComboBoxElement(4),
                new OutlineComboBoxElement(5)
        );
        
        this.setCellFactory(new Callback<ListView<OutlineComboBoxElement>, ListCell<OutlineComboBoxElement>>() {
            @Override
            public ListCell<OutlineComboBoxElement> call(ListView<OutlineComboBoxElement> param) {
                return new ListCell<OutlineComboBoxElement>() {
                    private final OutlineComboBoxElement outlineComboBoxElement;
                    {
                        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                        outlineComboBoxElement = new OutlineComboBoxElement();
                    }
                    
                    @Override
                    protected void updateItem(OutlineComboBoxElement item, boolean empty) {
                        super.updateItem(item, empty);
                        
                        if ((item == null) || empty) {
                            setGraphic(null);
                        }
                        else {
                            outlineComboBoxElement.Set(item.Get());
                            setGraphic(outlineComboBoxElement);
                        }
                    }
                };
            }
        });
    }
}
