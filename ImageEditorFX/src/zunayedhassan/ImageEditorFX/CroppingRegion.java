package zunayedhassan.ImageEditorFX;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.shape.Line;

/**
 *
 * @author Zunayed Hassan
 */
public class CroppingRegion extends BorderPane {
    public static final double STROKE_WIDTH = 2.0;
    public static final double CORNER_SIZE = 5.0;
    public static final double DEFAULT_SCALING_PERCENT = 0.7;
    
    public boolean IsAlreadyScalled = false;
    
    private MainView _parent = null;
    private Line _topHandle = new Line();
    private Line _bottomHandle = new Line();
    private Line _leftHandle = new Line();
    private Line _rightHandle = new Line();
    private boolean _isMouseOnHandle = false;
    private Rectangle _topLeftCorner = this._getCorner(Cursor.NW_RESIZE);
    private Rectangle _topRightCorner = this._getCorner(Cursor.NE_RESIZE);
    private Rectangle _bottomLeftCorner = this._getCorner(Cursor.SW_RESIZE);
    private Rectangle _bottomRightCorner = this._getCorner(Cursor.SE_RESIZE);
    
    double orgSceneX = 0;
    double orgSceneY = 0;
    double orgTranslateX = 0;
    double orgTranslateY = 0;
    
    double width = 0;
    double height = 0;
    double ratio = 1;
    
    public CroppingRegion(MainView parent) {
        this._parent = parent;
        
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeLayout() {
        this.setBackground(new Background(new BackgroundFill(Color.rgb(100, 149, 237, 0.33), CornerRadii.EMPTY, Insets.EMPTY)));
        
        this.SetSize(100, 100);
        
        this.setTop(this._topHandle);
        this.setBottom(this._bottomHandle);
        this.setLeft(this._leftHandle);
        this.setRight(this._rightHandle);
        
        this._topHandle.setStartX(0);
        this._topHandle.setStartY(0);
        this._topHandle.setEndY(0);
        
        this._bottomHandle.setStartX(0);
        this._bottomHandle.setStartY(0);
        this._bottomHandle.setEndY(0);
        
        this._leftHandle.setStartX(0);
        this._leftHandle.setStartY(0);
        this._leftHandle.setEndX(0);
        
        this._rightHandle.setStartX(0);
        this._rightHandle.setStartY(0);
        this._rightHandle.setEndX(0);
        
        this._setStrokeToHandle(this._topHandle);
        this._setStrokeToHandle(this._bottomHandle);
        this._setStrokeToHandle(this._leftHandle);
        this._setStrokeToHandle(this._rightHandle);
        
        this._topHandle.setCursor(Cursor.N_RESIZE);
        this._bottomHandle.setCursor(Cursor.N_RESIZE);
        this._leftHandle.setCursor(Cursor.H_RESIZE);
        this._rightHandle.setCursor(Cursor.H_RESIZE);
        
        BorderPane cornerPane = new BorderPane();
        this.setCenter(cornerPane);
        
        BorderPane topCornerPane = new BorderPane();
        BorderPane bottomCornerPane = new BorderPane();
        
        cornerPane.setTop(topCornerPane);
        cornerPane.setBottom(bottomCornerPane);
        
        topCornerPane.setLeft(this._topLeftCorner);
        topCornerPane.setRight(this._topRightCorner);
        bottomCornerPane.setLeft(this._bottomLeftCorner);
        bottomCornerPane.setRight(this._bottomRightCorner);
        
        this.setCursor(Cursor.MOVE);
    }
    
    private void _initializeEvents() {
        this._topHandle.endXProperty().bind(this.maxWidthProperty());
        this._bottomHandle.endXProperty().bind(this.maxWidthProperty());
        this._leftHandle.endYProperty().bind(this.maxHeightProperty());
        this._rightHandle.endYProperty().bind(this.maxHeightProperty());

        // Top Handle
        this._topHandle.setOnMouseDragged(new EventHandler<MouseEvent>() {
            double height = 0;
            
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = true;
                height = getMaxHeight() - event.getY();
                
                if (height >= 1) {
                    double currentRatio = 1;
                    SetHeight(height);
                    
                    _parent.CropPaneForPhoto.LockAspectRatioCheckBox.setSelected(false);
                    
                    _parent.CropPaneForPhoto.ImageSizeComboBox.getSelectionModel().select("Custom");
                    
                    setTranslateY(getTranslateY() + (event.getY() / 2.0));
                    IsAlreadyScalled = true;
                }
            }
        });
        
        this._topHandle.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
        
        // Bottom Handle
        this._bottomHandle.setOnMouseDragged(new EventHandler<MouseEvent>() {
            double height = 0;
            
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = true;
                height = getMaxHeight() + event.getY();
                
                if (height >= 1) {
                    double currentRatio = 1;
                    SetHeight(height);
                    
                    _parent.CropPaneForPhoto.LockAspectRatioCheckBox.setSelected(false);
                    
                    _parent.CropPaneForPhoto.ImageSizeComboBox.getSelectionModel().select("Custom");
                    
                    setTranslateY(getTranslateY() + (event.getY() / 2.0));
                    IsAlreadyScalled = true;
                }
            }
        });
        
        this._bottomHandle.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
        
        // Left Handle
        this._leftHandle.setOnMouseDragged(new EventHandler<MouseEvent>() {
            double width = 0;
            
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = true;
                width = getMaxWidth() - event.getX();
                
                if (width >= 1) {
                    double currentRatio = 1;
                    SetWidth(width);
                    
                    _parent.CropPaneForPhoto.LockAspectRatioCheckBox.setSelected(false);
                    
                    _parent.CropPaneForPhoto.ImageSizeComboBox.getSelectionModel().select("Custom");

                    setTranslateX(getTranslateX() + (event.getX() / 2.0));
                    IsAlreadyScalled = true;
                }
            }
        });
        
        this._leftHandle.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
        
        // Right Handle
        this._rightHandle.setOnMouseDragged(new EventHandler<MouseEvent>() {
            double width = 0;
            
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = true;
                width = getMaxWidth() + event.getX();
                
                if (width >= 1) {
                    double currentRatio = 1;
                    SetWidth(width);
                    
                    _parent.CropPaneForPhoto.LockAspectRatioCheckBox.setSelected(false);
                    
                    _parent.CropPaneForPhoto.ImageSizeComboBox.getSelectionModel().select("Custom");
                    
                    setTranslateX(getTranslateX() + (event.getX() / 2.0));
                    IsAlreadyScalled = true;
                }
            }
        });
        
        this._rightHandle.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
        
        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                orgSceneX = event.getSceneX();
                orgSceneY = event.getSceneY();
                
                orgTranslateX = getTranslateX();
                orgTranslateY = getTranslateY();
            }
        });
        
        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            double offsetX = 0;
            double offsetY = 0;
            double newTranslateX = 0;
            double newTranslateY = 0;
            
            @Override
            public void handle(MouseEvent event) {
                if (!_isMouseOnHandle) {
                    offsetX = event.getSceneX() - orgSceneX;
                    offsetY = event.getSceneY() - orgSceneY;
                    newTranslateX = orgTranslateX + offsetX;
                    newTranslateY = orgTranslateY + offsetY;

                    SetPosition(newTranslateX, newTranslateY);
                }
            }
        });
        
        this.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                orgSceneX = 0;
                orgSceneY = 0;
                orgTranslateX = 0;
                orgTranslateY = 0;
            }
        });
        
        // Top Right Corner
        this._topRightCorner.setOnMouseDragged(new EventHandler<MouseEvent>() {
            Point2D mousePosition = null;
            
            @Override
            public void handle(MouseEvent event) {
                mousePosition = new Point2D(event.getX(), -event.getY());
                _updateSizeBasedOnCornersMousePosition(mousePosition);
            }
        });
        
        this._topRightCorner.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
        
        // Bottom Right Corner
        this._bottomRightCorner.setOnMouseDragged(new EventHandler<MouseEvent>() {
            Point2D mousePosition = null;
            
            @Override
            public void handle(MouseEvent event) {
                mousePosition = new Point2D(event.getX(), event.getY());
                _updateSizeBasedOnCornersMousePosition(mousePosition);
            }
        });
        
        this._bottomRightCorner.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
        
        // Top Left Corner
        this._topLeftCorner.setOnMouseDragged(new EventHandler<MouseEvent>() {
            Point2D mousePosition = null;
            
            @Override
            public void handle(MouseEvent event) {
                mousePosition = new Point2D(event.getX(), -event.getY());
                _updateSizeBasedOnCornersMousePosition(mousePosition);
            }
        });
        
        this._topLeftCorner.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
        
        // Bottom Left Corner
        this._bottomLeftCorner.setOnMouseDragged(new EventHandler<MouseEvent>() {
            Point2D mousePosition = null;
            
            @Override
            public void handle(MouseEvent event) {
                mousePosition = new Point2D(event.getX(), event.getY());
                _updateSizeBasedOnCornersMousePosition(mousePosition);
            }
        });
        
        this._bottomLeftCorner.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _isMouseOnHandle = false;
            }
        });
    }
    
    public Point2D[] GetCropRegion() {
        double scaleX = this._parent.CurrentImageView.getScaleX();
        double scaleY = this._parent.CurrentImageView.getScaleY();

        double originalImageWidth = this._parent.CurrentImageView.getFitWidth();
        double originalImageHeight = this._parent.CurrentImageView.getFitHeight();

        double scalledImageWidth = originalImageWidth * scaleX;
        double scalledImageHeight = originalImageHeight * scaleY;

        double positionX = (this.getTranslateX() - (this.getWidth() / 2.0)) + (scalledImageWidth / 2.0);
        double positionY = (this.getTranslateY() - (this.getHeight() / 2.0)) + (scalledImageHeight / 2.0);

        double translateX = positionX / scaleX;
        double translateY = positionY / scaleY;

        Point2D start = new Point2D(
                Math.ceil(translateX),
                Math.ceil(translateY)
        );  
        
        Point2D size = new Point2D(
                Math.floor(this.getWidth()  / this._parent.CurrentImageView.getScaleX()),
                Math.floor(this.getHeight() / this._parent.CurrentImageView.getScaleY())
        );

        return (new Point2D[] {
            start,
            size
        });
    }
    
    private void _setStrokeToHandle(Line line) {
        line.setStroke(Color.CORNFLOWERBLUE);
        line.getStrokeDashArray().add(5d);
        line.setStrokeWidth(STROKE_WIDTH);
    }
    
    public void SetHeight(double height) {
        if (!(height < 1)) {
            this.setPrefHeight(height);
            this.setMaxHeight(height);
        }
    }
    
    public void SetWidth(double width) {
        if (!(width < 1)) {
            this.setPrefWidth(width);
            this.setMaxWidth(width);
        }
    }
    
    public void SetSize(double width, double height) {
        this.SetWidth(width);
        this.SetHeight(height);
    }
    
    public void SetPosition(double x, double y) {
        this.setTranslateX(x);
        this.setTranslateY(y);
    }
    
    private Rectangle _getCorner(Cursor cursor) {
        Rectangle corner = new Rectangle(CORNER_SIZE, CORNER_SIZE, Color.WHITE);
        corner.setStroke(Color.CORNFLOWERBLUE);
        corner.setStrokeWidth(STROKE_WIDTH);
        corner.setCursor(cursor);
        
        return corner;
    }
    
    private void _updateSizeBasedOnCornersMousePosition(Point2D mousePosition) {
        this._isMouseOnHandle = true;
        this.IsAlreadyScalled = true;
        
        ratio =  getMaxWidth() / getMaxHeight();
        height = getMaxHeight() + mousePosition.getY();
        width = ratio * height;

        this.SetSize(width, height);
    }
    
    public void UpdateToDefaultSize() {
        double scaleX              = this._parent.CurrentImageView.getScaleX();
        double scaleY              = this._parent.CurrentImageView.getScaleY();

        double originalImageWidth  = this._parent.CurrentImageView.getFitWidth();
        double originalImageHeight = this._parent.CurrentImageView.getFitHeight();

        double scalledImageWidth   = originalImageWidth * scaleX;
        double scalledImageHeight  = originalImageHeight * scaleY;
        
        if (!this.IsAlreadyScalled) {
            this.SetSize(scalledImageWidth * DEFAULT_SCALING_PERCENT, scalledImageHeight * DEFAULT_SCALING_PERCENT);
        }
        else {
            this._parent.CropPaneForPhoto.ApplyCroppingDimension(this._parent.CropPaneForPhoto.ImageSizeComboBox.getSelectionModel().getSelectedItem());
        }
        
        this._parent.CropPaneForPhoto.ImageSizeComboBox.getSelectionModel().select("Custom");
        this.SetPosition(0, 0);
    }
    
    public void ClearSettings() {
        this.IsAlreadyScalled = false;
    }
}
