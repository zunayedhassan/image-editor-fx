package zunayedhassan.ImageEditorFX;

import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.image.ImageView;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.util.StringConverter;
import zunayedhassan.CommonUI.ColorChooserFX.ColorChooserButton;
import zunayedhassan.CommonUI.FontComboBoxElement;
import zunayedhassan.CommonUI.FontsComboBox;
import zunayedhassan.CommonUI.IconButton;
import zunayedhassan.CommonUI.IconToggleButton;

/**
 *
 * @author Zunayed Hassan
 */
public class TextPane extends MainToolBarPane {
    public Button AddTextButton = new Button("Add Text");
    public FontsComboBox FontsComboBox = new FontsComboBox();
    public ComboBox<Integer> FontSizeComboBox = new ComboBox<>();
    public IconToggleButton BoldButton = new IconToggleButton("icons/format-text-bold_16.png", "Bold");
    public IconToggleButton ItalicButton = new IconToggleButton("icons/format-text-italic_16.png", "Italic");
    public IconToggleButton UnderlineButton = new IconToggleButton("icons/format-text-underline_16.png", "Underline");
    public IconToggleButton StrikethroughButton = new IconToggleButton("icons/format-text-strikethrough_16.png", "Strikethrough");
    public IconButton LeftAlignButton = new IconButton("icons/format-justify-left_16.png", "Align Left");
    public IconButton CenterAlignButton = new IconButton("icons/format-justify-center_16.png", "Align Center");
    public IconButton JustifyAlignButton = new IconButton("icons/format-justify-fill_16.png", "Align Justify");
    public IconButton RightAlignButton = new IconButton("icons/format-justify-right_16.png", "Align Right");
    public Slider TextRotationSlider = new Slider(-180, 180, 0);
    public Spinner<Double> TextRotationSpinner = new Spinner<>(this.TextRotationSlider.getMin(), this.TextRotationSlider.getMax(), this.TextRotationSlider.getValue());
    public Rectangle FillColorPreview = this._getColorPreviewNodeForColorChooser(Color.BLACK);
    public Rectangle OutlineColorPreview = this._getColorPreviewNodeForColorChooser(Color.BLACK);
    public ColorChooserButton FillColorPicker = new ColorChooserButton(this.FillColorPreview);
    public ColorChooserButton OutlineColorPicker = new ColorChooserButton(this.OutlineColorPreview);
    public OutlineComboBox OutlineWidthComboBox = new OutlineComboBox();
    public Slider OpacitySlider = new Slider(10, 100, 100);
    public Spinner<Double> OpacitySpinner = new Spinner<>(this.OpacitySlider.getMin(), this.OpacitySlider.getMax(), this.OpacitySlider.getValue());
    public Button RemoveAllTextButton = new Button("Remove All Text");
    
    public TextPane(MainView parent) {
        super("Text", parent);
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeLayout() {
        super.Add(this.AddTextButton);
        super.Add(super.GetSeperator());
        super.Add(this.FontsComboBox);
        super.Add(super.GetSeperator());
        
        BorderPane textPropertiesBorderPane = new BorderPane();
        super.Add(textPropertiesBorderPane);
        
        HBox textSizeHBox = new HBox(5);
        textPropertiesBorderPane.setLeft(textSizeHBox);
        textSizeHBox.setAlignment(Pos.CENTER_LEFT);
        
        this.FontSizeComboBox.setEditable(true);
        this.FontSizeComboBox.setMaxWidth(70);
        this.FontSizeComboBox.setItems(FXCollections.observableArrayList(
                10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72, 96, 120, 144
        ));
        
        textSizeHBox.getChildren().addAll(
                new Label("Size:"),
                this.FontSizeComboBox
        );
        
        GridPane textStyleAndAlignments = new GridPane();
        textPropertiesBorderPane.setCenter(textStyleAndAlignments);
        
        textStyleAndAlignments.add(this.BoldButton, 0, 0);
        textStyleAndAlignments.add(this.ItalicButton, 1, 0);
        textStyleAndAlignments.add(this.UnderlineButton, 2, 0);
        textStyleAndAlignments.add(this.StrikethroughButton, 3, 0);
        textStyleAndAlignments.add(this.LeftAlignButton, 0, 1);
        textStyleAndAlignments.add(this.CenterAlignButton, 1, 1);
        textStyleAndAlignments.add(this.JustifyAlignButton, 2, 1);
        textStyleAndAlignments.add(this.RightAlignButton, 3, 1);
        
        textStyleAndAlignments.setVgap(5);
        textStyleAndAlignments.setPadding(new Insets(0, 0, 0, 10));
        
        super.Add(super.GetSeperator());
        super.Add(new Label("Text Rotation:"));
        
        HBox textRotationPane = new HBox(5);
        textRotationPane.setAlignment(Pos.CENTER_LEFT);
        super.Add(textRotationPane);
        
        this.TextRotationSpinner.setMaxWidth(70);
        
        textRotationPane.getChildren().addAll(
                this.TextRotationSlider,
                this.TextRotationSpinner
        );
        
        super.Add(super.GetSeperator());
        
        HBox textColorPane = new HBox(5);
        textColorPane.setAlignment(Pos.CENTER);
        super.Add(textColorPane);
        
        this.FillColorPicker.setTooltip(new Tooltip("Fill color"));
        this.OutlineColorPicker.setTooltip(new Tooltip("Outline color"));
        this.OutlineWidthComboBox.setTooltip(new Tooltip("Outline width"));
        
        textColorPane.getChildren().addAll(
                new ImageView(new Image(this.getClass().getResourceAsStream("icons/color-fill_24.png"))),
                this.FillColorPicker,
                this._getHorizontalGap(15),
                new ImageView(new Image(this.getClass().getResourceAsStream("icons/Draw-Ring_24.png"))),
                this.OutlineColorPicker,
                this.OutlineWidthComboBox
        );
        
        super.Add(super.GetSeperator());
        super.Add(new Label("Opacity:"));
        
        HBox opacityPane = new HBox(5);
        opacityPane.setAlignment(Pos.CENTER_LEFT);
        super.Add(opacityPane);
        
        opacityPane.getChildren().addAll(
                this.OpacitySlider,
                this.OpacitySpinner
        );
        
        this.OpacitySpinner.setMaxWidth(70);
        
        super.Add(super.GetSeperator());
        
        this.RemoveAllTextButton.setTooltip(new Tooltip("Delete all text blocks from the image"));
        super.Add(this.RemoveAllTextButton);
        
        this.AddTextButton.setPrefWidth(262);
        this.RemoveAllTextButton.setPrefWidth(262);        
    }
    
    private void _initializeEvents() {
        // Font Family
        this.FontsComboBox.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                AddTextButton.setPrefWidth(newValue.doubleValue());
                RemoveAllTextButton.setPrefWidth(newValue.doubleValue());
            }
        });
        
        // Text Fill Color
        this.FillColorPicker.CustomColorPicker.ColorProperty.addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                _applyTextFill(newValue);
            }
        });
        
        // Text Outline Color
        this.OutlineColorPicker.CustomColorPicker.ColorProperty.addListener(new ChangeListener<Color>() {
            @Override
            public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
                _applyTextOutline(newValue, OutlineWidthComboBox.getSelectionModel().getSelectedItem());
            }
        });
        
        // Add Text
        this.AddTextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyAddText();
            }
        });
        
        // Font Family
        this.FontsComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<FontComboBoxElement>() {
            @Override
            public void changed(ObservableValue<? extends FontComboBoxElement> observable, FontComboBoxElement oldValue, FontComboBoxElement selectedItem) {
                if (selectedItem != null) {
                    _applyChangeFontFamily(selectedItem);
                }
            }
        });
        
        // Font Size
        this.FontSizeComboBox.setConverter(new StringConverter<Integer>() {
            @Override
            public String toString(Integer object) {
                return (object != null) ? object.toString() : "12";
            }

            @Override
            public Integer fromString(String string) {
                return Integer.parseInt(string);
            }
        });
        
        this.FontSizeComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Integer>() {
            @Override
            public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
                _applyTextFontSize(newValue.doubleValue());
            }
        });
        
        // Bold
        this.BoldButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {             
                _applyTextBold(BoldButton.isSelected());
            }
        });
        
        // Italic
        this.ItalicButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyTextItalic(ItalicButton.isSelected());
            }
        });
        
        // Underline
        this.UnderlineButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyTextUnderline(UnderlineButton.isSelected());
            }
        });
        
        // Strikethrough
        this.StrikethroughButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyStrikethrough(StrikethroughButton.isSelected());
            }
        });
        
        // Left Alignment
        this.LeftAlignButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyLeftAlignment();
            }
        });
        
        // Center Alignment
        this.CenterAlignButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyCenterAlignment();
            }
        });
        
        // Justify Alignment
        this.JustifyAlignButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyJustifyAlignment();
            }
        });
        
        // Right Alignment
        this.RightAlignButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyRightAlignment();
            }
        });
        
        // Text Rotate
        this.TextRotationSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _applyTextRotation(newValue.intValue());
                TextRotationSpinner.getValueFactory().setValue((double) newValue.intValue());
            }
        });
        
        this.TextRotationSpinner.getValueFactory().setConverter(new StringConverter<Double>() {
            @Override
            public String toString(Double object) {
                return object.toString();
            }

            @Override
            public Double fromString(String string) {
                return ((string.trim().equals(" ")) ? 0 : Double.parseDouble(string));
            }
        });
        
        this.TextRotationSpinner.getValueFactory().valueProperty().addListener(new ChangeListener<Double>() {
            @Override
            public void changed(ObservableValue<? extends Double> observable, Double oldValue, Double newValue) {
                _applyTextRotation(newValue.intValue());
                TextRotationSlider.setValue(newValue.intValue());
            }
        });
        
        // Outline Width
        this.OutlineWidthComboBox.valueProperty().addListener(new ChangeListener<OutlineComboBoxElement>() {
            @Override
            public void changed(ObservableValue<? extends OutlineComboBoxElement> observable, OutlineComboBoxElement oldValue, OutlineComboBoxElement selectedItem) {
                _applyTextOutline(OutlineColorPicker.CustomColorPicker.ColorProperty.get(), selectedItem);
            }
        });
        
        // Opcity
        this.OpacitySlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _applyOpacitySlider(newValue.doubleValue());
            }
        });
        
        this.OpacitySpinner.getValueFactory().setConverter(new StringConverter<Double>() {
            @Override
            public String toString(Double object) {
                return object.toString();
            }

            @Override
            public Double fromString(String string) {
                return Double.parseDouble(string);
            }
        });
        
        this.OpacitySpinner.getValueFactory().valueProperty().addListener(new ChangeListener<Double>() {
            @Override
            public void changed(ObservableValue<? extends Double> observable, Double oldValue, Double newValue) {
                _applyOpacitySpinner(newValue);
            }
        });
        
        // Remove All Texts
        this.RemoveAllTextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ApplyRemoveAllTexts();
            }
        });
    }
    
    private Rectangle _getColorPreviewNodeForColorChooser(Color color) {
        Rectangle buttonColorPreview = new Rectangle(16, 16);
        buttonColorPreview.setStroke(Color.BLACK);
        buttonColorPreview.setStrokeWidth(1);
        buttonColorPreview.setFill(color);
        
        return buttonColorPreview;
    }
    
    private void _applyTextFill(Color color) {
        this._updateColorPreview(this.FillColorPreview, color);
        
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetColor(color);
                    break;
                }
            }
        }
    }
    
    private void _applyTextOutline(Color color, OutlineComboBoxElement outlineComboBoxElement) {
        this._updateColorPreview(this.OutlineColorPreview, color);
        
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetOutline(color, (outlineComboBoxElement == null) ? 0 : outlineComboBoxElement.GetStrokeWidth());

                    break;
                }
            }
        }
    }
    
    private Pane _getHorizontalGap(double size) {
        Pane gap = new Pane();
        gap.setPrefWidth(size);
        gap.setMinWidth(size);
        
        return gap;
    }
    
    private void _updateColorPreview(Rectangle preview, Color color) {
        preview.setFill(color);
    }
    
    private void _applyAddText() {
        TextElement textElement = new TextElement(super.parent);
        super.parent.DeselctAllTextElement();
        super.parent.Add(textElement);
        textElement.SetSelect();
    }
    
    private void _applyChangeFontFamily(FontComboBoxElement selectedItem) {
        String fontFamily = selectedItem.FontFamilyName;
        
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetFontFamily(fontFamily);
                    break;
                }
            }
        }
    }
    
    private void _applyTextBold(boolean isBold) {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetBold(isBold);
                    break;
                }
            }
        }
    }
    
    private void _applyTextItalic(boolean isItalic) {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetItalic(isItalic);
                    break;
                }
            }
        }
    }
    
    private void _applyTextUnderline(boolean isUnderline) {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetUnderline(isUnderline);
                    break;
                }
            }
        }
    }
    
    private void _applyStrikethrough(boolean isStrikethrough) {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetStrikethrough(isStrikethrough);
                    break;
                }
            }
        }
    }
    
    private void _applyTextFontSize(double fontSize) {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetFontSize(fontSize);
                    break;
                }
            }
        }
    }
    
    private void _applyLeftAlignment() {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetAlignment(TextAlignment.LEFT);
                    break;
                }
            }
        }
    }
    
    private void _applyCenterAlignment() {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetAlignment(TextAlignment.CENTER);
                    break;
                }
            }
        }
    }
    
    private void _applyJustifyAlignment() {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetAlignment(TextAlignment.JUSTIFY);
                    break;
                }
            }
        }
    }
    
    private void _applyRightAlignment() {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetAlignment(TextAlignment.RIGHT);
                    break;
                }
            }
        }
    }
    
    private void _applyTextRotation(double angle) {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    double newAngle = angle - currentTextElement.Rotation;
                    
                    if (newAngle != 0) {
                        currentTextElement.Rotation = angle;
                        currentTextElement.setRotate(angle);
                    }
                    
                    break;
                }
            }
        }
    }
    
    private void _applyOpacity(double opacity) {
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                if (currentTextElement.IsSelected()) {
                    currentTextElement.SetOpacity(opacity / 100.0);
                    
                    break;
                }
            }
        }
    }
    
    private void _applyOpacitySlider(double opacity) {
        this._applyOpacity(opacity);
        this.OpacitySpinner.getValueFactory().setValue(opacity);
    }
    
    private void _applyOpacitySpinner(double opacity) {
        this._applyOpacity(opacity);
        this.OpacitySlider.setValue(opacity);
    }
    
    public void ApplyRemoveAllTexts() {
        ArrayList<TextElement> toBeRemoved = new ArrayList<>();
        
        for (Node node : super.parent.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                toBeRemoved.add(currentTextElement);
            }
        }
        
        for (TextElement textElement : toBeRemoved) {
            super.parent.Remove(textElement);
        }
    }
}
