package zunayedhassan.ImageEditorFX;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToolBar;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.Bounds;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.geometry.Point2D;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.input.ScrollEvent;
import javafx.scene.Node;
import javax.imageio.ImageIO;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import zunayedhassan.CommonTools;
import zunayedhassan.CommonUI.IconButton;
import zunayedhassan.CommonUI.IconToggleButton;
import zunayedhassan.CommonUI.ToolbarHorizontalGap;
import zunayedhassan.ImageProcessor.SimpleImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class MainView extends BorderPane {
    public static final Integer MIN_SCALE                      = 5;
    public static final Integer MAX_SCALE                      = 1000;
    public static final Integer ZOOM_STEP                      = 5;
    public static final Color   SELECTION_PREVIEW_STROKE_COLOR = Color.CORNFLOWERBLUE;
    public static final double  SELECTION_PREVIEW_STROKE_WIDTH = 2;
    
    private ToolBar _topToolBar                                = new ToolBar();
    private BorderPane _leftPane                               = new BorderPane();
    private VBox _mainToolbarPane                              = new VBox();
    private ScrollPane _mainToolbarPaneScrollPane              = new ScrollPane(this._mainToolbarPane);
    private ToolBar _bottomToolbar                             = new ToolBar();
    private Point2D _grabHandPosition                          = null;
    private ImageModel _modifiedImage                          = null;
    private ScrollPane _mainToolBarScrollPane                  = new ScrollPane();
    private Rectangle _rectangleSelectionPreview               = this._getPreviewSelectionRectangle();
    private double _rectangleSelectionPreviewPreviousX         = 0;
    private double _rectangleSelectionPreviewPreviousY         = 0;
    private FileChooser _saveAsFileChooser                     = new FileChooser();
    
    public IconButton OpenFileIconButton                       = new IconButton("Open", "icons/document-open_24.png", "Open an Image");
    public IconButton SaveIconButton                           = new IconButton("Save", "icons/document-save_24.png", "Save Image");
    public IconButton SaveAsIconButton                         = new IconButton("Save As","icons/document-save-as_24.png", "Save As Image");
    public IconButton PrintIconButton                          = new IconButton("Print", "icons/Gnome-Document-Print_24.png", "Print");
    public IconButton SettingsIconButton                       = new IconButton("icons/Gnome-Preferences-System_24.png", "Settings");
    public IconButton HelpContentsIconButton                   = new IconButton("icons/help-contents_24.png", "Help Contents");
    public IconButton AboutIconButton                          = new IconButton("icons/help-about_24.png", "About");
    public ToggleGroup MainToolbarToggleGroup                  = new ToggleGroup();
    public IconToggleButton AdjustButton                       = new IconToggleButton("Adjust", "icons/nepomuk_48.png", ContentDisplay.TOP);
    public IconToggleButton EffctsButton                       = new IconToggleButton("Effects", "icons/tools-wizard_48.png", ContentDisplay.TOP);
    public IconToggleButton SelectButton                       = new IconToggleButton("Select", "icons/select-rectangular_48.png", ContentDisplay.TOP);
    public IconToggleButton RotateButton                       = new IconToggleButton("Rotate", "icons/transform-rotate_48.png", ContentDisplay.TOP);
    public IconToggleButton CropButton                         = new IconToggleButton("Crop", "icons/transform-crop_48.png", ContentDisplay.TOP);
    public IconToggleButton TextButton                         = new IconToggleButton("Text", "icons/draw-text_48.png", ContentDisplay.TOP);
    public IconToggleButton ResizeButton                       = new IconToggleButton("Resize", "icons/transform-scale_48.png", ContentDisplay.TOP);
    public IconButton FitToScreenButton                        = new IconButton("icons/zoom-fit-best_24.png", "Fit to Screen");
    public IconButton ActualSizeButton                         = new IconButton("icons/zoom-original_24.png", "Actual Size");
    public IconButton ZoomOutButton                            = new IconButton("icons/zoom-out_24.png", "Zoom Out");
    public Slider ZoomingSlider                                = new Slider(MIN_SCALE, MAX_SCALE, 100);
    public IconButton ZoomInButton                             = new IconButton("icons/zoom-in_24.png", "Zoom In");
    public Spinner<Double> ZoomingSpinner                      = new Spinner<>(MIN_SCALE, MAX_SCALE, this.ZoomingSlider.getValue());
    public IconToggleButton GrabToggleButton                   = new IconToggleButton("icons/Hand_24.png", "Move the image while image is zoomed in");
    public IconButton PreviousFileButton                       = new IconButton("icons/go-previous_24.png", "Previous File");
    public IconButton NextFileButton                           = new IconButton("icons/go-next_24.png", "Next File");
    public IconButton DeleteButton                             = new IconButton("icons/edit-delete_24.png", "Delete file from disk");
    public IconButton ViewOriginalImageButton                  = new IconButton("icons/image-x-generic_24.png", "Click and hold to view the original image");
    public IconButton FileInformationButton                    = new IconButton("", "icons/Info_24.png", "File Information", ContentDisplay.RIGHT);
    public IconButton UndoAllButton                            = new IconButton("icons/Arrow-Undo.png", "Undo all changes up to the all last saved state");
    public IconButton UndoButton                               = new IconButton("icons/edit-undo_24.png", "Undo");
    public IconButton RedoButton                               = new IconButton("icons/edit-undo-rtl_24.png", "Redo");
    public ImageView CurrentImageView                          = new ImageView();
    public StackPane ImageViewStackPane                        = new StackPane(this.CurrentImageView);
    public ScrollPane ImageScrollPane                          = new ScrollPane(this.ImageViewStackPane);
    public FileChooser OpenImageFileChooser                    = new FileChooser();
    public SimpleDoubleProperty ImageScaleProperty             = new SimpleDoubleProperty(1);
    public Alert ImageFileDeleteAlert                          = new Alert(Alert.AlertType.CONFIRMATION);
    public AdjustPane AdjustPaneForPhoto                       = new AdjustPane(this);
    public EffectsPane EffectsPaneForPhoto                     = new EffectsPane(this);
    public SelectPane SelectPaneForPhoto                       = new SelectPane(this);
    public TextPane TextPaneForPhoto                           = new TextPane(this);
    public RotatePane RotatePaneForPhoto                       = new RotatePane(this);
    public CropPane CropPaneForPhoto                           = new CropPane(this);
    public ResizePane ResizePaneForPhoto                       = new ResizePane(this);
    public SimpleBooleanProperty IsFileOpenedYet               = new SimpleBooleanProperty(false);
    
    public MainView() {
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeLayout() {
        this.setTop(this._topToolBar);
        this.setLeft(this._leftPane);
        
        this._topToolBar.getItems().addAll(
                this.OpenFileIconButton,
                new Separator(),
                this.SaveIconButton,
                this.SaveAsIconButton,
//                this.PrintIconButton,
                new ToolbarHorizontalGap()
//                this.SettingsIconButton,
//                new Separator(),
//                this.HelpContentsIconButton
//                this.AboutIconButton
        );
        
        this._mainToolbarPane.getStyleClass().add("common-ui-toolbar-gradient");
        this._leftPane.setLeft(this._mainToolbarPaneScrollPane);
        this._mainToolbarPaneScrollPane.setFitToWidth(true);
        this._mainToolbarPaneScrollPane.setFitToHeight(true);
        
        this._mainToolbarPane.setAlignment(Pos.TOP_CENTER);
        
        this._mainToolbarPane.getChildren().addAll(
                this.AdjustButton,
                this.EffctsButton,
                this.SelectButton,
                this.RotateButton,
                this.CropButton,
                this.TextButton,
                this.ResizeButton
        );
        
        this.MainToolbarToggleGroup.getToggles().addAll(
                this.AdjustButton,
                this.EffctsButton,
                this.SelectButton,
                this.RotateButton,
                this.CropButton,
                this.TextButton,
                this.ResizeButton
        );
        
        this.setBottom(this._bottomToolbar);
        
        this.ZoomingSpinner.getStyleClass().add("zooming-spinner");
        this.ZoomingSpinner.setEditable(true);
        
        this._bottomToolbar.getItems().addAll(
                this.FitToScreenButton,
                this.ActualSizeButton,
                this.ZoomOutButton,
                this.ZoomingSlider,
                this.ZoomInButton,
                this.ZoomingSpinner,
                new Separator(),
                this.GrabToggleButton,
                new Separator(),
                new ToolbarHorizontalGap(),
                this.PreviousFileButton,
                this.NextFileButton,
                new ToolbarHorizontalGap(),
                new Separator(),
                this.DeleteButton,
                this.ViewOriginalImageButton,
                this.FileInformationButton
//                new Separator(),
//                this.UndoAllButton,
//                this.UndoButton,
//                this.RedoButton
        );
        
        this.setCenter(this.ImageScrollPane);
        this.ImageScrollPane.setFitToWidth(true);
        this.ImageScrollPane.setFitToHeight(true);
        
        this.OpenImageFileChooser.setTitle("Open an image file to edit");
        this.OpenImageFileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter(
                        "Image files",
                        "*.jpg",
                        "*.png",
                        "*.bmp",
                        "*.gif"));
        
        this.ImageFileDeleteAlert.setTitle("Attention");
        this.ImageFileDeleteAlert.setHeaderText("Are you sure?");
        
        this._leftPane.setCenter(this._mainToolBarScrollPane);
        
        // Save as
        this._saveAsFileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Joint Photographic Experts Group (JPEG)", "*.jpg"),
                new FileChooser.ExtensionFilter("Portable Network Graphics (PNG)",         "*.png" ),
                new FileChooser.ExtensionFilter("Graphics Interchange Format (GIF)",       "*.gif" )
        );
        
        // TODO: I will update later
        this.UndoAllButton.setDisable(true);
        this.UndoButton.setDisable(true);
        this.RedoButton.setDisable(true);
    }
    
    private void _initializeEvents() {
        // Main toolbar
        this.MainToolbarToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                IconToggleButton seletedToggleButton = (IconToggleButton) newValue;
                
                if ((newValue == null) && (oldValue != null)) {
                    oldValue.setSelected(true);
                    seletedToggleButton = (IconToggleButton) oldValue;
                }
                
                if (seletedToggleButton != null) {
                    // Adjust
                    if (seletedToggleButton.equals(AdjustButton)) {
                        _setMainToolBarPane(AdjustPaneForPhoto);
                        AdjustPaneForPhoto.Initialize();
                    }
                    // Effect
                    else if (seletedToggleButton.equals(EffctsButton)) {
                        _setMainToolBarPane(EffectsPaneForPhoto);
                        EffectsPaneForPhoto.Initialize();
                    }
                    // Select
                    else if (seletedToggleButton.equals(SelectButton)) {
                        ApplyActualSize();
                        _setMainToolBarPane(SelectPaneForPhoto);
                    }
                    // Rotate
                    else if (seletedToggleButton.equals(RotateButton)) {
                        _setMainToolBarPane(RotatePaneForPhoto);
                        RotatePaneForPhoto.Initialize();
                    }
                    // Crop
                    else if (seletedToggleButton.equals(CropButton)) {
                        _setMainToolBarPane(CropPaneForPhoto);
                        CropPaneForPhoto.InitializeCroppingFeature();
                    }
                    // Text
                    else if (seletedToggleButton.equals(TextButton)) {
                        _setMainToolBarPane(TextPaneForPhoto);
                    }
                    // Resize
                    else if (seletedToggleButton.equals(ResizeButton)) {
                        _setMainToolBarPane(ResizePaneForPhoto);
                    }
                    
                    // Clear Settings when toggle button changes
                    if (!seletedToggleButton.equals(RotateButton)) {
                        RotatePaneForPhoto.TerminateFeatures();
                    }
                    
                    if (!seletedToggleButton.equals(CropButton)) {
                        CropPaneForPhoto.TerminateFeatures();
                    }
                    
                    if (!seletedToggleButton.equals(TextButton)) {
                        DeselctAllTextElement();
                    }
                    
                    if (!seletedToggleButton.equals(SelectButton)) {
                        RemovePreviewSelectionRectangle();
                    }
                }
            }
        });
        
        // Click on image stack pane
        this.ImageViewStackPane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnImageStackPanePressed(event);
            }
        });
        
        this.ImageViewStackPane.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnImageStackPaneDragged(event);
            }
        });

        // Scrolling image pane
        this.ImageScrollPane.addEventFilter(ScrollEvent.SCROLL, new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                _applyZoomingOnScroll(event.getDeltaY());
                
                event.consume();
            }
        });
        
        // Open image
        this.OpenFileIconButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _openAnImageFile();
            }
        });
        
        this.IsFileOpenedYet.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean isEnable) {
                SetEnableMainMenuToolBar(isEnable);
            }
        });
        
        // Fit to screen
        this.FitToScreenButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyFitToScreen();
            }
        });
        
        // Actual size
        this.ActualSizeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ApplyActualSize();
            }
        });
        
        // Zoom out
        this.ZoomOutButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyZoomOut();
            }
        });
        
        // Zoom slider
        this.ImageScaleProperty.bindBidirectional(this.ZoomingSlider.valueProperty());
        
        // Zoom in
        this.ZoomInButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyZoomIn();
            }
        });
        
        // Zoom spinner
        this.ZoomingSpinner.valueProperty().addListener(new ChangeListener<Double>() {
            @Override
            public void changed(ObservableValue<? extends Double> observable, Double oldValue, Double newValue) {
                _applyZoomSpinner(newValue);
            }
        });
        
        // Zooming
        this.ImageScaleProperty.addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _applyZoomSlider(newValue.doubleValue());
            }
        });
        
        // Grab image
        this.GrabToggleButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean isSelected) {
                if (isSelected && IsFileOpened()) {
                    ImageScrollPane.setCursor(Cursor.OPEN_HAND);
                }
                else if (!isSelected) {
                    ImageScrollPane.setCursor(Cursor.DEFAULT);
                }
            }
        });
        
        // Image View
        this.CurrentImageView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnImageViewMousePressed(event);
            }
        });
        
        this.CurrentImageView.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnImageViewMouseReleased(event);
            }
        });
        
        // Grab image
        this.CurrentImageView.setOnMouseDragged(new EventHandler<MouseEvent>() {            
            Point2D mousePosition = new Point2D(0, 0);
            
            @Override
            public void handle(MouseEvent event) {
                mousePosition = new Point2D(event.getX(), event.getY());
                _applyGrabImage(mousePosition);
            }
        });
        
        // Previous file
        this.PreviousFileButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyPreviousFile();
            }
        });
        
        // Next file
        this.NextFileButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyNextFile();
            }
        });
        
        // Delete
        this.DeleteButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyDeleteCurrentImageFile();
            }
        });
        
        // View original image
        this.ViewOriginalImageButton.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyViewOriginalImage(true);
            }
        });
        
        this.ViewOriginalImageButton.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyViewOriginalImage(false);
            }
        });
        
        // File information
        this.FileInformationButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyShowFileInfrmationDialog();
            }
        });
        
        // Save
        this.SaveIconButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applySave();
            }
        });
        
        // Save as
        this.SaveAsIconButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applySaveAs();
            }
        });
        
        this.AdjustButton.setSelected(true);
        this.SetEnableMainMenuToolBar(false);
    }
    
    public void SetEnableMainMenuToolBar(boolean isEnable) {
        this.AdjustButton.setDisable(!isEnable);
        this.EffctsButton.setDisable(!isEnable);
        this.SelectButton.setDisable(!isEnable);
        this.RotateButton.setDisable(!isEnable);
        this.CropButton.setDisable(!isEnable);
        this.TextButton.setDisable(!isEnable);
        this.ResizeButton.setDisable(!isEnable);
    }
    
    private void _openImageFile(File choosenFile) {
        this.IsFileOpenedYet.set(true);
        this._clear();
        
        ImageModel choosenImage = new ImageModel(new Image("file:" + choosenFile.getAbsolutePath()), "file:" + choosenFile.getAbsolutePath());
        this._clearFitImageSize(choosenImage);
        this.CurrentImageView.setImage(choosenImage);
        this.FileInformationButton.setText((int) choosenImage.GetWidth() + " X " + (int) choosenImage.GetHeight());
        this.RotatePaneForPhoto.StartFeatures();
        this.ResizePaneForPhoto.SetCurrentImageSize(choosenImage.GetWidth(), choosenImage.GetHeight());
    }
    
    public void OpenImageFile(ImageModel choosenImage) {
        this._clear();
        this._clearFitImageSize(choosenImage);
        this.CurrentImageView.setImage(choosenImage);
    }
    
    private void _clear() {
        this.CurrentImageView.setImage(null);
        this.SetScale(1);
        this.FileInformationButton.setText("");
        this.CropPaneForPhoto.CurrentCroppingRegion.ClearSettings();
        this.TextPaneForPhoto.ApplyRemoveAllTexts();
    }
    
    private void _clearFitImageSize(ImageModel image) {
        this.SetScale(1);
        this.CurrentImageView.setFitWidth(image.GetWidth());
        this.CurrentImageView.setFitHeight(image.GetHeight());
    }
    
    public ImageModel GetCurrentImage() {
        return ((ImageModel) this.CurrentImageView.getImage());
    }
    
    public void SetScale(double scale) {
        this.CurrentImageView.setScaleX(scale);
        this.CurrentImageView.setScaleY(scale);
        
        if (this.GetCurrentImage() != null) {
            StackPane basePane = (StackPane) this.ImageScrollPane.getContent();
            basePane.setMinSize(this.GetCurrentImage().GetWidth()  * scale, this.GetCurrentImage().GetHeight() * scale);

            this.ImageScrollPane.setFitToWidth(true);
            this.ImageScrollPane.setFitToHeight(true);
            
            this.CropPaneForPhoto.CurrentCroppingRegion.UpdateToDefaultSize();
            
            for (Node node : this.ImageViewStackPane.getChildren()) {
                if (node instanceof TextElement) {
                    TextElement currentTextElement = (TextElement) node;
                    
                    currentTextElement.setScaleX(scale);
                    currentTextElement.setScaleY(scale);
                }
            }
        } 
    }
    
    private double _getScale() {
        return this.CurrentImageView.getScaleX();
    }
    
    private String _getCurrentImageFileName() {
        String fileName = this.GetCurrentImage().GetFileLocation();
        fileName = fileName.substring("file:".length(), fileName.length());
        
        return fileName;
    }
    
    private File _getCurrentDirectoryFromCurrentImage() {
        String fileName = this._getCurrentImageFileName();
        File previousImageFile = new File(fileName);

        String currentDirectoryPath = previousImageFile.getParent();
        File currentdirectory = new File(currentDirectoryPath);
        
        return currentdirectory;
    }
    
    private ArrayList<File> _getOnlyImageFilesFromDirectory(File directory) {
        if (directory.isDirectory()) {
            ArrayList<File> listOfImages = new ArrayList<>();
            
            File[] files = directory.listFiles();
            
            for (File file : files) {
                if (file.getPath().toLowerCase().endsWith(".jpg") ||
                    file.getPath().toLowerCase().endsWith(".png") ||
                    file.getPath().toLowerCase().endsWith(".bmp") ||
                    file.getPath().toLowerCase().endsWith(".gif")) {
                    
                    listOfImages.add(file);
                }
            }
            
            return listOfImages;
        }
        
        return null;
    }
    
    private File _getNextImageFile() {
        File currentdirectory = this._getCurrentDirectoryFromCurrentImage();
        String fileName = this._getCurrentImageFileName();
        ArrayList<File> fileNames = this._getOnlyImageFilesFromDirectory(currentdirectory);

        if (fileNames != null) {
            for (int i = 0; i < fileNames.size(); i++) {
                File file = fileNames.get(i);

                if (file.getAbsolutePath().equals(fileName)) {
                    if (i == fileNames.size() - 1) {
                        return null;
                    }
                    else {
                        return fileNames.get(i + 1);
                    }
                }
            }
        }
        
        return null;
    }
    
    private File _getPreviousImageFile() {
        File currentdirectory = this._getCurrentDirectoryFromCurrentImage();
        String fileName = this._getCurrentImageFileName();
        ArrayList<File> fileNames = this._getOnlyImageFilesFromDirectory(currentdirectory);

        if (fileNames != null) {
            for (int i = 0; i < fileNames.size(); i++) {
                File file = fileNames.get(i);

                if (file.getAbsolutePath().equals(fileName)) {
                    if (i == 0) {
                        return null;
                    }
                    else {
                        return fileNames.get(i - 1);
                    }
                }
            }
        }
        
        return null;
    }
    
    public boolean IsFileOpened() {
        return (this.GetCurrentImage() != null);
    }
    
    private void _setScrollToCenter() {
        this.ImageScrollPane.setHvalue(0.5);
        this.ImageScrollPane.setVvalue(0.5);
    }
    
    private void _openAnImageFile() {
        File choosenFile = this.OpenImageFileChooser.showOpenDialog(CommonTools.PRIMARY_STAGE);

        if (choosenFile != null) {
            this.AdjustPaneForPhoto.ClearAdjustOptions();
            this.EffectsPaneForPhoto.ClearEffectOptions();
            this.EffectsPaneForPhoto.ClearSettings();
            this._openImageFile(choosenFile);
            this.AdjustPaneForPhoto.ClearResetImage();
            this._resetAfterLoadingImage();
        }
    }
    
    private void _applyFitToScreen() {
        if (this.IsFileOpened()) {
            this._clearFitImageSize(this.GetCurrentImage());

            double viewportHeight = this.ImageScrollPane.getViewportBounds().getHeight();
            double viewportWidth = this.ImageScrollPane.getViewportBounds().getWidth();

            double originalHeight = this.GetCurrentImage().GetHeight();
            double originalWidth = this.GetCurrentImage().GetWidth();

            double newWidth = 0;
            double newHeight = 0;

            double size = 0;

            if (originalWidth > originalHeight) {
                size = Math.max(viewportWidth, viewportHeight);
                newWidth = size;
                newHeight = (originalHeight / originalWidth) * newWidth;
            }
            else {
                size = Math.min(viewportWidth, viewportHeight);
                newHeight = size;
                newWidth = (originalWidth / originalHeight) * newHeight;
            }

            this.CurrentImageView.setFitWidth(newWidth);
            this.CurrentImageView.setFitHeight(newHeight);
            
            StackPane basePane = (StackPane) this.ImageScrollPane.getContent();
            basePane.setMinSize(newWidth, newHeight);
        }
    }
    
    public void ApplyActualSize() {
        if (this.IsFileOpened()) {
            this._clearFitImageSize(this.GetCurrentImage());
            this.ImageScaleProperty.set(100);
        }
    }
    
    private void _applyZoomOut() {
        if (this.IsFileOpened() && !this.SelectButton.isSelected()) {
            if ((this._getScale() * 100) >= MIN_SCALE + ZOOM_STEP) {
                this.ImageScaleProperty.set((this._getScale() * 100) - ZOOM_STEP);
            }
        }
    }
    
    private void _applyZoomIn() {
        if (this.IsFileOpened() && !this.SelectButton.isSelected()) {
            if ((this._getScale() * 100) <= MAX_SCALE - ZOOM_STEP) {
                this.ImageScaleProperty.set((this._getScale() * 100) + ZOOM_STEP);
            }
        }
    }
    
    private void _applyZoomSpinner(Double newValue) {
        if (this.IsFileOpened() && !this.SelectButton.isSelected()) {
            this.ImageScaleProperty.set(newValue);
        }
    }
    
    private void _applyZoomSlider(Double newValue) {
        if (this.IsFileOpened() && !this.SelectButton.isSelected()) {
            double scale = newValue.doubleValue() / 100;
            this.ZoomingSpinner.getValueFactory().valueProperty().setValue(newValue.doubleValue());
            this.SetScale(scale);
            _setScrollToCenter();
        }
    }
    
    private void _applyGrabImage(Point2D mousePosition) {
        if (this.GrabToggleButton.isSelected() && this.IsFileOpened()) {
            double differenceX = Math.abs(mousePosition.getX() - this._grabHandPosition.getX()) / 1000;
            double hvalue = ImageScrollPane.getHvalue() + ((mousePosition.getX() - this._grabHandPosition.getX() > 0) ? -differenceX : differenceX);

            if ((hvalue >= 0) && (hvalue <= 1)) {
                this.ImageScrollPane.setHvalue(hvalue);
            }

            double differenceY = Math.abs(mousePosition.getY() - this._grabHandPosition.getY()) / 1000;
            double vvalue = this.ImageScrollPane.getVvalue() + ((mousePosition.getY() - this._grabHandPosition.getY() > 0) ? -differenceY : differenceY);

            if ((vvalue >= 0) && (vvalue <= 1)) {
                this.ImageScrollPane.setVvalue(vvalue);
            }
        }
    }
    
    private void _applyPreviousFile() {
        if (this.IsFileOpened()) {
            File previousImageFile = this._getPreviousImageFile();

            if (previousImageFile != null) {
                this._openImageFile(previousImageFile);
            }
        }
    }
    
    private void _applyNextFile() {
        if (this.IsFileOpened()) {
            File nextImageFile = this._getNextImageFile();

            if (nextImageFile != null) {
                this._openImageFile(nextImageFile);
            }
        }
    }
    
    private void _applyZoomingOnScroll(double deltaY) {
        if (deltaY > 0) {
            this._applyZoomIn();
        }
        else if (deltaY < 0) {
            this._applyZoomOut();
        }
    }
    
    private void _applyDeleteCurrentImageFile() {
        if (this.IsFileOpened()) {
            this.ImageFileDeleteAlert.setContentText("The file \"" + (new File(this._getCurrentImageFileName())).getName() + "\"will be deleted from disk. Are you sure you want to continue?");
            Optional<ButtonType> result = this.ImageFileDeleteAlert.showAndWait();
            
            if (result.isPresent() && (result.get() == ButtonType.OK)) {
                // Delete file
                try {
                    Files.delete((new File(this._getCurrentImageFileName())).toPath());
                    this._clear();
                }
                catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        }
    }
    
    private void _applyViewOriginalImage(boolean isMouseClicked) {
        if (isMouseClicked) {
            this._modifiedImage = this.GetCurrentImage();
            File choosenImage = new File(this._getCurrentImageFileName());
            this._openImageFile(choosenImage);
            this._applyFitToScreen();
        }
        else {
            this.OpenImageFile(this._modifiedImage);
            this.FileInformationButton.setText((int) this._modifiedImage.GetWidth() + " X " + (int) this._modifiedImage.GetHeight());
        }
    }
    
    private void _applyShowFileInfrmationDialog() {
        if (this.IsFileOpened()) {
            FileInformationDialog dialog = new FileInformationDialog(this.GetCurrentImage());
            dialog.showAndWait();
        }
    }
    
    private void _setMainToolBarPane(MainToolBarPane pane) {
        this._mainToolBarScrollPane.setContent(pane);
        this._mainToolBarScrollPane.setFitToWidth(true);
        this._mainToolBarScrollPane.setFitToHeight(true);
    } 
    
    public boolean IsImageScaled() {
        return ((this.CurrentImageView.getScaleX() != 1) ||
                (this.CurrentImageView.getScaleY() != 1));
    }
    
    private void _resetAfterLoadingImage() {
        this.CropPaneForPhoto.ApplyReset();
        this.AdjustPaneForPhoto.ClearSettings();
        this.EffectsPaneForPhoto.ClearSettings();
    }
    
    public double GetCurrentZoomLevel() {
        return (this.ZoomingSlider.getValue() / 100.0);
    }
    
    public void Add(Node node) {
        this.ImageViewStackPane.getChildren().add(node);
    }
    
    public void Remove(Node node) {
        this.ImageViewStackPane.getChildren().remove(node);
    }
    
    public void DeselctAllTextElement() {
        for (Node node : this.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                currentTextElement.SetDeselect();
            }
        }
    }
    
    private void _applyOnImageStackPanePressed(MouseEvent event) {
        this.DeselctAllTextElement();
        
        for (Node node : this.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                
                Bounds currentTextElementLocalBounds = currentTextElement.localToScreen(currentTextElement.getBoundsInLocal());
                
                if ((event.getScreenX() >= currentTextElementLocalBounds.getMinX()) && 
                    (event.getScreenX() <= currentTextElementLocalBounds.getMinX() + currentTextElementLocalBounds.getWidth()) &&
                    (event.getScreenY() >= currentTextElementLocalBounds.getMinY()) && 
                    (event.getScreenY() <= currentTextElementLocalBounds.getMinY() + currentTextElementLocalBounds.getHeight())) {
                    
                    currentTextElement.SetSelect();
                    
                    break;
                }
            }
        }
        
        // Rectangle Selection
        if (this.IsRectangleSelectionModeEnabled()) {
            this.AddPreviewSelectionRectangle();
            this.SetRectangleSelectionPreviewPosition(event);
            this.SetRectangleSelectionPreviewSize(0, 0);
            this._rectangleSelectionPreviewPreviousX = event.getX();
            this._rectangleSelectionPreviewPreviousY = event.getY();
        }
    }
    
    private void _applyOnImageStackPaneDragged(MouseEvent event) {
        // Rectangle Selection
        if (this.IsRectangleSelectionModeEnabled()) {
            this.SetRectangleSelectionPreviewSize(
                    Math.abs(event.getX() - this._rectangleSelectionPreviewPreviousX),
                    Math.abs(event.getY() - this._rectangleSelectionPreviewPreviousY)
            );

            this.SetRectangleSelectionPreviewPosition(event);
        }
    }
    
    public void AddPreviewSelectionRectangle() {
        if (!this.ImageViewStackPane.getChildren().contains(this._rectangleSelectionPreview)) {
            this.Add(this._rectangleSelectionPreview);
        }
    }

    public void RemovePreviewSelectionRectangle() {
        if (this.ImageViewStackPane.getChildren().contains(this._rectangleSelectionPreview)) {
            this.Remove(this._rectangleSelectionPreview);
        }
    }
    
    public boolean IsRectangleSelectionModeEnabled() {
        return (
                this.SelectButton.isSelected()                             &&
                this.IsFileOpened()                                        &&
                this.SelectPaneForPhoto.IsRectangleSelectionToolSelected() &&
                !this.GrabToggleButton.isSelected()                        &&
                !this._isAnyScrapImageSelected()
        );
    }
    
    private boolean _isAnyScrapImageSelected() {
        boolean isFound = false;
        
        for (Node node : this.ImageViewStackPane.getChildren()) {
            if (node instanceof ScrapImage) {
                ScrapImage scrapImage = (ScrapImage) node;
                
                if (scrapImage.IsSelected()) {
                    isFound = true;
                    break;
                }
            }
        }
        
        return isFound;
    }
    
    public void SetRectangleSelectionPreviewPosition(MouseEvent event) {
        this._rectangleSelectionPreview.setTranslateX((-this.ImageViewStackPane.getWidth()  / 2.0) + event.getX() -  (this._rectangleSelectionPreview.getWidth()  / 2.0));
        this._rectangleSelectionPreview.setTranslateY((-this.ImageViewStackPane.getHeight() / 2.0) + event.getY() -  (this._rectangleSelectionPreview.getHeight() / 2.0));
    }
    
    public void SetRectangleSelectionPreviewSize(double width, double height) {
        this._rectangleSelectionPreview.setWidth(width);
        this._rectangleSelectionPreview.setHeight(height);
    }
    
    public boolean IsRectangleSelectionPreviewExists() {
        return this.ImageViewStackPane.getChildren().contains(this._rectangleSelectionPreview);
    }
    
    public double[] GetNodeBoundsRelativeToSourceImage(Node node, double nodeWidth, double nodeHeight, double biasX, double biasY) {
        double posX = 0;
        double posY = 0;
        double width = 0;
        double height = 0;
        
        Point2D imagePosition  = this.CurrentImageView.localToScreen(new Point2D(0, 0));
        Point2D nodeProperties = node.localToScreen(new Point2D(0, 0));

        posX = ((nodeProperties.getX() - imagePosition.getX()) / (this.GetCurrentImage().getWidth()  * this.CurrentImageView.getScaleX())) * this.GetCurrentImage().getWidth();
        posY = ((nodeProperties.getY() - imagePosition.getY()) / (this.GetCurrentImage().getHeight() * this.CurrentImageView.getScaleY())) * this.GetCurrentImage().getHeight();
        width = nodeWidth;
        height = nodeHeight;    

        posX += biasX;
        posY += biasY;

        if (posX < 0) {
            width += posX;
            posX = 0;
        }

        if (posY < 0) {
            height += posY;
            posY = 0;
        }

        if ((posX > this.GetCurrentImage().getWidth()) || (posY > this.GetCurrentImage().getHeight())) {
            width = 0;
            posX = 0;

            height = 0;
            posY = 0;
        }

        if (posX + width > this.GetCurrentImage().getWidth()) {
            width = this.GetCurrentImage().getWidth() - posX;
        }

        if (posY + height > this.GetCurrentImage().getHeight()) {
            height = this.GetCurrentImage().getHeight() - posY;
        }

        if ((width < 0) || (height < 0)) {
            posX = 0;
            posY = 0;
            width = 0;
            height = 0;
        }

        return (new double[] { 
            posX,
            posY,
            width,
            height
        });
    }
    
    public double[] GetPreviewRectangleProperties() {
        return this.GetNodeBoundsRelativeToSourceImage(this._rectangleSelectionPreview, this._rectangleSelectionPreview.getWidth(), this._rectangleSelectionPreview.getHeight(), 0, 0);
    }
    
    private Rectangle _getPreviewSelectionRectangle() {
        Rectangle preview = new Rectangle();
        preview.setStroke(SELECTION_PREVIEW_STROKE_COLOR);
        preview.setStrokeWidth(SELECTION_PREVIEW_STROKE_WIDTH);
        preview.getStrokeDashArray().add(3d);
        preview.setFill(Color.TRANSPARENT);
        
        return preview;
    }
    
    private void _applyOnImageViewMousePressed(MouseEvent event) {
        if (this.GrabToggleButton.isSelected() && this.IsFileOpened()) {
            this._grabHandPosition = new Point2D(event.getX(), event.getY());
            this.CurrentImageView.setCursor(Cursor.CLOSED_HAND);
        }
    }
    
    private void _applyOnImageViewMouseReleased(MouseEvent event) {
        if (this.GrabToggleButton.isSelected() && this.IsFileOpened()) {
            this._grabHandPosition = null;
            this.CurrentImageView.setCursor(Cursor.OPEN_HAND);
        }
        else {
            this.CurrentImageView.setCursor(Cursor.DEFAULT);
        }
    }
    
    public boolean IsBlankBounds(double[] bounds) {
        return ((bounds[2] == 0) &&
                (bounds[3] == 0)
        );
    }
    
    public void AttachScrapImageToSourceImage() {
        if (this.IsFileOpened() && !this._isAnyScrapImageSelected()) {

            ArrayList<ScrapImage> scrapImageToBeRemoved = new ArrayList<>();
            
            for (Node node : this.ImageViewStackPane.getChildren()) {
                if (node instanceof ScrapImage) {
                    ScrapImage currentScrapImage = (ScrapImage) node;
 
                    double[] scrapImageBounds = this.GetNodeBoundsRelativeToSourceImage(currentScrapImage, currentScrapImage.getWidth(), currentScrapImage.getHeight(), ScrapImage.STROKE_WIDTH, ScrapImage.STROKE_WIDTH);

                    if (!this.IsBlankBounds(scrapImageBounds)) {
                        ImageModel result = SimpleImageProcessor.GET_JOINED_IMAGE(this.GetCurrentImage(), currentScrapImage.GetImage(), scrapImageBounds);
                        this.OpenImageFile(result);
                        scrapImageToBeRemoved.add(currentScrapImage);
                    }
                }
            } 
            
            for (ScrapImage currentScrapImage : scrapImageToBeRemoved) {
                this.Remove(currentScrapImage);
            }
            
            this.RemovePreviewSelectionRectangle();
        }
    }
    
    public Image GetOutputImage() {
        ImageModel outputImage = this.GetCurrentImage();
                
        for (Node node : this.ImageViewStackPane.getChildren()) {
            if (node instanceof TextElement) {
                TextElement currentTextElement = (TextElement) node;
                currentTextElement.SetDeselect();

                SnapshotParameters snapshotParameters = new SnapshotParameters();
                snapshotParameters.setFill(Color.TRANSPARENT);

                ImageModel textAsImage = new ImageModel(currentTextElement.snapshot(snapshotParameters, null), this.GetCurrentImage().GetFileLocation());
                double[] textElementBounds = this.GetNodeBoundsRelativeToSourceImage(currentTextElement.GetTextArea(), currentTextElement.getWidth(), currentTextElement.getHeight(), TextElement.BORDER_WIDTH, TextElement.BORDER_WIDTH);

                outputImage = SimpleImageProcessor.GET_JOINED_IMAGE(outputImage, textAsImage, textElementBounds);
            }
        } 
        
        return outputImage;
    }
    
    private String _getImageFormat(FileChooser fileChooser) {
        return this._saveAsFileChooser.getSelectedExtensionFilter().getExtensions().get(0).split("\\.")[1];
    }
    
    private void _saveImage(File choosenFile, String imageFormat) {
        Image output = this.GetOutputImage();
                
        try {
            // JPG
            if (imageFormat.equals("jpg")) {
                // Get buffered image:
                BufferedImage image = SwingFXUtils.fromFXImage(output, null); 

                // Remove alpha-channel from buffered image:
                BufferedImage imageRGB = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.OPAQUE);
                Graphics2D graphics = imageRGB.createGraphics();

                graphics.drawImage(
                    image, 
                    0, 
                    0, 
                    null
                );

                ImageIO.write(
                    imageRGB, 
                    "jpg", 
                    choosenFile
                );

                graphics.dispose();
            }
            // PNG, GIF
            else {
                ImageIO.write(SwingFXUtils.fromFXImage(output, null), imageFormat, choosenFile);
            }
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
    }
    
    private void _applySave() {
        if (this.IsFileOpened()) {
            String fileName = this.GetCurrentImage().GetFileLocation().substring("file:".length(), this.GetCurrentImage().GetFileLocation().length());
            String extension = fileName.substring(fileName.length() - 3, fileName.length()).toLowerCase();

            if (extension.equals("bmp")) {
                extension = "png";
                fileName = fileName.substring(0, fileName.length() - 3);
                fileName += extension;
            }

            this._saveImage(new File(fileName), extension);
        }
    }
    
    private void _applySaveAs() {
        if (this.IsFileOpened()) {
            File choosenFile = this._saveAsFileChooser.showSaveDialog(CommonTools.PRIMARY_STAGE);
            
            if (choosenFile != null) {
                this._saveImage(choosenFile, this._getImageFormat(this._saveAsFileChooser));
            }
        }
    }
}
