package zunayedhassan.ImageEditorFX;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import zunayedhassan.CommonUI.FontComboBoxElement;
import zunayedhassan.CommonUI.IconButton;

/**
 *
 * @author Zunayed Hassan
 */
public class TextElement extends GridPane {
    public static final String DEFAULT_TEXT_COLOR = "#FFFFFF";
    public static final double DEFAULT_FONT_SIZE = 14;
    public static final double BORDER_WIDTH = 2;
    
    public double Rotation = 0.0;
    public Color FillColor = Color.WHITE;
    public Color OutlineColor = Color.WHITE;
    
    private MainView _parent = null;
    private IconButton _closeButton = new IconButton("icons/emblem-dropbox-unsyncable_16.png");
    private IconButton _resizeButton = new IconButton("icons/transform-move-horizontal_16.png");
    private TextArea _textArea = new TextArea();
    private boolean _isSelected = false;
    private Border _selectBorder = new Border(new BorderStroke(Color.WHITE, BorderStrokeStyle.DASHED, CornerRadii.EMPTY, new BorderWidths(BORDER_WIDTH)));
    private Border _deselectBorder = new Border(new BorderStroke(Color.TRANSPARENT, BorderStrokeStyle.DASHED, CornerRadii.EMPTY, new BorderWidths(BORDER_WIDTH)));
    private boolean _isBold = false;
    private boolean _isItalic = false;
    private boolean _isUnderline = false;
    private boolean _isStrikethrough = false;
    
    
    double orgSceneX = 0;
    double orgSceneY = 0;
    double orgTranslateX = 0;
    double orgTranslateY = 0;
    
    double previousWidth = 0;
    double previousHeight = 0;
    
    public TextElement(MainView parent) {
        this(parent, "Click here to edit text...");
    }
    
    public TextElement(MainView parent, String text) {
        this._parent = parent;
        this._textArea.setText(text);
        
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeLayout() {        
        this.add(this._closeButton, 0, 0);
        this.add(this._textArea, 1, 1);
        this.add(this._resizeButton, 2, 2);
        
        this._closeButton.setPickOnBounds(false);
        this._resizeButton.setPickOnBounds(false);
        
        this._closeButton.getStyleClass().add("text-element-button");
        this._resizeButton.getStyleClass().add("text-element-button");
        
        this._closeButton.setCursor(Cursor.DEFAULT);
        this._resizeButton.setCursor(Cursor.DEFAULT);
        
        this._textArea.getStyleClass().add("text-element-transparent");
        this._textArea.setWrapText(true);

        this.setMaxWidth(256);
        this.setMaxHeight(40);
        this.setCursor(Cursor.MOVE);
        
        this.SetTextColor(DEFAULT_TEXT_COLOR);
        this.SetFontSize(DEFAULT_FONT_SIZE);
        this.SetOutline(Color.WHITE, 0);
    }
    
    private void _initializeEvents() {
        this.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _onMousePressed(event);
            }
        });
        
        this.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _onMouseDragged(event);
            }
        });
        
        this.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _onMouseReleased(event);
            }
        });
        
        this._textArea.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyOnTextAreaMouseClicked(event);
            }
        });
        
        // Close Button
        this._closeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyOnClose();
            }
        });
        
        // Resize Button
        this._resizeButton.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                _applyResize(event);
            }
        });
    }
    
    private void _onMousePressed(MouseEvent event) {
        orgSceneX = event.getSceneX();
        orgSceneY = event.getSceneY();

        orgTranslateX = getTranslateX();
        orgTranslateY = getTranslateY();
    }
    
    private void _onMouseDragged(MouseEvent event) {
        double offsetX = event.getSceneX() - orgSceneX;
        double offsetY = event.getSceneY() - orgSceneY;
        double newTranslateX = orgTranslateX + offsetX;
        double newTranslateY = orgTranslateY + offsetY;
        
        this.SetPosition(newTranslateX, newTranslateY);
    }
    
    private void _onMouseReleased(MouseEvent event) {
        orgSceneX = 0;
        orgSceneY = 0;
        orgTranslateX = 0;
        orgTranslateY = 0;
    }
     
    public boolean IsSelected() {
        return this._isSelected;
    }
    
    public void SetSelect() {
        this._isSelected = true;
        this._textArea.setBorder(this._selectBorder);
        this._setButtonOpacity(1);
        
        TextPane textPane = this._parent.TextPaneForPhoto;
        
        // Font Family
        String fontFamily = this._textArea.getFont().getFamily();
        ObservableList<FontComboBoxElement> listOfFontElements = textPane.FontsComboBox.getItems();
        int fontFamilyIndex = 0;
        
        for (FontComboBoxElement fontComboBoxElement : listOfFontElements) {
            if (fontComboBoxElement.FontFamilyName.equals(fontFamily)) {
                break;
            }
            
            fontFamilyIndex++;
        }
        
        textPane.FontsComboBox.getSelectionModel().select(fontFamilyIndex);
        
        // Font Size
        double fontSize = this._textArea.getFont().getSize();
        ObservableList<Integer> listOfFontSizes = textPane.FontSizeComboBox.getItems();
        int fontSizeIndex = 0;
        
        for (Integer size : listOfFontSizes) {
            if (fontSize == size) {
                break;
            }
            
            fontSizeIndex++;
        }
        
        textPane.FontSizeComboBox.getSelectionModel().select(fontSizeIndex);
        
        // Bold
        textPane.BoldButton.setSelected(this._isBold);
        
        // Italic
        textPane.ItalicButton.setSelected(this._isItalic);
        
        // Underline
        textPane.UnderlineButton.setSelected(this._isUnderline);
        
        // Strikethrough
        textPane.StrikethroughButton.setSelected(this._isStrikethrough);
        
        // Text Rotation
        textPane.TextRotationSlider.setValue(this.getRotate());
        textPane.TextRotationSpinner.getValueFactory().setValue(this.getRotate());
        
        // Text Fill
        textPane.FillColorPreview.setFill(this._getColorFromCSS("-fx-text-fill"));
        
        Node textNode = this._textArea.lookup(".text");

        if (textNode != null) {
            // Text Outline Color
            Color textOutlineColor = (Color) ((Text) textNode).getStroke();
            
            if (textOutlineColor != null) {
                textPane.OutlineColorPreview.setFill(textOutlineColor);
            
                // Text Outline Width
                double width = ((Text) textNode).getStrokeWidth();
                textPane.OutlineWidthComboBox.getSelectionModel().select((int) width);
            }
        }

        // Opacity
        textPane.OpacitySlider.setValue(this._textArea.getOpacity() * 100);
        textPane.OpacitySpinner.getValueFactory().setValue(this._textArea.getOpacity() * 100);
    }
    
    public void SetDeselect() {
        this._isSelected = false;
        this._textArea.setBorder(this._deselectBorder);
        this._setButtonOpacity(0);
    }
    
    private void _applyOnTextAreaMouseClicked(MouseEvent event) {
        this._parent.DeselctAllTextElement();
        this.SetSelect();
    }
    
    private void _setButtonOpacity(double opacity) {
        this._closeButton.setOpacity(opacity);
        this._resizeButton.setOpacity(opacity);
    }
    
    private void _applyOnClose() {
        if (this.IsSelected()) {
            this._parent.ImageViewStackPane.getChildren().remove(this);
        }
    }
    
    private void _applyResize(MouseEvent event) {
        if (this.IsSelected()) {
            double width = this._textArea.getMinWidth() + (event.getX() - previousWidth);
            double height = this._textArea.getMinHeight() + (event.getY() - previousHeight);
            
            this._textArea.setMinSize(width, height);
            this.setMaxSize(this.getMaxWidth() + (event.getX() - previousWidth), height + (event.getY() - previousHeight));
            
            previousWidth = event.getX();
            previousHeight = event.getY();
            
            ScrollBar verticalScrollBar = (ScrollBar) this._textArea.lookup(".scroll-bar:vertical");
            verticalScrollBar.setStyle("-fx-opacity: 0;");
        }
    }
    
    public void SetPosition(double x, double y) {
        this.setTranslateX(x);
        this.setTranslateY(y);
    }
    
    public void SetTextColor(String hexColor) {
        this._textArea.setStyle("-fx-text-fill: " + hexColor + ";");
    }
    
    public void SetFontSize(double newFontSize) {
        String fontFamily = this._textArea.getFont().getFamily();
        double fontSize = this._textArea.getFont().getSize();
        
        Font newFont = Font.font(
                fontFamily,
                (this._isBold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this._isItalic ? FontPosture.ITALIC : FontPosture.REGULAR),
                newFontSize
        );
        
        this._textArea.setFont(newFont);
    }
    
    public void SetFontFamily(String newFontFamily) {
        String fontFamily = this._textArea.getFont().getFamily();
        double fontSize = this._textArea.getFont().getSize();
        
        Font newFont = Font.font(
                newFontFamily,
                (this._isBold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this._isItalic ? FontPosture.ITALIC : FontPosture.REGULAR),
                fontSize
        );
        
        this._textArea.setFont(newFont);
    }
    
    public void SetBold(boolean isBold) {
        this._isBold = isBold;
        
        String fontFamily = this._textArea.getFont().getFamily();
        double fontSize = this._textArea.getFont().getSize();
        
        Font newFont = Font.font(
                fontFamily,
                (this._isBold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this._isItalic ? FontPosture.ITALIC : FontPosture.REGULAR),
                fontSize
        );
        
        this._textArea.setFont(newFont);
    }
    
    public void SetItalic(boolean isItalic) {
        this._isItalic = isItalic;
        
        String fontFamily = this._textArea.getFont().getFamily();
        double fontSize = this._textArea.getFont().getSize();
        
        Font newFont = Font.font(
                fontFamily,
                (this._isBold ? FontWeight.BOLD : FontWeight.NORMAL),
                (this._isItalic ? FontPosture.ITALIC : FontPosture.REGULAR),
                fontSize
        );
        
        this._textArea.setFont(newFont);
    }
    
    public void SetUnderline(boolean isUnderline) {
        this._isUnderline = isUnderline;

        if (this._isUnderline) {
            this._textArea.getStyleClass().remove("text-area-no-underline");
            this._textArea.getStyleClass().add("text-area-underline");
        }
        else {
            this._textArea.getStyleClass().add("text-area-no-underline");
            this._textArea.getStyleClass().remove("text-area-underline");
        }
    }
    
    public void SetStrikethrough(boolean isStrikethrough) {
        this._isStrikethrough = isStrikethrough;

        if (this._isStrikethrough) {
            this._textArea.getStyleClass().remove("text-area-no-strikethrough");
            this._textArea.getStyleClass().add("text-area-strikethrough");
        }
        else {
            this._textArea.getStyleClass().add("text-area-no-strikethrough");
            this._textArea.getStyleClass().remove("text-area-strikethrough");
        }
    }
    
    private void _removeAllAlignmentClasses() {
        this._textArea.getStyleClass().removeAll(
                "text-alignment-left",
                "text-alignment-center",
                "text-alignment-justify",
                "text-alignment-right"
        );
    }
    
    public void SetAlignment(TextAlignment textAlignment) {
        this._removeAllAlignmentClasses();
        
        switch (textAlignment) {
            case LEFT:
                this._textArea.getStyleClass().add("text-alignment-left");
                break;
                
            case CENTER:
                this._textArea.getStyleClass().add("text-alignment-center");
                break;
                
            case JUSTIFY:
                this._textArea.getStyleClass().add("text-alignment-justify");
                break;
                
            case RIGHT:
                this._textArea.getStyleClass().add("text-alignment-right");
                break;
        }
    }
    
    public void SetColor(Color color) {
        this.FillColor = color;
        this._textArea.setStyle("-fx-text-fill: " + this._toRgbString(color) + ";");
    }

    private String _toRgbString(Color c) {
        return "rgb("
                          + _to255Int(c.getRed())
                    + "," + _to255Int(c.getGreen())
                    + "," + _to255Int(c.getBlue())
             + ")";
    }

    private int _to255Int(double d) {
        return (int) (d * 255);
    }
    
    public void SetOutline(Color color, double width) {
        this.OutlineColor = color;
        Node textNode = this._textArea.lookup(".text");
        
        if (textNode != null) {
            textNode.setStyle("-fx-stroke: " + this._toRgbString(color) + "; -fx-stroke-width: " + Double.toString(width) + ";");
        }
    }
    
    public void SetOpacity(double opacity) {
        this._textArea.setOpacity(opacity);
    }
    
    private Color _getColorFromCSS(String element) {
        element += ":";
        
        Color color = Color.BLACK;
        
        if (this._textArea.getStyle().split(element)[1].contains("rgb")) {
            String[] rgbFillColor = this._textArea.getStyle().split(element)[1].trim().substring(4, this._textArea.getStyle().split(element)[1].trim().length() - 2).split(",");
            color = Color.rgb(Integer.parseInt(rgbFillColor[0]), Integer.parseInt(rgbFillColor[1]), Integer.parseInt(rgbFillColor[2]));
            
        }
        else {
            color = Color.web(this._textArea.getStyle().split(element)[1].trim().split(";")[0]);
        }
        
        return color;
    }
    
    public TextArea GetTextArea() {
        return this._textArea;
    }
}