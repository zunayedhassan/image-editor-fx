package zunayedhassan.ImageEditorFX;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.stage.Screen;
import javafx.util.StringConverter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import zunayedhassan.CommonTools;
import zunayedhassan.ImageProcessor.SimpleImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class ResizePane extends MainToolBarPane {
    private Label _imageSizeLabel               = new Label();
    private ComboBox<String> _fileSizeComboBox  = new ComboBox<>();
    private ComboBox<String> _unitComboBox      = new ComboBox<>();
    private Spinner<Double> _widthSpinner       = new Spinner<>(0, Integer.MAX_VALUE, 100);
    private Spinner<Double> _heightSpinner      = new Spinner<>(0, Integer.MAX_VALUE, 100);
    private CheckBox _constrainProportions      = new CheckBox("Constrain proportions");
    private Button _applyButton                 = new Button("Apply");
    private Button _cancelButton                = new Button("Cancel");
    private GridPane _dimensionGridPane         = null;
    private boolean _isSetUnitWithoutConverting = false;
    
    
    public ResizePane(MainView parent) {
        super("Resize", parent);
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeLayout() {
        this.getStyleClass().add("common-ui-toolbar-gradient");
        
        HBox currentImageSizePane = new HBox();
        currentImageSizePane.getChildren().addAll(
                new Label("Current Image Size: "),
                this._imageSizeLabel
        );
        
        super.Add(currentImageSizePane);
        super.Add(new Label("Fit into: "));
        super.Add(this._fileSizeComboBox);
        
        this._fileSizeComboBox.getItems().addAll(
                "Custom",
                "640 X 480 px",
                "800 X 600 px",
                "1024 X 768 px",
                "1280 X 800 px",
                "1280 X 960 px",
                "1680 X 1050 px",
                "1920 X 1200 px",
                "2048 X 1536 px",
                "3200 X 2400 px",
                this._getSeperator(),
                "4 X 3 cm",
                "13 X 9 cm",
                "15 X 10 cm",
                "18 X 13 cm",
                "20 X 15 cm",
                "25 X 20 cm",
                "30 X 20 cm",
                "45 X 30 cm",
                this._getSeperator(),
                "3 X 2 inches",
                "6 X 4 inches",
                "7 X 5 inches",
                "8 X 6 inches",
                "10 X 8 inches",
                "12 X 8 inches",
                this._getSeperator(),
                "A7 (10.5 X 7.4 cm)",
                "A6 (14.8 X 10.5 cm)",
                "A5 (21.0 X 14.8 cm)",
                "A4 (29.7 X 21.0 cm)",
                this._getSeperator(),
                "iPad Air - 2048 X 1536 px",
                "iPad Mini - 1024 X 768 px",
                "iPhone 5 - 1136 X 640 px",
                "iPhone 4 - 960 X 640 px"
        );
        
        this._dimensionGridPane = new GridPane();
        this._dimensionGridPane.setHgap(10);
        this._dimensionGridPane.setVgap(5);
        
        this._widthSpinner.setEditable(true);
        this._heightSpinner.setEditable(true);
        
        this._dimensionGridPane.add(new Label("Width"), 0, 0);
        this._dimensionGridPane.add(this._widthSpinner, 1, 0);
        this._dimensionGridPane.add(new Label("Height"), 0, 1);
        this._dimensionGridPane.add(this._heightSpinner, 1, 1);
        this._dimensionGridPane.add(new Label("Unit"), 0, 2);
        this._dimensionGridPane.add(this._unitComboBox, 1, 2);
        
        this._unitComboBox.getItems().addAll(
                "px",
                "inches",
                "cm",
                "mm",
                "points"
        );
        
        HBox buttonsPane = new HBox(5, this._applyButton, this._cancelButton);
        buttonsPane.setAlignment(Pos.CENTER);
        
        super.Add(this._dimensionGridPane);
        super.Add(super.GetSeperator());
        super.Add(this._constrainProportions);
        super.Add(super.GetSeperator());
        super.Add(buttonsPane);
        
        this._resizeFileSizeCombobox(195);
        this._fileSizeComboBox.getSelectionModel().select(0);
        this._unitComboBox.getSelectionModel().select(0);
    }
    
    private void _initializeEvents() {
        this._dimensionGridPane.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _resizeFileSizeCombobox(newValue.doubleValue());
            }
        });
        
        this._widthSpinner.setValueFactory(new DoubleSpinnerValueFactory(1, Integer.MAX_VALUE, 100, 0.1));
        
        this._widthSpinner.getValueFactory().setConverter(new StringConverter<Double>() {
            @Override
            public String toString(Double object) {
                return Double.toString(GET_DOUBLE_ROUNDED(object, 2));
            }

            @Override
            public Double fromString(String string) {
                return Double.parseDouble(string);
            }
        });
        
        this._heightSpinner.setValueFactory(new DoubleSpinnerValueFactory(1, Integer.MAX_VALUE, 100, 0.1));
        
        this._heightSpinner.getValueFactory().setConverter(new StringConverter<Double>() {
            @Override
            public String toString(Double object) {
                return Double.toString(GET_DOUBLE_ROUNDED(object, 2));
            }

            @Override
            public Double fromString(String string) {
                return Double.parseDouble(string);
            }
        });
        
        this._widthSpinner.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean isFocused) {
                _widthSpinner.getValueFactory().setValue(Double.parseDouble(_widthSpinner.getEditor().getText().trim()));
            }
        });
        
        this._heightSpinner.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean isFocused) {
                _heightSpinner.getValueFactory().setValue(Double.parseDouble(_heightSpinner.getEditor().getText().trim()));
            }
        });
             
        // Preset Sizes
        this._fileSizeComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.equals(_getSeperator())) {
                    _isSetUnitWithoutConverting = true;
                    _setOnUserChoosesPreset(newValue);
                }
            }
        });
        
        // Unit
        this._unitComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!_isSetUnitWithoutConverting) {
                    _convertUnit(newValue, oldValue);
                }
                
                _isSetUnitWithoutConverting = false;
            }
        });
        
        // Apply
        this._applyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (parent.IsFileOpened()) {
                    _applyResize();
                }
            }
        });
    }
    
    public void SetCurrentImageSize(double width, double height) {
        this._imageSizeLabel.setText((int) width + " X " + (int) height + " px");
    }
    
    private String _getSeperator() {
        return "\t";
    }
    
    private boolean _isDigit(String test) {
        return test.matches("\\d+");
    }
    
    private void _resizeFileSizeCombobox(double width) {
        this._fileSizeComboBox.setMinWidth(width);
        this._fileSizeComboBox.setPrefWidth(width);
    }
    
    private void _setOnUserChoosesPreset(String text) {
        this._constrainProportions.setSelected(false);
        
        double width = -1;
        double height = -1;
        String unit = "";

        if (!text.contains("Custom")) {
            if (text.contains("(")) {
                text = text.split("\\(")[1];
                text = text.split("\\)")[0];
            }
            else if (text.contains("-")) {
                text = text.split("-")[1];
            }

            String[] result = text.split(" ");

            for (String part : result) {
                if (this._isDigit(part) || part.contains(".")) {
                    if (width == -1) {
                        width = Double.parseDouble(part);
                    }
                    else {
                        height = Double.parseDouble(part);
                    }
                }
            }

            unit = result[result.length - 1];

            this._widthSpinner.getValueFactory().setValue(width);
            this._heightSpinner.getValueFactory().setValue(height);
            
            if (unit.trim().toLowerCase().equals("cm")) {
                this._unitComboBox.setValue("cm");
            }
            else if (unit.trim().toLowerCase().equals("inches")) {
                this._unitComboBox.setValue("inches");
            } 
            else if (unit.trim().toLowerCase().equals("px")) {
                this._unitComboBox.setValue("px");
            }
        }
        // Custom
        else {
            this._unitComboBox.setValue(this._unitComboBox.getItems().get(0));
        }
    }
    
    private void _convertUnit(String targetUnit, String oldUnit) {
        targetUnit = targetUnit.trim().toLowerCase();

        if (this._fileSizeComboBox.getSelectionModel().getSelectedItem() != null) {
            String preset = this._fileSizeComboBox.getSelectionModel().getSelectedItem().trim().toLowerCase();
        
            String presetUnit = "";

            if (preset.contains("cm")) {
                presetUnit = "cm";
            }
            else if (preset.contains("inches")) {
                presetUnit = "inches";
            }
            else if (preset.contains("px")) {
                presetUnit = "px";
            }
            else if (preset.contains("mm")) {
                presetUnit = "mm";
            }

            // If unit doesn't match with the unit of the preset, then convert
            if ((!targetUnit.equals(presetUnit) || !oldUnit.equals(targetUnit)) && !oldUnit.trim().toLowerCase().equals("custom")) {
                double widthInInches  = this._getUnitToInches(oldUnit, this._widthSpinner.getValue().doubleValue());
                double heightInInches = this._getUnitToInches(oldUnit, this._heightSpinner.getValue().doubleValue());

                double convertedWidth  = this._getConvertedValueFromInches(targetUnit, widthInInches);
                double convertedHeight = this._getConvertedValueFromInches(targetUnit, heightInInches);
                
                if (this._constrainProportions.isSelected()) {
                    double originalWidth  = super.parent.GetCurrentImage().getWidth();
                    double originalHeight = super.parent.GetCurrentImage().getHeight();
                    
                    convertedHeight = (originalHeight / originalWidth) * convertedWidth;
                }
                
                this._widthSpinner.getValueFactory().setValue(convertedWidth);
                this._heightSpinner.getValueFactory().setValue(convertedHeight);
            }
        }
    }
    
    public static double GET_DPI() {
        return Screen.getPrimary().getDpi();
    }
    
    private double _getUnitToInches(String unit, double value) {
        double dpi = GET_DPI();
        double coefficient = 1.0;
                
        if (unit.equals("px")) {
            coefficient = 1.0 / dpi;
        }
        else if (unit.equals("cm")) {
            coefficient = 1.0 / 2.54;
        }
        else if (unit.equals("inches")) {
            coefficient = 1.0;
        }
        else if (unit.equals("mm")) {
            coefficient = 1.0 / (2.54 * 10.0);
        }
        else if (unit.equals("points")) {
            coefficient = 1.0 / 72.0;
        }
        
        double result = (coefficient * value);
        
        return result;
    }
    
    private double _getConvertedValueFromInches(String unit, double valueInInches) {
        double coefficient = 1.0;
        double dpi = GET_DPI();

        // Convert inches to new unit
        if (unit.equals("px")) {
            coefficient = dpi;
        }
        else if (unit.equals("cm")) {
            coefficient = 2.54;
        }
        else if (unit.equals("inches")) {
            coefficient = 1.0;
        }
        else if (unit.equals("mm")) {
            coefficient = 2.54 * 10.0;
        }
        else if (unit.equals("points")) {
            coefficient = 72.0;
        }
        
        double result = (coefficient * valueInInches);
        
        return result;
    }
    
    public static double GET_DOUBLE_ROUNDED(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bigDecimal = new BigDecimal(value);
        bigDecimal = bigDecimal.setScale(places, RoundingMode.HALF_UP);
        return bigDecimal.doubleValue();
    }
    
    private void _applyResize() {
        int targetWidth  = (int) (this._getUnitToInches(this._unitComboBox.getSelectionModel().getSelectedItem(), this._widthSpinner.getValue())  * GET_DPI());
        int targetHeight = (int) (this._getUnitToInches(this._unitComboBox.getSelectionModel().getSelectedItem(), this._heightSpinner.getValue()) * GET_DPI());

        if (this._constrainProportions.isSelected()) {
            double originalWidth  = super.parent.GetCurrentImage().getWidth();
            double originalHeight = super.parent.GetCurrentImage().getHeight();

            targetHeight = (int) ((originalHeight / originalWidth) * targetWidth);
        }
        
        ImageModel result = SimpleImageProcessor.GET_RESIZED_IMAGE(super.parent.GetCurrentImage(), targetWidth, targetHeight);

        super.parent.CurrentImageView.setImage(result);
        super.parent.ApplyActualSize();
    }
}