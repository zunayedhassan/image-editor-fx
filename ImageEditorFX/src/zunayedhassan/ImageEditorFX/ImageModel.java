package zunayedhassan.ImageEditorFX;

import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;

/**
 *
 * @author Zunayed Hassan
 */
public class ImageModel extends WritableImage {    
    private double _width = 0;
    private double _height = 0;
    private String _fileLocation = null;
    
    public ImageModel(Image image, String location) {
        super(image.getPixelReader(), (int) image.getWidth(), (int) image.getHeight());
        this._fileLocation = location;
        this._width = this.getWidth();
        this._height = this.getHeight();
    }
    
    public double GetWidth() {
        return this._width;
    }
    
    public double GetHeight() {
        return this._height;
    }
    
    public String GetFileLocation() {
        return this._fileLocation;
    }
}
