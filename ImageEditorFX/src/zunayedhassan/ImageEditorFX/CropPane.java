package zunayedhassan.ImageEditorFX;

import java.util.Arrays;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import zunayedhassan.CommonUI.IconButton;
import zunayedhassan.ImageProcessor.SimpleImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class CropPane extends MainToolBarPane {
    public static final String DEFAULT_SIZE = "Original";
    
    public CroppingRegion CurrentCroppingRegion = null;
    
    public ComboBox<String> ImageSizeComboBox = new ComboBox<>(
            FXCollections.observableArrayList(
                    "Custom",
                    DEFAULT_SIZE,
                    "1:1 (Square)",
                    "3:2 (iPhone)",
                    "4:3 (DVD)",
                    "6:4 (Postcard)",
                    "5:4 (Photo)",
                    "7:5 (L, 2L)",
                    "10:8 (Photo)",
                    "16:9 (Widescreen)"
            )
    );
    
    public CheckBox LockAspectRatioCheckBox = new CheckBox("Lock aspect ratio");
    
    private Spinner<Integer> _widthSpinner = new Spinner<>(0, Integer.MAX_VALUE, 100);
    private Spinner<Integer> _heightSpinner = new Spinner<>(0, Integer.MAX_VALUE, 100);
    private IconButton _reverseIconButton = new IconButton("icons/Arrow-Refresh_22.png", "Invert Proportions");
    private Button _applyButton = new Button("Apply");
    private Button _resetButton = new Button("Reset");
    private BorderPane _dimensionBasePane = new BorderPane();
    private boolean _isWidthChangeFromSpinner = true;
    private boolean _isHeightChangeFromSpinner = true;

    public CropPane(MainView parent) {
        super("Crop", parent);
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeLayout() {
        this.CurrentCroppingRegion = new CroppingRegion(super.parent);
        
        super.Add(this.ImageSizeComboBox);
        super.Add(super.GetSeperator());
        super.Add(this.LockAspectRatioCheckBox);
        super.Add(super.GetSeperator());
        
        super.Add(this._dimensionBasePane);
        
        GridPane dimensionGrids = new GridPane();
        this._dimensionBasePane.setLeft(dimensionGrids);
        dimensionGrids.setHgap(10);
        dimensionGrids.setVgap(5);
        dimensionGrids.setPadding(new Insets(0, 10, 0, 0));
        
        dimensionGrids.add(new Label("Width:"), 0, 0);
        dimensionGrids.add(this._widthSpinner, 1, 0);
        dimensionGrids.add(new Label("Height:"), 0, 1);
        dimensionGrids.add(this._heightSpinner, 1, 1);
        
        this._dimensionBasePane.setCenter(this._reverseIconButton);
        
        super.Add(super.GetSeperator());
        
        HBox buttonsPane = new HBox(5);
        buttonsPane.setAlignment(Pos.CENTER);
        
        buttonsPane.getChildren().addAll(
                this._applyButton,
                this._resetButton
        );
        
        super.Add(buttonsPane);
        
        this._widthSpinner.setEditable(true);
        this._heightSpinner.setEditable(true);
        this._resizeImageSizeComboBox(246);
    }
    
    private void _initializeEvents() {
        this._dimensionBasePane.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _resizeImageSizeComboBox(newValue.doubleValue());
            }
        });
        
        // Apply
        this._applyButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyCropping();
            }
        });
        
        // Reset
        this._resetButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ApplyReset();
            }
        });
        
        // Cropping Dimension
        this.ImageSizeComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String selectedItem) {
                ApplyCroppingDimension(selectedItem);
            }
        });
        
        // Cropping Width
        this.CurrentCroppingRegion.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _widthSpinner.getValueFactory().valueProperty().setValue(newValue.intValue());
            }
        });
        
        this.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _widthSpinner.getValueFactory().setValue(newValue.intValue());
            }
        });
        
        // Cropping Height        
        this.CurrentCroppingRegion.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _heightSpinner.getValueFactory().valueProperty().setValue(newValue.intValue());
            }
        });
        
        this.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                _heightSpinner.getValueFactory().setValue(newValue.intValue());
            }
        });
        
        // Revert
        this._reverseIconButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                _applyRevert();
            }
        });
    }
    
    public void ApplyCroppingDimension(String selectedItem) {
        if (!selectedItem.equals("Custom") && !selectedItem.equals(DEFAULT_SIZE)) {
            String ratioText = selectedItem.split(" ")[0];

            double width = Double.parseDouble(ratioText.split("\\:")[0].trim());
            double height = Double.parseDouble(ratioText.split("\\:")[1].trim());
            
            if (super.parent.CurrentImageView.getFitWidth() <= super.parent.CurrentImageView.getFitHeight()) {
                double croppingRegionWidth = super.parent.CurrentImageView.getFitWidth() * super.parent.CurrentImageView.getScaleX();
                double croppingRegionHeight = (height / width) * croppingRegionWidth;

                this.CurrentCroppingRegion.SetSize(croppingRegionWidth, croppingRegionHeight);
            }
            else {
                double croppingRegionHeight = super.parent.CurrentImageView.getFitHeight() * super.parent.CurrentImageView.getScaleY();
                double croppingRegionWidth = (width / height) * croppingRegionHeight;

                this.CurrentCroppingRegion.SetSize(croppingRegionWidth, croppingRegionHeight);
            }
            
            this.CurrentCroppingRegion.IsAlreadyScalled = true;
        }
    }
    
    private void _resizeImageSizeComboBox(double width) {
        this.ImageSizeComboBox.setMinWidth(width);
        this.ImageSizeComboBox.setPrefWidth(width);
    }
    
    private void _applyCropping() {
        if (super.parent.IsFileOpened()) {
            double scale = super.parent.CurrentImageView.getScaleX();
            
            Point2D[] croppingRegion = this.CurrentCroppingRegion.GetCropRegion();
            ImageModel result = SimpleImageProcessor.GET_CROPPED_IMAGE(super.parent.GetCurrentImage(), croppingRegion);
            super.parent.OpenImageFile(result);
            super.parent.SetScale(scale);
            this.CurrentCroppingRegion.SetPosition(0, 0);
        }
    }
    
    public void InitializeCroppingFeature() {
        this.AddCroppoingRegion();
    }
    
    public void TerminateFeatures() {
        this.RemoveCroppingRegion();;
    }
    
    public void AddCroppoingRegion() {
        if (!super.parent.ImageViewStackPane.getChildren().contains(this.CurrentCroppingRegion)) {
            super.parent.ImageViewStackPane.getChildren().add(this.CurrentCroppingRegion);
            
            if (super.parent.IsFileOpened()) {
                this.CurrentCroppingRegion.UpdateToDefaultSize();
            }
        }
    }
    
    public void RemoveCroppingRegion() {
        if (super.parent.ImageViewStackPane.getChildren().contains(this.CurrentCroppingRegion)) {
            super.parent.ImageViewStackPane.getChildren().remove(this.CurrentCroppingRegion);
        }
    }
    
    public void ApplyReset() {
        this.ImageSizeComboBox.getSelectionModel().select(DEFAULT_SIZE);
        this.LockAspectRatioCheckBox.setSelected(false);
        this.CurrentCroppingRegion.ClearSettings();
        this.CurrentCroppingRegion.UpdateToDefaultSize();
    }
    
    private void _applyRevert() {
        int width = _widthSpinner.getValue().intValue();
        int height = _heightSpinner.getValue().intValue();

        _widthSpinner.getValueFactory().valueProperty().setValue(height);
        _heightSpinner.getValueFactory().valueProperty().setValue(width);

        CurrentCroppingRegion.SetSize(height, width);
    }
}
