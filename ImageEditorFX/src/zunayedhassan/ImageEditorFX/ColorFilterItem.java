package zunayedhassan.ImageEditorFX;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Zunayed Hassan
 */
public class ColorFilterItem extends BorderPane {
    public int        Min         = 0;
    public int        Max         = 0;
    public int        Value       = 0;
    
    private String    _title      = "";
    private String    _icon       = "";
    private ImageView _imageView  = null;
    private Slider    _slider     = null;
    
    public ColorFilterItem(String title, String icon, int min, int max, int value) {
        this._initializeVariables(title, icon, min, max, value);
        this._initializeLayout();
        this._initializeEvents();
    }
    
    private void _initializeVariables(String title, String icon, int min, int max, int value) {
        this._title = title;
        this._icon  = icon;
        this.Min    = min;
        this.Max    = max;
        this.Value  = value;
    }
    
    private void _initializeLayout() {
        this.setPadding(new Insets(5));
        this._imageView = new ImageView(new Image(this.getClass().getResourceAsStream(this._icon)));
        this.setCenter(this._imageView);
        
        VBox vbox = new VBox(5);
        vbox.setAlignment(Pos.CENTER);
        this.setBottom(vbox);
        
        this._slider = new Slider(this.Min, this.Max, this.Value);
        this._slider.setPrefWidth(96);
        this._slider.setMaxWidth(this._slider.getPrefWidth());
        
        vbox.getChildren().addAll(
                new Label(this._title),
                this._slider
        );
    }
    
    private void _initializeEvents() {
        
    }
    
    public void Reset() {
        this._slider.setValue(this.Value);
    }
    
    public Slider GetSlider() {
        return this._slider;
    }
}
