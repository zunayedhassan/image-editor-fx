package zunayedhassan;

/**
 *
 * @author Zunayed Hassan
 */
public class Settings {
    public final static String      WINDOW_TITLE         = "ImageEditorFX";
    public final static int         WINDOW_WIDTH         = 1024;
    public final static int         WINDOW_HEIGHT        = 700;
    public static final String      APPLICATION_ICON     = null;

    public final static String[]    STYLE_CLASSES        = new String[] {
        "stylesheets/style.css"
    };

    public static final String[][]  FONTS                = new String[][] { };

    public static final boolean     IS_SCENE_TRANSPARENT = false;
}
