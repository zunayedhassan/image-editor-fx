package zunayedhassan;

import zunayedhassan.ImageEditorFX.MainView;

/**
 *
 * @author Zunayed Hassan
 */
public class RootUI extends BaseUI {
    public RootUI() {
        super.setCenter(new MainView());
    }
}
