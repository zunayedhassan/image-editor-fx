package zunayedhassan;

import java.util.Random;

/**
 *
 * @author Zunayed Hassan
 */
public class CommonUtilities {
    private static Random _RANDOM = null;
    private static long   _SEED   = 0;
    
    static {
        _SEED   = System.currentTimeMillis();
        _RANDOM = new Random(_SEED);
    }
    
    public static int GET_RANDOM_NUMBER_IN_BETWEEN(int a, int b) {
        int n = b - a + 1;
        
        if (n <= 0) {
            throw new IllegalArgumentException("argument must be positive");
        }
        
        return (a + _RANDOM.nextInt(n));
    }
}
