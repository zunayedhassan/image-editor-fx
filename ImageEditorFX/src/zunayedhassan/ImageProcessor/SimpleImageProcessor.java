package zunayedhassan.ImageProcessor;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import javafx.beans.value.WritableBooleanValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import zunayedhassan.CommonUtilities;
import zunayedhassan.ImageEditorFX.ImageModel;

/**
 *
 * @author Zunayed Hassan
 */
public class SimpleImageProcessor {
    public static final double[] LUMINANCE_FOR_GRAYSCALE = { 0.2989, 0.5870, 0.1140 };
    
    public static final double[][] SHARPEN_KERNEL = {
        {  0, -1,  0 },
        { -1,  5, -1 },
        {  0, -1,  0 }
    };
    
    public static final double[][] EMBOSS_KERNEL = {
        { -1, -1,  0 },
        { -1,  0,  1 },
        {  0,  1,  1 }
    };
    
    public static enum COLOR_TYPE {
        RED,
        GREEN,
        BLUE
    }
    
    public static ImageModel GET_RESIZED_IMAGE(ImageModel sourceImage, final int targetWidth, final int targetHeight) {
        BufferedImage originalImage = SwingFXUtils.fromFXImage(sourceImage, null);

        BufferedImage resizedImage = new BufferedImage(targetWidth, targetHeight, originalImage.getType());
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,
        RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        RenderingHints.VALUE_ANTIALIAS_ON);

        graphics2D.drawImage(originalImage, 0, 0, targetWidth, targetHeight, null);
        graphics2D.dispose();

        WritableImage writableImage = new WritableImage(targetWidth, targetHeight);

        for (int x = 0; x < targetWidth; x++) {
            for (int y = 0; y < targetHeight; y++) {
                java.awt.Color color = new java.awt.Color(resizedImage.getRGB(x, y));
                Color colorFx = new Color(color.getRed() / 255.0, color.getGreen() / 255.0, color.getBlue() / 255.0, 1);
                writableImage.getPixelWriter().setColor(x, y, colorFx);
            }
        }

        ImageModel output = new ImageModel(writableImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_CROPPED_IMAGE(ImageModel originalImage, Point2D[] croppingRegion) {
        double startX = croppingRegion[0].getX();
        double startY = croppingRegion[0].getY();
        double width = croppingRegion[1].getX();
        double height = croppingRegion[1].getY();
        
        if (startX < 0) {
            width = width + startX;
            startX = 0;
        }
        
        if (startY < 0) {
            height = height + startY;
            startY = 0;
        }
        
        if (originalImage.GetWidth() < startX + width) {
            width = originalImage.GetWidth() - startX;
        }
        
        if (originalImage.GetHeight() < startY + height) {
            height = originalImage.GetHeight() - startY;
        }
        
        WritableImage outputImage = new WritableImage((int) width, (int) height);
        
        for (int y = (int) startY, j = 0; y < outputImage.getHeight() + (int) startY; y++, j++) {
            for (int x = (int) startX, i = 0; x < outputImage.getWidth() + (int) startX; x++, i++) {
                Color color = originalImage.getPixelReader().getColor(x, y);
                outputImage.getPixelWriter().setColor(i, j, color);
            }
        }
        
        ImageModel result = new ImageModel(outputImage, originalImage.GetFileLocation());
        
        return result;
    }
    
    public static ImageModel GET_ROTATED_IMAGE(ImageModel originalImage, double angleInDegree) {
        // NOTE: Algorithm is copied from http://introcs.cs.princeton.edu/java/31datatype/Rotation.java.html
        double angle = Math.toRadians(angleInDegree);
        int width  = (int) originalImage.getWidth();
        int height = (int) originalImage.getHeight();

        double sin = Math.sin(angle);
        double cos = Math.cos(angle);
        double x0 = 0.5 * (width  - 1);     // point to rotate about
        double y0 = 0.5 * (height - 1);     // center of image

        WritableImage outputImage = new WritableImage(width, height);

        // Rotattion
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                double a = x - x0;
                double b = y - y0;
                int xx = (int) (+a * cos - b * sin + x0);
                int yy = (int) (+a * sin + b * cos + y0);

                // plot pixel (x, y) the same color as (xx, yy) if it's in bounds
                if ((xx >= 0) && (xx < width) && (yy >= 0) && (yy < height)) {
                    outputImage.getPixelWriter().setColor(x, y, originalImage.getPixelReader().getColor(xx, yy));
                }
            }
        }

        ImageModel result = new ImageModel(outputImage, originalImage.GetFileLocation());
        
        return result;
    }
    
    private static ImageModel _GET_ROTATED_IMAGE_IN_90_DEGREES(ImageModel originalImage, double angleInDegree, int outputWidth, int outputHeight) {
        // JavaFX Image to BufferedImage
        BufferedImage currentImage = SwingFXUtils.fromFXImage(originalImage, null);
        
        // create the transform, note that the transformations happen
        // in reversed order (so check them backwards)
        AffineTransform affineTransform = new AffineTransform();
        
        // Translate it to the center of the component
        affineTransform.translate(outputWidth * ((angleInDegree >= 0) ? 1: 0), outputHeight * ((angleInDegree >= 0) ? 0 : 1));
        
        // Do the actual rotation
        affineTransform.rotate(angleInDegree * (Math.PI / 180.0));

        // Draw the image
        BufferedImage output = new BufferedImage(outputWidth, outputHeight, currentImage.getType());
        
        Graphics2D graphics2D = output.createGraphics();
        graphics2D.drawImage(currentImage, affineTransform, null);
        graphics2D.dispose();
        
        WritableImage writableImage = new WritableImage(outputWidth, outputHeight);
        SwingFXUtils.toFXImage(output, writableImage);
        
        ImageModel result = new ImageModel(writableImage, originalImage.GetFileLocation());
        
        return result;
    }
    
    public static ImageModel GET_ROTATED_IMAGE_COUNTERCLOCKWISE(ImageModel originalImage) {
        return _GET_ROTATED_IMAGE_IN_90_DEGREES(originalImage, -90.0, (int) originalImage.getHeight(), (int) originalImage.getWidth());
    }
    
    public static ImageModel GET_ROTATED_IMAGE_CLOCKWISE(ImageModel originalImage) {
        return _GET_ROTATED_IMAGE_IN_90_DEGREES(originalImage, 90.0, (int) originalImage.getHeight(), (int) originalImage.getWidth());
    }
    
    public static ImageModel GET_CLONED_IMAGE(ImageModel originalImage) {
        WritableImage writableImage = new WritableImage((int) originalImage.getWidth(), (int) originalImage.getHeight());
        SwingFXUtils.toFXImage(SwingFXUtils.fromFXImage(originalImage, null), writableImage);
        ImageModel image = new ImageModel(writableImage, originalImage.GetFileLocation());
        
        return image;
    }
    
    public static ImageModel GET_STRAIGHTEN_IMAGE(ImageModel rotatedImage) {
        boolean isBlankPointFound = false;
        Color blankPoint = new Color(0, 0, 0, 0);
        
        int total = (int) Math.min(rotatedImage.getWidth(), rotatedImage.getHeight());
        
        Integer distance1 = null;
        Integer distance2 = null;
        Integer distance3 = null;
        Integer distance4 = null;
        
        for (int i = 0; i < total; i++) {
            Color testColor = rotatedImage.getPixelReader().getColor(i, i);
            
            if (testColor.equals(blankPoint)) {
                isBlankPointFound = true;
            }
            else {
                isBlankPointFound = false;
            }
            
            if (!isBlankPointFound) {
                distance1 = new Integer(i);
                break;
            }
        }
        
        for (int i = 0, j = (int) rotatedImage.getWidth() - 1; i < total; i++, j--) {
            Color testColor = rotatedImage.getPixelReader().getColor(j, i);
            
            if (testColor.equals(blankPoint)) {
                isBlankPointFound = true;
            }
            else {
                isBlankPointFound = false;
            }
            
            if (!isBlankPointFound) {
                distance2 = new Integer(i);
                break;
            }
        }
        
        for (int i = 0, j = (int) rotatedImage.getHeight() - 1; i < total; i++, j--) {
            Color testColor = rotatedImage.getPixelReader().getColor(i, j);
            
            if (testColor.equals(blankPoint)) {
                isBlankPointFound = true;
            }
            else {
                isBlankPointFound = false;
            }
            
            if (!isBlankPointFound) {
                distance3 = new Integer(i);
                break;
            }
        }
        
        for (int i = (int) rotatedImage.getWidth() - 1,
                 j = (int) rotatedImage.getHeight() - 1,
                 k = 0;
                
                 (i >= 0) && (j >= 0);
                 
                 i--,
                 j--,
                 k++) {
            
            Color testColor = rotatedImage.getPixelReader().getColor(i, j);
            
            if (testColor.equals(blankPoint)) {
                isBlankPointFound = true;
            }
            else {
                isBlankPointFound = false;
            }
            
            if (!isBlankPointFound) {
                distance4 = new Integer(k);
                break;
            }
        }
        
        // Find highest number/distance
        int max = Math.max(distance1, Math.max(distance2, Math.max(distance3, distance4)));
        
        int height = (int) ((rotatedImage.getHeight() - 1 - max) - max);
        int width = (int) ((rotatedImage.getWidth() - 1 - max) - max);
        
        WritableImage output = new WritableImage(width, height);
        
        for (int x = max, i = 0; x < width + max; x++, i++) {
            for (int y = max, j = 0; y < height + max; y++, j++) {
                output.getPixelWriter().setColor(i, j, rotatedImage.getPixelReader().getColor(x, y));
            }
        }
        
        ImageModel result = new ImageModel(output, rotatedImage.GetFileLocation());
        
        return result;
    }
    
    public static ImageModel GET_VERTICALLY_FLIPPED_IMAGE(ImageModel originalImage) {
        BufferedImage currentImage = SwingFXUtils.fromFXImage(originalImage, null);
        
        int width = currentImage.getWidth();  
        int height = currentImage.getHeight(); 
        
        BufferedImage outputImage = new BufferedImage(width, height, currentImage.getColorModel().getTransparency()); 
        Graphics2D graphics2D = outputImage.createGraphics();
        graphics2D.drawImage(currentImage, 0, 0, width, height, 0, height, width, 0, null);  
        graphics2D.dispose();
        
        WritableImage writableImage = new WritableImage(width, height);
        SwingFXUtils.toFXImage(outputImage, writableImage);
        
        ImageModel result = new ImageModel(writableImage, originalImage.GetFileLocation());
        
        return result;
    }
    
    public static ImageModel GET_HORIZONTALLY_FLIPPED_IMAGE(ImageModel originalImage) {
        BufferedImage currentImage = SwingFXUtils.fromFXImage(originalImage, null);
        
        int width = currentImage.getWidth();  
        int height = currentImage.getHeight(); 
        
        BufferedImage outputImage = new BufferedImage(width, height, currentImage.getColorModel().getTransparency()); 
        Graphics2D graphics2D = outputImage.createGraphics();
        graphics2D.drawImage(currentImage, 0, 0, width, height, width, 0, 0, height, null);  
        graphics2D.dispose();
        
        WritableImage writableImage = new WritableImage(width, height);
        SwingFXUtils.toFXImage(outputImage, writableImage);
        
        ImageModel result = new ImageModel(writableImage, originalImage.GetFileLocation());
        
        return result;
    }
    
    public static double GET_TRUNCATED_RGB_VALUE(double value) {
        return _GET_TRUNCATED_VALUE(value, 0.0, 255.0);
    }
    
    private static double _GET_TRUNCATED_VALUE(double value, double min, double max) {
        return ((value > max) ? max : ((value < min) ? min : value));
    }
    
    public static double[] GET_HSL_FROM_RGB(double red, double green, double blue) {
        //  Get RGB values in the range 0 - 1
        red   /= 255.0;
        green /= 255.0;
        blue  /= 255.0;
        
        //  Minimum and Maximum RGB values are used in the HSL calculations
        double min = Math.min(red, Math.min(green, blue));
        double max = Math.max(red, Math.max(green, blue));
        
        //  Calculate the Hue
        double hue = 0.0;
        
        if (max == min) {
            hue = 0.0;
        }
        else if (max == red) {
            hue = (((green - blue) / (max - min) / 6.0) + 1.0) % 1.0;
        }
        else if (max == green) {
            hue = ((blue - red) / (max - min) / 6.0) + 1.0 / 3.0;
        }
        else if (max == blue) {
            hue = ((red - green) / (max - min) / 6.0) + 2.0 / 3.0;
        }
        
        //  Calculate the Luminance
        double lightness = (max + min) / 2.0;
        
        //  Calculate the Saturation
        double saturation = 0;
        
        if (max == min) {
            saturation = 0.0;
        }
        else if (lightness <= 0.5) {
            saturation = (max - min) / (max + min);
        }
        else {
            saturation = (max - min) / (2 - max - min);
        }
        
        double[] hsl = {
            hue        * 360,
            saturation * 100,
            lightness  * 100
        };
        
        return hsl;
    }
    
    public static double GET_RGB_FROM_HUE(double p, double q, double t) {
        if (t < 0.0) {
            t += 1.0;
        }
        
        if (t > 1.0) {
            t -= 1.0;
        }
        
        if (t < 1.0 / 6.0) {
            return (p + (q - p) * 6.0 * t);
        }
        
        if (t < 1.0 / 2.0) {
            return q;
        }
        
        if (t < 2.0 / 3.0) {
            return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
        }
        
        return p;
    }
    
    public static double[] GET_RGB_FROM_HSL(double hue, double saturation, double lightness) {
        double[] rgb = new double[3];
        
        hue        /= 360.0;
        saturation /= 100.0;
        lightness  /= 100.0;
        
        double red   = 0;
        double green = 0;
        double blue  = 0;
        
        if (saturation == 0) {
            red = green = blue = lightness;     // Achromatic
        }
        else {
            double q = (lightness < 0.5) ? lightness * (1 + saturation) : lightness + saturation - lightness * saturation;
            double p = 2 * lightness - q;
            
            red   = GET_RGB_FROM_HUE(p, q, hue + 1.0 / 3.0);
            green = GET_RGB_FROM_HUE(p, q, hue);
            blue  = GET_RGB_FROM_HUE(p, q, hue - 1.0 / 3.0);
        }
        
        red   *= 255;
        green *= 255;
        blue  *= 255;
        
        rgb[0] = GET_TRUNCATED_RGB_VALUE(red);
        rgb[1] = GET_TRUNCATED_RGB_VALUE(green);
        rgb[2] = GET_TRUNCATED_RGB_VALUE(blue);
        
        return rgb;
    }
    
    public static double[] GET_CMYK_FROM_RGB(double red, double green, double blue) {
        red   = red   / 255.0;
        green = green / 255.0;
        blue  = blue  / 255.0;
        
        double black   = 1.0 - Math.max(red, Math.max(green, blue));
        double cyan    = (1.0 - red   - black) / (1.0 - black);
        double magenta = (1.0 - green - black) / (1.0 - black);
        double yellow  = (1.0 - blue  - black) / (1.0 - black);
        
        return (new double[] {
            cyan,
            magenta,
            yellow,
            black
        });
    }
    
    public static double[] GET_RGB_FROM_CMYK(double cyan, double magenta, double yellow, double black) {
        double red   = 255.0 * (1.0 - cyan   ) * (1.0 - black);
        double green = 255.0 * (1.0 - magenta) * (1.0 - black);
        double blue  = 255.0 * (1.0 - yellow ) * (1.0 - black);
        
        return (new double[] {
            red,
            green,
            blue
        });
    }
    
    public static ImageModel GET_ADJUSTED_IMAGE_AFTER_BRIGHTNESS_CHANGED(ImageModel sourceImage, int brightness) {
        int imageWidth = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(red   + brightness),
                        (int) GET_TRUNCATED_RGB_VALUE(green + brightness),
                        (int) GET_TRUNCATED_RGB_VALUE(blue  + brightness)
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing Memory
                sourceColor = null;
                red = null;
                green = null;
                blue = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_ADJUSTED_IMAGE_AFTER_CONTRAST_CHANGED(ImageModel sourceImage, int contrast) {
        int imageWidth = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        double factor = (259.0 * (contrast + 255.0)) / (255.0 * (259.0 - contrast));
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                Double newRed = (factor * (red - 128)) + 128;
                Double newGreen = (factor * (green - 128)) + 128;
                Double newBlue = (factor * (blue - 128)) + 128;
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(newRed.intValue()),
                        (int) GET_TRUNCATED_RGB_VALUE(newGreen.intValue()),
                        (int) GET_TRUNCATED_RGB_VALUE(newBlue.intValue())
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing Memory
                sourceColor = null;
                red = null;
                green = null;
                blue = null;
                newRed = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_ADJUSTED_IMAGE_AFTER_SATURATION_CHANGED(ImageModel sourceImage, int saturation) {
        int imageWidth = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                double[] hsl = SimpleImageProcessor.GET_HSL_FROM_RGB(red, green, blue);
                
                Double newHue = hsl[0];
                Double newSaturation = hsl[1] + saturation;
                Double newLightness =  hsl[2];
                
                double[] newRgb = GET_RGB_FROM_HSL(newHue, newSaturation, newLightness);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(newRgb[0]),
                        (int) GET_TRUNCATED_RGB_VALUE(newRgb[1]),
                        (int) GET_TRUNCATED_RGB_VALUE(newRgb[2])
                );

                outputImage.getPixelWriter().setColor(x, y, modifiedColor);

                // Clearing memory
                sourceColor = null;
                red = null;
                green = null;
                blue = null;
                hsl = null;
                newHue = null;
                newSaturation = null;
                newLightness = null;
                newRgb = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_ADJUSTED_IMAGE_AFTER_HUE_CHANGED(ImageModel sourceImage, int hue) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                double[] hsl = GET_HSL_FROM_RGB(red, green, blue);
                
                Double newHue = hsl[0] + hue;
                Double newSaturation = hsl[1];
                Double newLightness =  hsl[2];
                
                double[] newRgb = GET_RGB_FROM_HSL(newHue, newSaturation, newLightness);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(newRgb[0]),
                        (int) GET_TRUNCATED_RGB_VALUE(newRgb[1]),
                        (int) GET_TRUNCATED_RGB_VALUE(newRgb[2])
                );

                outputImage.getPixelWriter().setColor(x, y, modifiedColor);

                // Clearing memory
                sourceColor = null;
                red = null;
                green = null;
                blue = null;
                hsl = null;
                newHue = null;
                newSaturation = null;
                newLightness= null;
                newRgb = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_ADJUSTED_IMAGE_AFTER_TEMERATURE_CHANGED(ImageModel sourceImage, int temperature) {
        int imageWidth = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(red + temperature),
                        (int) GET_TRUNCATED_RGB_VALUE(green),
                        (int) GET_TRUNCATED_RGB_VALUE(blue - temperature)
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red = null;
                green = null;
                blue = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_ADJUSTED_IMAGE_AFTER_TINT_CHANGED(ImageModel sourceImage, int tint) {
        int imageWidth = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(red),
                        (int) GET_TRUNCATED_RGB_VALUE(green + tint),
                        (int) GET_TRUNCATED_RGB_VALUE(blue)
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red = null;
                green = null;
                blue = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_ADJUSTED_IMAGE_AFTER_GAMMA_CHANGED(ImageModel sourceImage, int gamma) {
        int imageWidth = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        double gammCorrection = 1.0 / (double) gamma;
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE((int) (255 * Math.pow(red   / 255.0, gammCorrection))),
                        (int) GET_TRUNCATED_RGB_VALUE((int) (255 * Math.pow(green / 255.0, gammCorrection))),
                        (int) GET_TRUNCATED_RGB_VALUE((int) (255 * Math.pow(blue  / 255.0, gammCorrection)))
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red = null;
                green = null;
                blue = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static Matrix GET_MATRIX_FROM_IMAGE(ImageModel image, COLOR_TYPE pixelColorType, boolean isFillWithLastRowAndColumn) {
        final int kernel_size = 3;
        
        int canvasSize = Math.max((int) image.getWidth(), (int) image.getHeight());
        canvasSize = (int) (Math.floor(canvasSize / (kernel_size * 8)) * (kernel_size * 8)) + (kernel_size * 8);

        Matrix output = new Matrix(canvasSize, canvasSize);
        
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {                
                switch (pixelColorType) {
                    case RED:
                        output.Data[i][j] = image.getPixelReader().getColor(j, i).getRed() * 255;
                        break;
                        
                    case GREEN:
                        output.Data[i][j] = image.getPixelReader().getColor(j, i).getGreen() * 255;
                        break;
                        
                    case BLUE:
                        output.Data[i][j] = image.getPixelReader().getColor(j, i).getBlue() * 255;
                        break;
                }
            }
        }
        
        
        // Optional
        if (isFillWithLastRowAndColumn) {
            // replacing rest of the rows with last row
            int[] lastRow = new int[canvasSize];

            int x = 0,
                y = 0;

            for (int i = 0; i < canvasSize; i++) {
                x = i; 
                y = (int) image.getHeight() - 1;

                if (x > image.getWidth() - 1) {
                    x = (int) image.getWidth() - 1;
                }

                switch (pixelColorType) {
                    case RED:
                        lastRow[i] = (int) (image.getPixelReader().getColor(x, y).getRed() * 255);
                        break;

                    case GREEN:
                        lastRow[i] = (int) (image.getPixelReader().getColor(x, y).getGreen() * 255);
                        break;

                    case BLUE:
                        lastRow[i] = (int) (image.getPixelReader().getColor(x, y).getBlue() * 255);
                        break;
                }
            }

            for (int i = (int) image.getHeight(); i < canvasSize; i++) {
                for (int j = 0; j < canvasSize; j++) {
                    output.Data[i][j] = lastRow[j];
                }
            }

            // replacing rest of the columns with last column
            int[] lastColumn = new int[canvasSize];

            x = 0;
            y = 0;

            for (int i = 0; i < canvasSize; i++) {
                x = (int) image.getWidth() - 1;; 
                y = i;

                if (y > image.getHeight() - 1) {
                    y = (int) image.getHeight() - 1;
                }

                switch (pixelColorType) {
                    case RED:
                        lastColumn[i] = (int) (image.getPixelReader().getColor(x, y).getRed() * 255);
                        break;

                    case GREEN:
                        lastColumn[i] = (int) (image.getPixelReader().getColor(x, y).getGreen() * 255);
                        break;

                    case BLUE:
                        lastColumn[i] = (int) (image.getPixelReader().getColor(x, y).getBlue() * 255);
                        break;
                }
            }

            for (int i = 0; i < canvasSize; i++) {
                for (int j = (int) image.getWidth(); j < canvasSize; j++) {
                    output.Data[i][j] = lastColumn[i];
                }
            }
        }
        
        return output;
    }
    
    public static double GET_GAUSSIAN_WEIGHT(int x, int y, final double standardDeviation) {
        return (1 / (2 * Math.PI * Math.pow(standardDeviation, 2))) * (Math.pow(Math.E, ((-(Math.pow(x, 2) + Math.pow(y, 2)) / (2 * Math.pow(standardDeviation, 2)) ))));
    }
    
    public static Matrix GET_GAUSSIAN_KERNEL(final double standard_deviation) {
        final int kernel_size = 3;  // Kernel Size must be a odd number

        Matrix gaussinanKernel = new Matrix(kernel_size, kernel_size, false);
        double total = 0.0;

        for (int row = 0, y = -1; row < kernel_size; row++, y++) {
            for (int column = 0, x = -1; column < kernel_size; column++, x++) {
                gaussinanKernel.Data[row][column] = GET_GAUSSIAN_WEIGHT(x, y, standard_deviation);
                total += gaussinanKernel.Data[row][column];
            }
        }
        
        for (int i = 0; i < kernel_size; i++) {
            for (int j = 0; j < kernel_size; j++) {
                gaussinanKernel.Data[i][j] = gaussinanKernel.Data[i][j] * (1.0 / total);
            }
        }
        
        return gaussinanKernel;
    }
    
    public static Matrix GET_GAUSSIAN_BLUR_FROM_GIVEN_MATRIX(Matrix source, Matrix gaussianKernel) {
        Matrix output = new Matrix(source.GetRow(), source.GetColumn());
        
        for (int i = 0; i < output.GetRow(); i++) {
            for (int j = 0; j < output.GetColumn(); j++) {
                output.Data[i][j] = source.Data[i][j];
            }
        }
        
        final int kernel_size = 3;
        
        for (int i = 1; i < output.GetRow() - Math.floor(kernel_size / 2); i++) {
            for (int j = 1; j < output.GetColumn() - Math.floor(kernel_size / 2); j++) {
                
                double total = 0.0;
                
                for (int a = i - 1, p = 0; a < i - 1 + kernel_size; a++, p++) {
                    for (int b = j - 1, q = 0; b < j - 1 + kernel_size; b++, q++) {
                        total += source.Data[a][b] * gaussianKernel.Data[p][q];
                    }
                }
                
                output.Data[i][j] = total;
            }
        }
        
        source = null;
        System.gc();
        
        return output;
    }
    
    public static ImageModel GET_GAUSSIAN_BLURED_IMAGE(ImageModel image) {
        Matrix red   = GET_MATRIX_FROM_IMAGE(image, COLOR_TYPE.RED,   true);
        Matrix green = GET_MATRIX_FROM_IMAGE(image, COLOR_TYPE.GREEN, true);
        Matrix blue  = GET_MATRIX_FROM_IMAGE(image, COLOR_TYPE.BLUE,  true);

        Matrix gaussianKernel = GET_GAUSSIAN_KERNEL(1.0);

        Matrix gaussianRed = GET_GAUSSIAN_BLUR_FROM_GIVEN_MATRIX(red, gaussianKernel);
        Matrix gaussianGreen = GET_GAUSSIAN_BLUR_FROM_GIVEN_MATRIX(green, gaussianKernel);
        Matrix gaussianBlue = GET_GAUSSIAN_BLUR_FROM_GIVEN_MATRIX(blue, gaussianKernel);
        
        WritableImage outputImage = new WritableImage((int) image.getWidth(), (int) image.getHeight());
        
        for (int i = 0; i < image.getHeight(); i++) {
            for (int j = 0; j < image.getWidth(); j++) {
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE((int) gaussianRed.Data[i][j]),
                        (int) GET_TRUNCATED_RGB_VALUE((int) gaussianGreen.Data[i][j]),
                        (int) GET_TRUNCATED_RGB_VALUE((int) gaussianBlue.Data[i][j])
                );
                
                outputImage.getPixelWriter().setColor(j, i, modifiedColor);
            }
        }
        
        ImageModel output = new ImageModel(outputImage, image.GetFileLocation());
        
        return output;
    }
    
    public static Matrix GET_MATRIX_AFTER_CONVOLUTION(Matrix imageData, double[][] kernel) {
        Matrix output = new Matrix(imageData.GetRow(), imageData.GetColumn());
        
        for (int column = 0; column < imageData.GetColumn(); column++) {
            output.Data[0][column] = imageData.Data[0][column];
            output.Data[imageData.GetRow() - 1][column] = imageData.Data[imageData.GetRow() - 1][column];
        }
        
        for (int row = 0; row < imageData.GetRow(); row++) {
            output.Data[row][0] = imageData.Data[row][0];
            output.Data[row][imageData.GetColumn() - 1] = imageData.Data[row][imageData.GetColumn() - 1];
        }
        
        for (int row = 1; row < imageData.GetRow() - 1; row++) {
            for (int column = 1; column < imageData.GetColumn() - 1; column++) {
                output.Data[row][column] = (int) ((imageData.Data[row - 1][column - 1] * kernel[0][0]) + (imageData.Data[row - 1][column] * kernel[0][1]) + (imageData.Data[row - 1][column + 1] * kernel[0][2]) + 
                                                  (imageData.Data[row    ][column - 1] * kernel[1][0]) + (imageData.Data[row    ][column] * kernel[1][1]) + (imageData.Data[row   ][column  + 1] * kernel[1][2]) + 
                                                  (imageData.Data[row + 1][column - 1] * kernel[2][0]) + (imageData.Data[row + 1][column] * kernel[2][1]) + (imageData.Data[row + 1][column + 1] * kernel[2][2]));
            }
        }
        
        return output;
    }
    
    public static ImageModel GET_GAUSSIAN_SHARPENED_IMAGE(ImageModel sourceImage) {
        Matrix red   = GET_MATRIX_FROM_IMAGE(sourceImage, COLOR_TYPE.RED,   true);
        Matrix green = GET_MATRIX_FROM_IMAGE(sourceImage, COLOR_TYPE.GREEN, true);
        Matrix blue  = GET_MATRIX_FROM_IMAGE(sourceImage, COLOR_TYPE.BLUE,  true);
        
        Matrix outputRed   = GET_MATRIX_AFTER_CONVOLUTION(red,   SHARPEN_KERNEL);
        Matrix outputGreen = GET_MATRIX_AFTER_CONVOLUTION(green, SHARPEN_KERNEL);
        Matrix outputBlue  = GET_MATRIX_AFTER_CONVOLUTION(blue,  SHARPEN_KERNEL);
        
        WritableImage outputImage = new WritableImage((int) sourceImage.getWidth(), (int) sourceImage.getHeight());
        
        for (int i = 0; i < sourceImage.getHeight(); i++) {
            for (int j = 0; j < sourceImage.getWidth(); j++) {
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE((int) outputRed.Data[i][j]),
                        (int) GET_TRUNCATED_RGB_VALUE((int) outputGreen.Data[i][j]),
                        (int) GET_TRUNCATED_RGB_VALUE((int) outputBlue.Data[i][j])
                );
                
                outputImage.getPixelWriter().setColor(j, i, modifiedColor);
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_GRAYSCALLED_IMAGE(ImageModel sourceImage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Double red   = sourceColor.getRed()   * 255 * LUMINANCE_FOR_GRAYSCALE[0];
                Double green = sourceColor.getGreen() * 255 * LUMINANCE_FOR_GRAYSCALE[1];
                Double blue  = sourceColor.getBlue()  * 255 * LUMINANCE_FOR_GRAYSCALE[2];
                Double luma  = red + green + blue;
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(luma),
                        (int) GET_TRUNCATED_RGB_VALUE(luma),
                        (int) GET_TRUNCATED_RGB_VALUE(luma)
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red         = null;
                green       = null;
                blue        = null;
                luma        = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_BLACK_AND_WHITE_IMAGE(ImageModel sourceImage) {
        ImageModel grayscalledImage = GET_GRAYSCALLED_IMAGE(sourceImage);
        
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = grayscalledImage.getPixelReader().getColor(x, y);
                
                Double red   = sourceColor.getRed()   * 255;
                Double green = sourceColor.getGreen() * 255;
                Double blue  = sourceColor.getBlue()  * 255;
                
                if (red > 127.0) {
                    red = 255.0;
                }
                else {
                    red = 0.0;
                }
                
                if (green > 127.0) {
                    green = 255.0;
                }
                else {
                    green = 0.0;
                }
                
                if (blue > 127.0) {
                    blue = 255.0;
                }
                else {
                    blue = 0.0;
                }
                
                Color modifiedColor = Color.rgb(
                        red.intValue(),
                        green.intValue(),
                        blue.intValue()
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_SOBEL_EDGE_IMAGE(ImageModel sourceImage) {
        // Convert to grayscale
        ImageModel grayscaleImage = GET_GRAYSCALLED_IMAGE(sourceImage);
        
        final int KERNEL_SIZE = 3;
        int canvasSize = Math.max((int) grayscaleImage.getWidth(), (int) grayscaleImage.getHeight());
        canvasSize = (int) (Math.floor(canvasSize / (KERNEL_SIZE * 8)) * (KERNEL_SIZE * 8)) + (KERNEL_SIZE * 8);
        
        // Convert to a matrix
        Matrix grayscaleData = new Matrix(canvasSize, canvasSize);
        
        for (int i = 0; i < canvasSize; i++) {
            for (int j = 0; j < canvasSize; j++) {
                if ((j < grayscaleImage.getWidth()) && (i < grayscaleImage.getHeight())) {
                    grayscaleData.Data[i][j] = grayscaleImage.getPixelReader().getColor(j, i).getRed() * 255;
                }
            }
        }
        
        Matrix grayscaleDataAfterConversion = new Matrix(grayscaleData.GetRow(), grayscaleData.GetColumn());
        
        // Matrix Convolution
        for (int i = 0; i < canvasSize - 2; i++) {
            for (int j = 0; j < canvasSize - 2; j++) {
                double gx = (grayscaleData.Data[i + 2][j + 0] + (2 * grayscaleData.Data[i + 2][j + 1]) + grayscaleData.Data[i + 2][j + 2]) - 
                            (grayscaleData.Data[i + 0][j + 0] + (2 * grayscaleData.Data[i + 0][j + 1]) + grayscaleData.Data[i + 0][j + 2]);

                double gy = (grayscaleData.Data[i + 0][j + 2] + (2 * grayscaleData.Data[i + 1][j + 2]) + grayscaleData.Data[i + 2][j + 2]) - 
                            (grayscaleData.Data[i + 0][j + 0] + (2 * grayscaleData.Data[i + 1][j + 0]) + grayscaleData.Data[i + 2][j + 0]);

                double gradient = Math.sqrt(Math.pow(gx, 2) + Math.pow(gy, 2));
                // double gradient = Math.abs(gx) + Math.abs(gy);

                grayscaleDataAfterConversion.Data[i][j] = gradient;
            }
        }
        
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int i = 0; i < grayscaleImage.getHeight(); i++) {
            for (int j = 0; j < grayscaleImage.getWidth(); j++) {
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(grayscaleDataAfterConversion.Data[i][j]),
                        (int) GET_TRUNCATED_RGB_VALUE(grayscaleDataAfterConversion.Data[i][j]),
                        (int) GET_TRUNCATED_RGB_VALUE(grayscaleDataAfterConversion.Data[i][j])
                );
                
                outputImage.getPixelWriter().setColor(j, i, modifiedColor);
            }
        }
        
        // Clearing memory
        grayscaleImage               = null;
        grayscaleData                = null;
        grayscaleDataAfterConversion = null;
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_INVERTED_IMAGE(ImageModel sourceImage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) sourceColor.getGreen() * 255;
                Integer blue  = (int) sourceColor.getBlue()  * 255;
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(255 - red),
                        (int) GET_TRUNCATED_RGB_VALUE(255 - green),
                        (int) GET_TRUNCATED_RGB_VALUE(255 - blue)
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red         = null;
                green       = null;
                blue        = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_EMBOSS_IMAGE(ImageModel sourceImage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        ImageModel grayscaleImage = GET_GRAYSCALLED_IMAGE(sourceImage);        
        Matrix grayscaleData = GET_MATRIX_FROM_IMAGE(grayscaleImage, COLOR_TYPE.RED, true);
        Matrix embossData = GET_MATRIX_AFTER_CONVOLUTION(grayscaleData, EMBOSS_KERNEL);
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Integer colorData = (int) GET_TRUNCATED_RGB_VALUE(Math.abs(128 + embossData.Data[y][x]));
                
                Color modifiedColor = Color.rgb(
                        colorData,
                        colorData,
                        colorData
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                colorData = null;
            }
        }
        
        // Clearing memory
        grayscaleImage = null;
        grayscaleData = null;
        embossData = null;
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_PEN_OUTLINE_IMAGE(ImageModel sourceImage) {
        ImageModel sobelEdgeImage = SimpleImageProcessor.GET_SOBEL_EDGE_IMAGE(sourceImage);
        ImageModel blackAndWhiteImage = SimpleImageProcessor.GET_BLACK_AND_WHITE_IMAGE(sobelEdgeImage);
        ImageModel result = SimpleImageProcessor.GET_INVERTED_IMAGE(blackAndWhiteImage);
        
        return result;
    }
    
    public static ImageModel GET_SEPIA_IMAGE(ImageModel sourceImage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Double red   = sourceColor.getRed()   * 255;
                Double green = sourceColor.getGreen() * 255;
                Double blue  = sourceColor.getBlue()  * 255;
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE((red * .393) + (green *.769) + (blue * .189)),
                        (int) GET_TRUNCATED_RGB_VALUE((red * .349) + (green *.686) + (blue * .168)),
                        (int) GET_TRUNCATED_RGB_VALUE((red * .272) + (green *.534) + (blue * .131))
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red         = null;
                green       = null;
                blue        = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_GLASS_IMAGE(ImageModel sourceImage) {
        final int RADIUS = 5;
        
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        ImageModel outputImage = GET_CLONED_IMAGE(sourceImage);
        
        for (int y = 0; y < imageHeight; y++) {
            for (int x = 0; x < imageWidth; x++) {
                int xx = (imageWidth  + x + CommonUtilities.GET_RANDOM_NUMBER_IN_BETWEEN(-RADIUS, RADIUS)) % imageWidth;
                int yy = (imageHeight + y + CommonUtilities.GET_RANDOM_NUMBER_IN_BETWEEN(-RADIUS, RADIUS)) % imageHeight;
                
                Color sourceColor = sourceImage.getPixelReader().getColor(xx, yy);
                
                Integer red   = (int) GET_TRUNCATED_RGB_VALUE(sourceColor.getRed()   * 255);
                Integer green = (int) GET_TRUNCATED_RGB_VALUE(sourceColor.getGreen() * 255);
                Integer blue  = (int) GET_TRUNCATED_RGB_VALUE(sourceColor.getBlue()  * 255);
                
                outputImage.getPixelWriter().setColor(x, y, Color.rgb(red, green, blue));
                
                // Clearing memory
                sourceColor = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_OIL_PAINTING_IMAGE(ImageModel sourceImage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        final int OIL_PAINTING_DIAMETER = 7;
        final int START_INDEX = (int) Math.floor(OIL_PAINTING_DIAMETER / 2);

        ImageModel outputImage = GET_CLONED_IMAGE(sourceImage);
        
        for (int x = START_INDEX; x < sourceImage.getWidth() - START_INDEX; x++) {
            for (int y = START_INDEX; y < sourceImage.getHeight() - START_INDEX; y++) {
                // Get neighbour area's color
                Color[] neighbourColors = new Color[(int) Math.pow(OIL_PAINTING_DIAMETER, 2)];
                
                for (int i = x - START_INDEX, index = 0; i <= x + START_INDEX; i++) {
                    for (int j = y - START_INDEX; j <= y + START_INDEX; j++) {
                        neighbourColors[index++] = sourceImage.getPixelReader().getColor(i, j);
                    }
                }
                
                // Calculate intesities
                int[] intensities = new int[neighbourColors.length];
                
                for (int i = 0; i < neighbourColors.length; i++) {
                    Color color = neighbourColors[i];
                    intensities[i] = (int) GET_TRUNCATED_RGB_VALUE(((color.getRed() + color.getGreen() + color.getBlue()) / 3.0) * 255.0);
                }
                
                // Findout which color has most intensities
                int max = 0;
                
                for (int i = 0; i < intensities.length; i++) {
                    if (max < intensities[i]) {
                        max = intensities[i];
                    }
                }
                
                int maxIndex = 0;
                
                for (int i = 0; i < intensities.length; i++) {
                    if (max == intensities[i]) {
                        maxIndex = i;
                        break;
                    }
                }
                
                Color maxIntensiveColor = neighbourColors[maxIndex];
                
                // Replace the current pixel with maxium intensive color
                outputImage.getPixelWriter().setColor(x, y, maxIntensiveColor);
            }
        }
        
        return outputImage;
    }
    
    public static ImageModel GET_PIXELATE_IMAGE(ImageModel sourceImage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        final int SIZE = 7;
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int x = 0; x < imageWidth; x += SIZE) {
            for (int y = 0; y < imageHeight; y += SIZE) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                for (int i = x; i < x + SIZE; i++) {
                    for (int j = y; j < y + SIZE; j++) {
                        if ((i < imageWidth) && (j < imageHeight)) {
                            outputImage.getPixelWriter().setColor(i, j, sourceColor);
                        }
                    }
                }
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    private static ImageModel _GET_COLOR_FILTER_FROM_RGB_VALUE(ImageModel sourceImage, int value, double redPercentage, double greenPercentage, double bluePercentage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int x = 0; x < imageWidth; x++) {
            for (int y = 0; y < imageHeight; y++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(red   + (value * redPercentage  )),
                        (int) GET_TRUNCATED_RGB_VALUE(green + (value * greenPercentage)),
                        (int) GET_TRUNCATED_RGB_VALUE(blue  + (value * bluePercentage ))
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red         = null;
                green       = null;
                blue        = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_BLACK(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_RGB_VALUE(sourceImage, value, -1, -1, -1);
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_WHITE(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_RGB_VALUE(sourceImage, value, 1, 1, 1);
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_RED(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_RGB_VALUE(sourceImage, value, 1, 0, 0);
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_LIME(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_RGB_VALUE(sourceImage, value, 0, 1, 0);
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_BLUE(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_RGB_VALUE(sourceImage, value, 0, 0, 1);
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_TEAL(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_RGB_VALUE(sourceImage, value, 0, 1, 1);
    }
    
    private static ImageModel _GET_COLOR_FILTER_FROM_CMYK_VALUE(ImageModel sourceImage, int value, double cyanPercentage, double magentaPercentage, double yellowPercentage, double blackPercentage) {
        int imageWidth  = (int) sourceImage.getWidth();
        int imageHeight = (int) sourceImage.getHeight();
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int x = 0; x < imageWidth; x++) {
            for (int y = 0; y < imageHeight; y++) {
                Color sourceColor = sourceImage.getPixelReader().getColor(x, y);
                
                Integer red   = (int) (sourceColor.getRed()   * 255);
                Integer green = (int) (sourceColor.getGreen() * 255);
                Integer blue  = (int) (sourceColor.getBlue()  * 255);
                
                double[] cmyk = GET_CMYK_FROM_RGB(red, green, blue);
                cmyk[0] += ((value / 100.0) * cyanPercentage);
                cmyk[1] += ((value / 100.0) * magentaPercentage);
                cmyk[2] += ((value / 100.0) * yellowPercentage);
                cmyk[3] += ((value / 100.0) * blackPercentage);
                
                double[] rgb = GET_RGB_FROM_CMYK(cmyk[0], cmyk[1], cmyk[2], cmyk[3]);
                
                Color modifiedColor = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(rgb[0]),
                        (int) GET_TRUNCATED_RGB_VALUE(rgb[1]),
                        (int) GET_TRUNCATED_RGB_VALUE(rgb[2])
                );
                
                outputImage.getPixelWriter().setColor(x, y, modifiedColor);
                
                // Clearing memory
                sourceColor = null;
                red         = null;
                green       = null;
                blue        = null;
                cmyk        = null;
                rgb         = null;
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_YELLOW(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_CMYK_VALUE(sourceImage, value, 0, 0, 1, 0);
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_CYAN(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_CMYK_VALUE(sourceImage, value, 1, 0, 0, 0);
    }
    
    public static ImageModel GET_COLOR_FILTERED_IMAGE_MAGENTA(ImageModel sourceImage, int value) {
        return _GET_COLOR_FILTER_FROM_CMYK_VALUE(sourceImage, value, 0, 1, 0, 0);
    }
    
    public static ImageModel GET_IMAGE_PORTION_FROM_RECTANGLE_SELECTION(ImageModel sourceImage, double[] bounds) {
        int startX      = (int) bounds[0];
        int startY      = (int) bounds[1];
        int imageWidth  = (int) bounds[2];
        int imageHeight = (int) bounds[3];
        
        WritableImage outputImage = new WritableImage(imageWidth, imageHeight);
        
        for (int x = startX,
                 i = 0;
                
                 x < imageWidth + startX;
                 
                 x++,
                 i++) {
            
            for (int y = startY,
                     j = 0;
                    
                     y < imageHeight + startY;
                     
                     y++,
                     j++) {
                
                Color color = Color.rgb(
                        (int) GET_TRUNCATED_RGB_VALUE(sourceImage.getPixelReader().getColor(x, y).getRed()   * 255.0),
                        (int) GET_TRUNCATED_RGB_VALUE(sourceImage.getPixelReader().getColor(x, y).getGreen() * 255.0),
                        (int) GET_TRUNCATED_RGB_VALUE(sourceImage.getPixelReader().getColor(x, y).getBlue()  * 255.0)
                );
                
                outputImage.getPixelWriter().setColor(i, j, color);
            }
        }
        
        ImageModel output = new ImageModel(outputImage, sourceImage.GetFileLocation());
        
        return output;
    }
    
    public static ImageModel GET_JOINED_IMAGE(ImageModel mainImage, ImageModel scrapImage, double[] bounds) {
        int startX      = (int) bounds[0];
        int startY      = (int) bounds[1];
        int imageWidth  = (int) bounds[2];
        int imageHeight = (int) bounds[3];
        
        ImageModel outputImage = GET_CLONED_IMAGE(mainImage);
        
        for (int i = 0, x = startX; i < scrapImage.getWidth(); i++, x++) {
            for (int j = 0, y = startY; j < scrapImage.getHeight(); j++, y++) {
                if ((scrapImage.getPixelReader().getColor(i, j).getOpacity() != 0) && (x < mainImage.getWidth()) && (y < mainImage.getHeight())) {
                    Color color = Color.rgb(
                            (int) GET_TRUNCATED_RGB_VALUE(scrapImage.getPixelReader().getColor(i, j).getRed()   * 255.0),
                            (int) GET_TRUNCATED_RGB_VALUE(scrapImage.getPixelReader().getColor(i, j).getGreen() * 255.0),
                            (int) GET_TRUNCATED_RGB_VALUE(scrapImage.getPixelReader().getColor(i, j).getBlue()  * 255.0)
                    );

                    outputImage.getPixelWriter().setColor(x, y, color);
                }
            }
        }
        
        return outputImage;
    }
    
    public static ImageModel GET_IMAGE_AFTER_REMOVE_PORTION(ImageModel sourceImage, double[] bounds) {
        int startX      = (int) bounds[0];
        int startY      = (int) bounds[1];
        int imageWidth  = (int) bounds[2];
        int imageHeight = (int) bounds[3];
        
        ImageModel outputImage = GET_CLONED_IMAGE(sourceImage);
        
        for (int x = startX; x < startX + imageWidth; x++) {
            for (int y = startY; y < startY + imageHeight; y++) {
                outputImage.getPixelWriter().setColor(x, y, Color.rgb(0, 0, 0, 0));
            }
        }
        
        return outputImage;
    }
}
