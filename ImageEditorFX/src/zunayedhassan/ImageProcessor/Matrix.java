package zunayedhassan.ImageProcessor;

/**
 *
 * @author Zunayed Hassan
 */
public class Matrix {
    public double Data[][] = null;
    
    public Matrix(int row, int column, boolean isInitialize) {
        this.Data = new double[row][column];
        
        if (isInitialize) {
            for (int i = 0; i < row; i++) {
                for (int j = 0; j < column; j++) {
                    this.Data[i][j] = 0;
                }
            }
        }
    }
    
    public Matrix(int row, int column) {
        this.Data = new double[row][column];
        
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                this.Data[i][j] = 0;
            }
        }
    }
    
    public Matrix GetMultiplyBy(Matrix anotherMatrix) {
        if (this.GetColumn() == anotherMatrix.GetRow()) {
            Matrix result = new Matrix(anotherMatrix.GetRow(), this.GetColumn());
        
            for (int i = 0; i < result.GetRow(); i++) {
                for (int j = 0; j < result.GetColumn(); j++) {
                    for (int k = 0; k < this.Data[i].length; k++) {
                        result.Data[i][j] += this.Data[i][k] * anotherMatrix.Data[k][j];
                    }
                }
            }
        
            return result;
        }
        else {
            System.out.println("Error: Matrix dimension problem");
            return null;
        }
    }
    
    public Matrix GetMultiplyBy(int number) {
        Matrix result = new Matrix(this.GetRow(), this.GetColumn());
            
        for (int i = 0; i < result.GetRow(); i++) {
            for (int j = 0; j < result.GetColumn(); j++) {
                result.Data[i][j] = this.Data[i][j] * number;
            }
        }

        return result;
    }
    
    public Matrix GetAddBy(Matrix anotherMatrix) {
        if ((this.GetRow() == anotherMatrix.GetRow()) && (this.GetColumn() == anotherMatrix.GetColumn())) {
            Matrix result = new Matrix(this.GetRow(), this.GetColumn());
            
            for (int i = 0; i < result.GetRow(); i++) {
                for (int j = 0; j < result.GetColumn(); j++) {
                    result.Data[i][j] = this.Data[i][j] + anotherMatrix.Data[i][j];
                }
            }
            
            return result;
        }
        else {
            System.out.println("Error: Matrix dimension problem");
            return null;
        }
    }
    
    public Matrix GetAddBy(int number) {
        Matrix result = new Matrix(this.GetRow(), this.GetColumn());
            
        for (int i = 0; i < result.GetRow(); i++) {
            for (int j = 0; j < result.GetColumn(); j++) {
                result.Data[i][j] = this.Data[i][j] + number;
            }
        }

        return result;
    }
    
    public Matrix GetSubtractBy(Matrix anotherMatrix) {
        if ((this.GetRow() == anotherMatrix.GetRow()) && (this.GetColumn() == anotherMatrix.GetColumn())) {
            Matrix result = new Matrix(this.GetRow(), this.GetColumn());
            
            for (int i = 0; i < result.GetRow(); i++) {
                for (int j = 0; j < result.GetColumn(); j++) {
                    result.Data[i][j] = this.Data[i][j] - anotherMatrix.Data[i][j];
                }
            }
            
            return result;
        }
        else {
            System.out.println("Error: Matrix dimension problem");
            return null;
        }
    }
    
    public Matrix GetSubtractBy(int number) {
        Matrix result = new Matrix(this.GetRow(), this.GetColumn());
            
        for (int i = 0; i < result.GetRow(); i++) {
            for (int j = 0; j < result.GetColumn(); j++) {
                result.Data[i][j] = this.Data[i][j] - number;
            }
        }

        return result;
    }
    
    public int GetRow() {
        return this.Data.length;
    }
    
    public int GetColumn() {
        return this.Data[0].length;
    }
    
    @Override
    public String toString() {
        String output = "";

        for (int i = 0; i < this.GetRow(); i++) {
            for (int j = 0; j < this.GetColumn(); j++) {
                output += Double.toString(this.Data[i][j]) + "\t";
            }
            
            output += "\n";
        }
        
        return output;
    }
}
